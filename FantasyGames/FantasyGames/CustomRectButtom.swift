//
//  CustomRectButtom.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 15/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class CustomRectButtom: UIButton {
    
    func updateSolidLayerProperties() {
        
        if isEnabled{
            setTitleColor(UIColor.white, for: .normal)
            self.gradient();
        }
        else{
            self.removeTabGradient()
            layer.cornerRadius = 6.0;
            titleLabel?.font = UIFont(name: "Arial-Bold", size: 13)
            setTitleColor(UIColor.appThemeColorColor(), for: .normal)
            
            layer.borderColor = UIColor.appThemeColorColor().cgColor
            layer.borderWidth = 1.0
        }
    }
    
    func updateReactLayerProperties() {

        layer.cornerRadius = 6.0;
        titleLabel?.font = UIFont(name: "Arial-Bold", size: 13)
        setTitleColor(UIColor.appThemeColorColor(), for: .normal)
        layer.borderColor = UIColor.appThemeColorColor().cgColor
        layer.borderWidth = 1.0
        
        if isEnabled{
            backgroundColor = UIColor.white
        }
        else{
            backgroundColor = UIColor(red: 244.0/255, green: 244.0/255, blue: 244.0/255, alpha: 1)
        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        if self.tag == 100{            
            updateReactLayerProperties()
        }
        else{
            updateSolidLayerProperties()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}
