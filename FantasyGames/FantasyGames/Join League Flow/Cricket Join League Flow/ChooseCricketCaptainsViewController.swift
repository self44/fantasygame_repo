//
//  ChooseCricketCaptainsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class ChooseCricketCaptainsViewController: UIViewController {
    
    @IBOutlet weak var captainImgView: UIImageView!
    @IBOutlet weak var viceCaptainImgView: UIImageView!

    @IBOutlet weak var lblGetsCaptainPoints: UILabel!
    @IBOutlet weak var lblGetsViceCaptainsPoints: UILabel!

    @IBOutlet weak var teamPreviewButton: CustomRectButtom!
    
    @IBOutlet weak var saveTeamButton: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var captainsView: UIView!
    @IBOutlet weak var containerView: UIView!
    var leagueDetails: LeagueDetails?
    var timer: Timer?

    lazy var totalPlayerArray = Array<PlayerDetails>()
    lazy var matchType = MatchType.Cricket.rawValue
    lazy var isEditTeam = false
    var teamDetails: TeamDetails?
    var captionDetails: PlayerDetails?
    var viceCaptionDetails: PlayerDetails?
    var matchDetails: MatchDetails?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.bottomTabShadow()
        captainsView.bottomTabShadow()
        bottomView.bottomTabShadow()
        
        view.headerGradient()
        captainImgView.layer.borderWidth = 1.0
        viceCaptainImgView.layer.borderWidth = 1.0
        captainImgView.layer.borderColor = UIColor.appGrayColor().cgColor
        viceCaptainImgView.layer.borderColor = UIColor.appGrayColor().cgColor
        
        lblGetsCaptainPoints.textColor = UIColor.appGrayColor()
        lblGetsViceCaptainsPoints.textColor = UIColor.appGrayColor()
        tblView.register(UINib(nibName: "CaptainDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "CaptainDetailsTableViewCell")
        showMatchClosingTime()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        timer?.invalidate()
        timer = nil
        weak var weakSelf = self
        showMatchClosingTime()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.showMatchClosingTime()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer?.invalidate()
        timer = nil
    }

    //MARK:- Actions Methods
    @IBAction func saveTeamButtonTapped(_ sender: Any) {
    
        guard captionDetails != nil else {
            AppHelpers.showCustomErrorAlertView(message: "Please select your captain")
            return;
        }
        
        guard viceCaptionDetails != nil else {
            AppHelpers.showCustomErrorAlertView(message: "Please select your vice 'captain")
            return;
        }
        
        if isEditTeam {
            callUpdateTeamAPI()
        }
        else{
            callCreateTeamAPI()
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func teamPreviewButtonTapped(_ sender: Any) {
    
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "CricketTeamPreviewViewController") as! CricketTeamPreviewViewController
        teamPreviewVC.selectedMatchDetails = matchDetails
        teamPreviewVC.totalPlayerArray = totalPlayerArray
        navigationController?.pushViewController(teamPreviewVC, animated: true)

    }
    
    @objc func captainButtonTapped(button: UIButton) {
        
        let playerInfo = totalPlayerArray[button.tag]
        if playerInfo.playerKey == viceCaptionDetails?.playerKey{
            playerInfo.isViceCaption = false
            viceCaptionDetails = nil
        }
        
        captionDetails = playerInfo
        
        if captionDetails?.playerKey == viceCaptionDetails?.playerKey {
            viceCaptionDetails = nil
        }

        for details in totalPlayerArray {
            details.isCaption = false
        }

        captionDetails?.isCaption = true
        captionDetails?.isViceCaption = false

        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveTeamButton.isEnabled = true
        }
        else{
            saveTeamButton.isEnabled = false
        }
        showCaptainsData()
        tblView.reloadData()
    }
    
    @objc func viceCaptainButtonTapped(button: UIButton) {
        
        let playerInfo = totalPlayerArray[button.tag]
        if playerInfo.playerKey == viceCaptionDetails?.playerKey{
            playerInfo.isCaption = true
            captionDetails = nil
        }
        viceCaptionDetails = playerInfo
        
        if captionDetails?.playerKey == viceCaptionDetails?.playerKey {
            captionDetails = nil
        }

        for details in totalPlayerArray {
            details.isViceCaption = false
        }
        
        viceCaptionDetails?.isCaption = false
        viceCaptionDetails?.isViceCaption = true

        if (captionDetails != nil) &&  (viceCaptionDetails != nil){
            saveTeamButton.isEnabled = true
        }
        else{
            saveTeamButton.isEnabled = false
        }
        showCaptainsData()
        tblView.reloadData()
    }
    
    
    func showCaptainsData() {
     
        lblGetsCaptainPoints.text = "C gets 2x points"
        lblGetsViceCaptainsPoints.text = "VC gets 1.5x points"
        captainImgView.image = UIImage(named: "MalePlayerPhotoPlaceholder")
        viceCaptainImgView.image = UIImage(named: "MalePlayerPhotoPlaceholder")

        
        guard let capDetails = captionDetails else { return }
        
        var placeholder = "MalePlayerPhotoPlaceholder"
        if capDetails.gender == "f" {
            placeholder = "FemalePlayerPhotoPlaceholder"
        }

        lblGetsCaptainPoints.text = capDetails.playerShortName + " gets 2x points"
        if capDetails.playerImage.count > 0 {
            if let url = URL(string: capDetails.playerImage){
                captainImgView.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.captainImgView.image = image
                    }
                    else{
                        self?.captainImgView.image = UIImage(named: placeholder)
                    }
                })
            }
        }
        else{
            captainImgView.image = UIImage(named: placeholder)
        }
        
        
        guard let viceCapDetails = viceCaptionDetails else { return }
        
        if capDetails.gender == "f" {
            placeholder = "FemalePlayerPhotoPlaceholder"
        }
        
        lblGetsViceCaptainsPoints.text = viceCapDetails.playerShortName + " gets 2x points"
        if viceCapDetails.playerImage.count > 0 {
            if let url = URL(string: viceCapDetails.playerImage){
                viceCaptainImgView.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.viceCaptainImgView.image = image
                    }
                    else{
                        self?.viceCaptainImgView.image = UIImage(named: placeholder)
                    }
                })
            }
        }
        else{
            viceCaptainImgView.image = UIImage(named: placeholder)
        }
    }
    
    func redirectbackNavigation() {
        var isVistorVCFound = false
        
        if let navArray = navigationController?.viewControllers{
            for viewController in navArray {
                if let visitorVC = viewController as? MyCricketTeamsViewController{
                    navigationController?.popToViewController(visitorVC, animated: true)
                    isVistorVCFound = true
                    break;
                }
                else if let visitorVC = viewController as? MoreLeaguesViewController{
                    navigationController?.popToViewController(visitorVC, animated: true)
                    isVistorVCFound = true
                    break;
                }
//                else if let visitorVC = viewController as? MoreLeaguesViewController{
//                    navigationController?.popToViewController(visitorVC, animated: true)
//                }
                else if let visitorVC = viewController as? LeagueViewController{
                    navigationController?.popToViewController(visitorVC, animated: true)
                    isVistorVCFound = true
                    break;
                }
            }
            
            if !isVistorVCFound {
                navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    //MARK:- API Related Methods
    func callCreateTeamAPI()  {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }
        
        guard let details = matchDetails else {
            return;
        }
        
        AppHelpers.sharedInstance.displayLoader()

        var playerIDs = Array<String>()
        for details in totalPlayerArray {
            playerIDs.append(details.playerKey)
        }
        
        let params = ["matchId": details.matchID,
                      "players": playerIDs,
                      "captain": captionDetails!.playerKey,
                      "viceCaptian": viceCaptionDetails!.playerKey,
                      "fantasyType": "1"] as [String : Any]
        
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kGetCricketTeamURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? ""
                
                if statusCode == "200"{
                    
                    guard let details = weakSelf?.leagueDetails else {
                        AppHelpers.sharedInstance.removeLoader()
                        weakSelf?.redirectbackNavigation()
                        return
                    }
                    weakSelf?.callValidateLeagueAPI(leagueDetails: details)
                }
                else if statusCode == "405"{
                    AppHelpers.sharedInstance.removeLoader()
                    let matchCloseView = MatchClosedView(frame: APPDELEGATE.window!.frame)
                    APPDELEGATE.window?.addSubview(matchCloseView)
                }
                else{
                    AppHelpers.sharedInstance.removeLoader()
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
            }
            else{
                AppHelpers.sharedInstance.removeLoader()
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
    
    //MARK:- API Related Methods
    func callUpdateTeamAPI()  {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }
        
        guard let details = matchDetails else {
            return;
        }
        
        guard let teamInfo = teamDetails else {
            return;
        }
        
        AppHelpers.sharedInstance.displayLoader()

        var playerIDs = Array<String>()
        for details in totalPlayerArray {
            playerIDs.append(details.playerKey)
        }
        
        let params = ["matchId": details.matchID,
                      "players": playerIDs,
                      "captain": captionDetails!.playerKey,
                      "viceCaptian": viceCaptionDetails!.playerKey,
                      "teamId": teamInfo.teamId,
                      "fantasyType": "1"] as [String : Any]
        
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kGetUpdateTeamURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                AppHelpers.sharedInstance.removeLoader()

                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? ""
                
                if statusCode == "200"{
                    weakSelf?.redirectbackNavigation()
                }
                else if statusCode == "405"{
                    let matchCloseView = MatchClosedView(frame: APPDELEGATE.window!.frame)
                    APPDELEGATE.window?.addSubview(matchCloseView)
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
                
            }
            else{
                AppHelpers.sharedInstance.removeLoader()
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
    
    func callValidateLeagueAPI(leagueDetails: LeagueDetails) {
        
        guard let details = matchDetails else {
            return;
        }
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }
        AppHelpers.sharedInstance.displayLoader()

        var urlString = ""
        if matchType == MatchType.Cricket.rawValue {
            urlString = kValidateCricketLeagueURL
        }
        
        let params = ["matchId": details.matchID, "leagueId": leagueDetails.leagueId]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            AppHelpers.sharedInstance.removeLoader()
            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? kErrorMsg

                if statusCode == "200" {
                   
                    if result!["userInfo"].dictionary != nil {
                        UserDetails.sharedInstance.parseUserDetails(response: result!["userInfo"])
                    }

                    if let teamsArray = result!["teams"].array {
                        let userTeamsArray = TeamDetails.getAllUserTeamsForCricket(dataArray: teamsArray, matchDetails: details)
                        AppHelpers.redirectToJoinLeagueConfirmation(details: details, leagueDetails: leagueDetails, teamsArray: userTeamsArray, matchType: weakSelf!.matchType)
                    }
                    else if statusCode == "405"{
                        AppHelpers.sharedInstance.removeLoader()
                        let matchCloseView = MatchClosedView(frame: APPDELEGATE.window!.frame)
                        APPDELEGATE.window?.addSubview(matchCloseView)
                    }
                    else{
                        AppHelpers.redirectToJoinLeagueConfirmation(details: details, leagueDetails: leagueDetails, teamsArray: Array<TeamDetails>(), matchType: weakSelf!.matchType)
                    }
                }
                else if statusCode == "201" {
                    let requiredAmt = result!["requiredAmount"].stringValue
                    
                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                        let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: kAddCash, style: UIAlertAction.Style.default, handler: { action -> Void in
                            AppHelpers.redirectToAddCash(amount: Int(requiredAmt) ?? 0)
                        }))
                        alert.addAction(UIAlertAction(title: kCancel, style: UIAlertAction.Style.default, handler: nil))
                        navVC.present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
            }
        }
    }
    
    //MARK:- Match Timer Methods
    @objc func showMatchClosingTime()  {
        
        guard let details = matchDetails else {
            return;
        }
        
        if details.isClosed  {
            timer?.invalidate()
            timer = nil
            AppHelpers.showMatchClosePopup()
        }        
    }
}


extension ChooseCricketCaptainsViewController: UITableViewDelegate, UITableViewDataSource{
    
       
    //MARK:- Table View Data Source and Delegate Methods
     
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalPlayerArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 51.0
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
        var cell = tableView.dequeueReusableCell(withIdentifier: "CaptainDetailsTableViewCell") as? CaptainDetailsTableViewCell
        if cell == nil {
            cell = CaptainDetailsTableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "CaptainDetailsTableViewCell")
        }
         
        
        cell?.captainButton.addTarget(self, action: #selector(captainButtonTapped(button:)), for: .touchUpInside)
        cell?.viceCaptainButton.addTarget(self, action: #selector(viceCaptainButtonTapped(button:)), for: .touchUpInside)
        cell?.selectionStyle = .none
        cell?.captainButton.tag = indexPath.row
        cell?.viceCaptainButton.tag = indexPath.row
        
        let playerInfo = totalPlayerArray[indexPath.row]
        cell?.configData(details: playerInfo, gameType: MatchType.Cricket.rawValue)

         
        if playerInfo.playerKey == captionDetails?.playerKey {
            cell?.captainButton.layer.borderWidth = 0.0
            cell?.captainButton.backgroundColor = UIColor.appThemeColorColor()
            cell?.captainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor.appBlackColor()
            cell?.captainButton.setTitle("2x", for: .normal)
        }
         
        if playerInfo.playerKey == viceCaptionDetails?.playerKey {
            cell?.viceCaptainButton.layer.borderWidth = 0.0
            cell?.viceCaptainButton.backgroundColor = UIColor.appThemeColorColor()
            cell?.viceCaptainButton.setTitleColor(UIColor.white, for: .normal)
            cell?.contentView.backgroundColor = UIColor.appBlackColor()
            cell?.viceCaptainButton.setTitle("1.5x", for: .normal)
        }
         
        return cell!
     }
    
}
