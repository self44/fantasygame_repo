//
//  MyCricketTeamsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class MyCricketTeamsViewController: UIViewController {
    @IBOutlet weak var tblBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var createTeamButton: UIButton!
    
    var selectMatchDetails: MatchDetails?
    lazy var matchType = MatchType.Cricket.rawValue
    lazy var userTeamsArray = Array<TeamDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        containerView.appbackgroudColor()
        bottomView.bottomTabShadow()
        containerView.topViewCorners()
        createTeamButton.gradient()
        tblView.register(UINib(nibName: "CricketTeamTableViewCell", bundle: nil), forCellReuseIdentifier: "CricketTeamTableViewCell")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if !(selectMatchDetails?.isClosed ?? false) {
            callGetMatchTeams(isNeedToShowLoader: true)
        }
        else{
            bottomView.isHidden = true
            tblBottomConstraint.constant = 0
            tblView.layoutIfNeeded()

            if userTeamsArray.count == 0 {
                callGetMatchTeams(isNeedToShowLoader: true)
            }
        }
    }
    
    //MARK:- Actions Methods
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createTeamButtonTapped(_ sender: Any) {
        guard let details = selectMatchDetails else {
            return;
        }
        AppHelpers.redirectToCreateCricketTeam(details: details, leagueDetails: nil, isEditTeam: false, teamDetails: nil)
    }
    
    
    //MARK:- API Related Methods
    func callGetMatchTeams(isNeedToShowLoader: Bool)  {
        guard let details = selectMatchDetails else { return}
        
        if isNeedToShowLoader {
            if !AppHelpers.isInterNetConnectionAvailable(){
                return;
            }
            AppHelpers.sharedInstance.displayLoader()
        }
        
        var urlString = ""
        if matchType == MatchType.Cricket.rawValue {
            urlString = kGetCricketTeamURL + "/" + details.matchID
        }
        
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()

            if result != nil{
                let statusCode = result!["code"].stringValue
                if statusCode == "200" {
                    if let teamsArray = result!["teams"].array  {
                        weakSelf?.userTeamsArray.removeAll()
                        weakSelf?.userTeamsArray = TeamDetails.getAllUserTeamsForCricket(dataArray: teamsArray, matchDetails: weakSelf!.selectMatchDetails!)
                        weakSelf?.tblView.reloadData()
                    }
                }
            }
        }
    }
}


extension MyCricketTeamsViewController: UITableViewDelegate, UITableViewDataSource{
    
   //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userTeamsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if matchType == MatchType.Cricket.rawValue {
            var cell = tableView.dequeueReusableCell(withIdentifier: "CricketTeamTableViewCell") as? CricketTeamTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "CricketTeamTableViewCell") as? CricketTeamTableViewCell
            }
            
            cell?.selectionStyle = .none
            let teamDetails = userTeamsArray[indexPath.row]
            cell?.configData(details: teamDetails, selectedTeam: nil, isForJoineLeague: false)
            
            cell?.editButton.isHidden = false
            cell?.cloneButton.isHidden = false
            
            if selectMatchDetails?.isClosed ?? false {
                cell?.editButton.isHidden = true
                cell?.cloneButton.isHidden = true
            }
            
            cell?.editButton.tag = indexPath.row
            cell?.cloneButton.tag = indexPath.row
            cell?.previewButton.tag = indexPath.row
            
            cell?.editButton.addTarget(self, action: #selector(editButtonTapped(button:)), for: .touchUpInside)
            cell?.cloneButton.addTarget(self, action: #selector(cloneButtonTapped(button:)), for: .touchUpInside)
            cell?.previewButton.addTarget(self, action: #selector(previewButtonTapped(button:)), for: .touchUpInside)

            return cell!
        }
        else{
            return UITableViewCell()
        }
    }
     
    @objc func editButtonTapped(button: UIButton) {
        
        guard let details = selectMatchDetails else { return}
        let teamDetails = userTeamsArray[button.tag]
        AppHelpers.redirectToCreateCricketTeam(details: details, leagueDetails: nil, isEditTeam: true, teamDetails: teamDetails)
    }
    
    @objc func cloneButtonTapped(button: UIButton) {
        guard let details = selectMatchDetails else { return}
        let teamDetails = userTeamsArray[button.tag]
        AppHelpers.redirectToCreateCricketTeam(details: details, leagueDetails: nil, isEditTeam: false, teamDetails: teamDetails)
    }

    @objc func previewButtonTapped(button: UIButton) {
        guard let details = selectMatchDetails else { return}

        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "CricketTeamPreviewViewController") as! CricketTeamPreviewViewController
        teamPreviewVC.selectedMatchDetails = details
        let teamDetails = userTeamsArray[button.tag]
        teamPreviewVC.totalPlayerArray = teamDetails.playersArray
        navigationController?.pushViewController(teamPreviewVC, animated: true)

    }
    
}
