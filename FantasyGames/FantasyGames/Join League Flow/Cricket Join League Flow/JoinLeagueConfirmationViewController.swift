//
//  JoinLeagueConfirmationViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 16/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit


class JoinLeagueConfirmationViewController: UIViewController, JoiningConfirmationViewDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var joinLeagueButton: CustomRectButtom!
    @IBOutlet weak var lblRemainingTime: UILabel!
    @IBOutlet weak var lblMatchName: UILabel!
    @IBOutlet weak var containerView: UIView!

    var leagueDetails: LeagueDetails?
    var matchDetails: MatchDetails?
    var selectedTeam: TeamDetails?
    var timer: Timer?
    
    var matchType = MatchType.Cricket.rawValue
    lazy var userTeamsArray = Array<TeamDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.topViewCorners()
        bottomView.bottomTabShadow()
        view.headerGradient()
        tblView.register(UINib(nibName: "CricketTeamTableViewCell", bundle: nil), forCellReuseIdentifier: "CricketTeamTableViewCell")
        if let details = matchDetails {
            lblMatchName.text = "\(details.team1ShortName) vs \(details.team2ShortName)"
        }
        showMatchClosingTime()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        timer?.invalidate()
        timer = nil
        weak var weakSelf = self
        showMatchClosingTime()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.showMatchClosingTime()
        }
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        timer?.invalidate()
        timer = nil
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        redirectbackNavigation()
    }

    
    @IBAction func walletButtonTapped(_ sender: Any) {
        
        let addCashVC = storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
        navigationController?.pushViewController(addCashVC, animated: true)
    }
    
    @IBAction func joinLeagueButtonTapped(_ sender: Any) {

        if selectedTeam == nil {
            AppHelpers.showCustomErrorAlertView(message: "Please select a team")
            return
        }
        
        let joiningConfirmationView = JoiningConfirmationView(frame: APPDELEGATE.window!.frame)
        joiningConfirmationView.delegate = self
        joiningConfirmationView.leagueDetails = leagueDetails
        joiningConfirmationView.matchDetails = matchDetails
        joiningConfirmationView.selectedTeamsArray = [selectedTeam!]
        joiningConfirmationView.updateData()
        APPDELEGATE.window?.addSubview(joiningConfirmationView)
    }
    
    func redirectbackNavigation() {
        var isVistorVCFound = false
        
        if let navArray = navigationController?.viewControllers{
            for viewController in navArray {
                if let visitorVC = viewController as? MyCricketTeamsViewController{
                    navigationController?.popToViewController(visitorVC, animated: true)
                    isVistorVCFound = true
                    break;
                }
                else if let visitorVC = viewController as? MoreLeaguesViewController{
                    navigationController?.popToViewController(visitorVC, animated: true)
                    isVistorVCFound = true
                    break;
                }
//                else if let visitorVC = viewController as? MoreLeaguesViewController{
//                    navigationController?.popToViewController(visitorVC, animated: true)
//                }
                else if let visitorVC = viewController as? LeagueViewController{
                    navigationController?.popToViewController(visitorVC, animated: true)
                    isVistorVCFound = true
                    break;
                }
            }
            
            if !isVistorVCFound {
                navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    //MARK:- Match Timer Methods
    @objc func showMatchClosingTime()  {
        
        guard let details = matchDetails else {
            return;
        }
        
        if details.isClosed  {
            timer?.invalidate()
            timer = nil
            lblRemainingTime.text = "Leagues Closed"
            AppHelpers.showMatchClosePopup()
        }
        else {
            lblRemainingTime.text = AppHelpers.getMatchClosingTime(matchTimestamp: details.startDateUnix)
        }
    }
}



extension JoinLeagueConfirmationViewController: UITableViewDelegate, UITableViewDataSource{
    
       //MARK:- Table View Data Source and Delegate Methods
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return userTeamsArray.count
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 160.0
        }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if matchType == MatchType.Cricket.rawValue {
                var cell = tableView.dequeueReusableCell(withIdentifier: "CricketTeamTableViewCell") as? CricketTeamTableViewCell
                if cell == nil {
                    cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "CricketTeamTableViewCell") as? CricketTeamTableViewCell
                }
                
                cell?.selectionStyle = .none
                
                // TODO:- Will show in next version
                cell?.editButton.isHidden = true
                cell?.cloneButton.isHidden = true
                
                cell?.editButton.tag = indexPath.row
                cell?.cloneButton.tag = indexPath.row
                cell?.previewButton.tag = indexPath.row
                
                cell?.editButton.addTarget(self, action: #selector(editButtonTapped(button:)), for: .touchUpInside)
                cell?.cloneButton.addTarget(self, action: #selector(cloneButtonTapped(button:)), for: .touchUpInside)
                cell?.previewButton.addTarget(self, action: #selector(previewButtonTapped(button:)), for: .touchUpInside)
                
                let teamDetails = userTeamsArray[indexPath.row]
                cell?.configData(details: teamDetails, selectedTeam: selectedTeam, isForJoineLeague: true)
                return cell!

            }
            else{
                return UITableViewCell()
            }
        }
        
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let details = userTeamsArray[indexPath.row]
        selectedTeam = details
        tblView.reloadData()
    }
    
    //MARK:- Action Methods
    
    @objc func editButtonTapped(button: UIButton) {
        guard let details = matchDetails else { return}
        let teamDetails = userTeamsArray[button.tag]
        AppHelpers.redirectToCreateCricketTeam(details: details, leagueDetails: leagueDetails, isEditTeam: true, teamDetails: teamDetails)
    }
    
    @objc func cloneButtonTapped(button: UIButton) {
        guard let details = matchDetails else { return}
        let teamDetails = userTeamsArray[button.tag]
        AppHelpers.redirectToCreateCricketTeam(details: details, leagueDetails: leagueDetails, isEditTeam: true, teamDetails: teamDetails)
    }

    @objc func previewButtonTapped(button: UIButton) {
        guard let details = matchDetails else { return}

        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "CricketTeamPreviewViewController") as! CricketTeamPreviewViewController
        teamPreviewVC.selectedMatchDetails = details
        let teamDetails = userTeamsArray[button.tag]
        teamPreviewVC.totalPlayerArray = teamDetails.playersArray
        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
}
