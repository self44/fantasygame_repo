//
//  JoinedLeaguesViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 21/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class JoinedLeaguesViewController: UIViewController {

    @IBOutlet weak var collectionContainerTopConstrant: NSLayoutConstraint!
    @IBOutlet weak var versusIcon: UIImageView!
    @IBOutlet weak var myTeamButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewContainerView: UIView!
    @IBOutlet weak var team1Name: UILabel!
    @IBOutlet weak var team2Name: UILabel!
    @IBOutlet weak var lblTeam2Over: UILabel!
    @IBOutlet weak var lblTeam1Over: UILabel!
    @IBOutlet weak var lblTeam2Runs: UILabel!
    @IBOutlet weak var lblTeam1Runs: UILabel!
    @IBOutlet weak var collectionViewContainerTopConstraint: NSLayoutConstraint!
    
    var matchDetails: MatchDetails?
    lazy var leagueDetailsArray = Array<LeagueDetails>()
    lazy var matchType = MatchType.Cricket.rawValue
    lazy var isNeedToshowMyTeams = false
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.headerGradient()
        containerView.topViewCorners()
        collectionViewContainerView.topViewCorners()
        collectionViewContainerView.backgroundColor = UIColor.appbackgroudColor()
        collectionView.backgroundColor = UIColor.appbackgroudColor()

        myTeamButton.gradient()
        bottomView.bottomTabShadow()
        refreshControl.addTarget(self, action: #selector(refreshLeagues), for: UIControl.Event.valueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.alwaysBounceVertical = true;

        collectionView.register(UINib(nibName: "JoinedLeaguesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JoinedLeaguesCollectionViewCell")
        collectionView.register(UINib(nibName: "JoinedLeaguesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JoinedLeaguesCollectionViewCell")
        
        callGetAllJoinedLeagueAPI(isShowLoader: true)
    }
    
    @objc func refreshLeagues() {
//        refreshControl.beginRefreshing()
        callGetAllJoinedLeagueAPI(isShowLoader: true)
        refreshControl.endRefreshing()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myTeamsButtonTapped(_ sender: Any) {
        if matchType == MatchType.Cricket.rawValue{
            let myteamVC = storyboard?.instantiateViewController(withIdentifier: "MyCricketTeamsViewController") as! MyCricketTeamsViewController
            myteamVC.matchType = matchType
            myteamVC.selectMatchDetails = matchDetails
            navigationController?.pushViewController(myteamVC, animated: true)
        }
    }
    
    //MARK:- API Related Methods
    func callGetAllJoinedLeagueAPI(isShowLoader: Bool) {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        guard let details = matchDetails else {
            return;
        }
        
        if isShowLoader{
            AppHelpers.sharedInstance.displayLoader()
        }
        
        var matchURL = ""
        
        if matchType == MatchType.Cricket.rawValue {
            matchURL = kCricketJoinedLeagueURL + details.matchID
        }
        
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: matchURL) { (result, error) in
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    if let leaguesArray = result!["leagues"].array {
                        weakSelf?.leagueDetailsArray = LeagueDetails.getAllLeagues(dataArray: leaguesArray)
                        if let liveScore = result!["liveScore"].dictionary {
                            if let matchRunsArray = liveScore["runs"]?.array {
                                weakSelf?.lblTeam1Runs.text = "0-0"
                                weakSelf?.lblTeam1Over.text = "0"
                                weakSelf?.team1Name.text = weakSelf?.matchDetails?.team1ShortName
                                weakSelf?.lblTeam2Runs.text = "0-0"
                                weakSelf?.lblTeam2Over.text = "0"
                                weakSelf?.team2Name.text = weakSelf?.matchDetails?.team2ShortName
                                
                                weakSelf?.lblTeam1Runs.isHidden = false
                                weakSelf?.lblTeam1Over.isHidden = false
                                weakSelf?.team1Name.isHidden = false
                                weakSelf?.team2Name.isHidden = false
                                weakSelf?.lblTeam2Runs.isHidden = false
                                weakSelf?.lblTeam1Runs.isHidden = false
                                weakSelf?.lblTeam2Over.isHidden = false
                                weakSelf?.versusIcon.isHidden = false

                                if matchRunsArray.count >= 2 {
                                    let team1Score = matchRunsArray[0]
                                    let team2Score = matchRunsArray[1]
                                    let team1Id = team1Score["teamId"].stringValue
                                    let team2Id = team2Score["teamId"].stringValue

                                    if team1Id == weakSelf?.matchDetails?.team1ID {
                                        let team1Run = team1Score["score"].stringValue
                                        let team1Wickets = team1Score["wickets"].stringValue
                                        weakSelf?.lblTeam1Runs.text = team1Run + "-" + team1Wickets
                                        weakSelf?.lblTeam1Over.text = "(" + String(format: "%.1f", team1Score["overs"].floatValue) + " ov)"

                                    }
                                    else if team1Id == weakSelf?.matchDetails?.team2ID {
                                        let team1Run = team1Score["score"].stringValue
                                        let team1Wickets = team1Score["wickets"].stringValue
                                         weakSelf?.lblTeam2Runs.text = team1Run + "-" + team1Wickets
                                         weakSelf?.lblTeam2Over.text = "(" + String(format: "%.1f", team1Score["overs"].floatValue) + " ov)"
                                    }
                                    
                                    if team2Id == weakSelf?.matchDetails?.team1ID {
                                        let team1Run = team2Score["score"].stringValue
                                        let team1Wickets = team2Score["wickets"].stringValue
                                        weakSelf?.lblTeam1Runs.text = team1Run + "-" + team1Wickets
                                        weakSelf?.lblTeam1Over.text = "(" + String(format: "%.1f", team2Score["overs"].floatValue) + " ov)"
                                    }
                                    else if team2Id == weakSelf?.matchDetails?.team2ID {
                                        let team1Run = team2Score["score"].stringValue
                                        let team1Wickets = team2Score["wickets"].stringValue
                                        weakSelf?.lblTeam2Runs.text = team1Run + "-" + team1Wickets
                                        weakSelf?.lblTeam2Over.text = "(" + String(format: "%.1f", team2Score["overs"].floatValue) + " ov)"
                                    }
                                }
                                else if matchRunsArray.count == 1{
                                    let team1Score = matchRunsArray[0]
                                    let team1Id = team1Score["teamId"].stringValue

                                    if team1Id == weakSelf?.matchDetails?.team1ID {
                                        let team1Run = team1Score["score"].stringValue
                                        let team1Wickets = team1Score["wickets"].stringValue
                                        weakSelf?.lblTeam1Runs.text = team1Run + "-" + team1Wickets
                                        weakSelf?.lblTeam1Over.text = "(" + String(format: "%.1f", team1Score["overs"].floatValue) + " ov)"
                                    }
                                    else if team1Id == weakSelf?.matchDetails?.team2ID {
                                        let team1Run = team1Score["score"].stringValue
                                        let team1Wickets = team1Score["wickets"].stringValue
                                        weakSelf?.lblTeam2Runs.text = team1Run + "-" + team1Wickets
                                        weakSelf?.lblTeam2Over.text = "(" + String(format: "%.1f", team1Score["overs"].floatValue) + " ov)"
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    weakSelf?.collectionViewContainerTopConstraint.constant = 46.0
                                }
                            }
                        }
                        weakSelf?.collectionView.reloadData()
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
            AppHelpers.sharedInstance.removeLoader()
        }
    }
}

extension JoinedLeaguesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JoinedLeaguesCollectionViewCell", for: indexPath) as! JoinedLeaguesCollectionViewCell
        cell.configData(matchDetails: matchDetails, leaguesArray: leagueDetailsArray)
        return cell
    }
    
}
