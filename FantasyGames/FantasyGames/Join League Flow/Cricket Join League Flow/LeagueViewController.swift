//
//  LeagueViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class LeagueViewController: UIViewController {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var teamButton: CustomRectButtom!
    @IBOutlet weak var leagueButton: CustomRectButtom!
    @IBOutlet weak var lblRemaingTime: UILabel!
    @IBOutlet weak var lblMatchName: UILabel!

    lazy var leaguesCategoriesArray = Array<LeagueCategoryDetails>()
    lazy var matchType = MatchType.Cricket.rawValue
    lazy var totalTeamCount = 0;
    lazy var totalJoinedLeagueCount = 0;
    var matchDetails: MatchDetails?
    var timer: Timer?

    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.headerGradient()
        bottomView.bottomTabShadow()
        containerView.topViewCorners()
        containerView.appbackgroudColor()
        tblView.appbackgroudColor()

        tblView.register(UINib(nibName: "LeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "LeagueTableViewCell")
        if let details = matchDetails {
            lblMatchName.text = "\(details.team1ShortName) vs \(details.team2ShortName)"
        }

        showMatchClosingTime()
        getAllLeaguesFromLocal()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if leaguesCategoriesArray.count == 0{
            callGetMatchLeagues(isNeedToShowLoader: true)
        }
        else{
            callGetMatchLeagues(isNeedToShowLoader: false)
        }
        
        timer?.invalidate()
        timer = nil
        weak var weakSelf = self
        showMatchClosingTime()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.showMatchClosingTime()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer?.invalidate()
        timer = nil
    }
    

    //MARK:- Action Methods
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func walletButtonTapped(_ sender: Any) {
        let walletCashVC = storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
        navigationController?.pushViewController(walletCashVC, animated: true)
    }
    
    func getAllLeaguesFromLocal() {
        
        guard let details = matchDetails else { return}
        let urlString = kCricketLeagueURL + details.matchID

        let savedResponse = CoreDataFileHandler.getDetailsFromCoreData(urlString: urlString)
        guard let response = savedResponse else {
            return;
        }
        
        let teamCount = response["teamCount"].stringValue
        let joinedLeagueCount = response["joinedLeagueCount"].stringValue
        totalTeamCount = Int(teamCount) ?? 0
        totalJoinedLeagueCount = Int(joinedLeagueCount) ?? 0
        if totalTeamCount > 1 {
            teamButton.setTitle("Teams \(totalTeamCount)", for: .normal)
        }
        else{
            teamButton.setTitle("Team \(totalTeamCount)", for: .normal)
        }
        
        if totalJoinedLeagueCount > 1 {
            leagueButton.setTitle("Leagues \(totalJoinedLeagueCount)", for: .normal)
        }
        else{
            leagueButton.setTitle("League \(totalJoinedLeagueCount)", for: .normal)
        }
        
        if response["match"].dictionary != nil {
            matchDetails = MatchDetails.parseMatchDetails(details: response["match"])
        }

        if let localTeamArray = response["leagues"].array {
            leaguesCategoriesArray.removeAll()
            let leagueArray = LeagueDetails.getAllLeagues(dataArray: localTeamArray)
            let leagueCate = LeagueCategoryDetails()
            leagueCate.leaguesArray = leagueArray
            leagueCate.categoryName = "Fantasyug Exclusive"
            leagueCate.showLeaguesCount = leagueArray.count
            leagueCate.categoryID = "1"
            leaguesCategoriesArray.append(leagueCate)
            tblView.reloadData()
        }
    }
    
    @IBAction func teamButtonTapped(_ sender: Any) {
        let myteamVC = storyboard?.instantiateViewController(withIdentifier: "MyCricketTeamsViewController") as! MyCricketTeamsViewController
        myteamVC.matchType = matchType
        myteamVC.selectMatchDetails = matchDetails
        navigationController?.pushViewController(myteamVC, animated: true)
    }
    
    @IBAction func leagueButtonTapped(_ sender: Any) {
        
        let joinedWinnerRankVC = storyboard?.instantiateViewController(withIdentifier: "JoinedLeaguesViewController") as! JoinedLeaguesViewController
        joinedWinnerRankVC.matchType = matchType
        joinedWinnerRankVC.matchDetails = matchDetails
        joinedWinnerRankVC.isNeedToshowMyTeams = false
        navigationController?.pushViewController(joinedWinnerRankVC, animated: true)
    }
    
    
   @objc func winnerButtonTapped(button: UIButton) {

       guard let cell = button.superview?.superview?.superview?.superview as? LeagueTableViewCell else {
           return;
       }
       guard let indexPath = tblView.indexPath(for: cell) else{
           return;
       }
       let categoryDetails = leaguesCategoriesArray[indexPath.section]
       let leagueDetails = categoryDetails.leaguesArray[indexPath.row]

       if leagueDetails.leagueType == LeagueType.Practice.rawValue {
           return
       }
       
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
       let leagueWinnerVC = storyboard.instantiateViewController(withIdentifier: "LeagueWinnersListViewController") as! LeagueWinnersListViewController
       leagueWinnerVC.leagueDetails = leagueDetails
       if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
           navVC.present(leagueWinnerVC, animated: true, completion: nil)
       }
   }
    
    //MARK:- API Related Methods

    func callGetMatchLeagues(isNeedToShowLoader: Bool)  {
        
        guard let details = matchDetails else { return}
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }
        
        if isNeedToShowLoader {
            AppHelpers.sharedInstance.displayLoader()
        }
        
        var urlString = ""
        if matchType == MatchType.Cricket.rawValue {
            urlString = kCricketLeagueURL + details.matchID
        }
        
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in

            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? kErrorMsg

                if statusCode == "200" {
                    DispatchQueue.main.async {
                        weakSelf?.getAllLeaguesFromLocal()
                    }
                    AppHelpers.sharedInstance.removeLoader()
                }
                else if statusCode == "405"{
                    AppHelpers.sharedInstance.removeLoader()
                    let matchCloseView = MatchClosedView(frame: APPDELEGATE.window!.frame)
                    APPDELEGATE.window?.addSubview(matchCloseView)
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message)
                    AppHelpers.sharedInstance.removeLoader()
                }
            }
            else{
                AppHelpers.sharedInstance.removeLoader()
            }
        }
    }
    
    
    func callValidateLeagueAPI(leagueDetails: LeagueDetails) {
        
        guard let details = matchDetails else {
            return;
        }
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }

        var urlString = ""
        if matchType == MatchType.Cricket.rawValue {
            urlString = kValidateCricketLeagueURL
        }
        
        AppHelpers.sharedInstance.displayLoader()
        let params = ["matchId": details.matchID, "leagueId": leagueDetails.leagueId]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? kErrorMsg

                if statusCode == "200" {
                   
                    if result!["userInfo"].dictionary != nil {
                        UserDetails.sharedInstance.parseUserDetails(response: result!["userInfo"])
                    }
                    
                    if let teamsArray = result!["teams"].array {
                        let userTeamsArray = TeamDetails.getAllUserTeamsForCricket(dataArray: teamsArray, matchDetails: details)
                        AppHelpers.redirectToJoinLeagueConfirmation(details: details, leagueDetails: leagueDetails, teamsArray: userTeamsArray, matchType: weakSelf!.matchType)
                    }
                    else{
                        AppHelpers.redirectToJoinLeagueConfirmation(details: details, leagueDetails: leagueDetails, teamsArray: Array<TeamDetails>(), matchType: weakSelf!.matchType)
                    }
                }
                else if statusCode == "201" {
                    let requiredAmt = result!["requiredAmount"].stringValue

                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                        let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: kAddCash, style: UIAlertAction.Style.default, handler: { action -> Void in
                            AppHelpers.redirectToAddCash(amount: Int(requiredAmt) ?? 0)
                        }))
                        alert.addAction(UIAlertAction(title: kCancel, style: UIAlertAction.Style.default, handler: nil))
                        navVC.present(alert, animated: true, completion: nil)
                    }
                }
                else if statusCode == "407"{
                    AppHelpers.showCustomErrorAlertView(message: message)
                    weakSelf?.callGetMatchLeagues(isNeedToShowLoader: false)
                }
                else if statusCode == "202" {
                    if weakSelf?.matchType == MatchType.Cricket.rawValue {
                        AppHelpers.redirectToCreateCricketTeam(details: details, leagueDetails: leagueDetails, isEditTeam: false, teamDetails: nil)
                    }
                    else if weakSelf?.matchType == MatchType.Kabaddi.rawValue {
                    }
                    else if weakSelf?.matchType == MatchType.Football.rawValue {
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
            }
        }
    }
    
    //MARK:- Match Timer Methods
    @objc func showMatchClosingTime()  {
        
        guard let details = matchDetails else {
            return;
        }
        
        if details.isClosed  {
            timer?.invalidate()
            timer = nil
            lblRemaingTime.text = "Leagues Closed"
            AppHelpers.showMatchClosePopup()
        }
        else {
            lblRemaingTime.text = AppHelpers.getMatchClosingTime(matchTimestamp: details.startDateUnix)
        }
    }
}

extension LeagueViewController: UITableViewDelegate, UITableViewDataSource {
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return leaguesCategoriesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let details = leaguesCategoriesArray[section]
        if details.leaguesArray.count > details.showLeaguesCount {
            return details.showLeaguesCount
        }
        else{
            return details.leaguesArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40))
        let titleLabel = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.bounds.size.width, height: 24))
        titleLabel.textColor = UIColor.appBlackColor()
        titleLabel.font = UIFont(name: "Arial-Bold", size: 15)
        headerView.appbackgroudColor()
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        let details = leaguesCategoriesArray[section]
        titleLabel.text = details.categoryName

        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell") as? LeagueTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "LeagueTableViewCell") as? LeagueTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        cell?.contentView.appbackgroudColor()
        cell?.winnerButton.tag = indexPath.row
        cell?.winnerButton.addTarget(self, action: #selector(winnerButtonTapped(button:)), for: .touchUpInside)
        let categoryDetails = leaguesCategoriesArray[indexPath.section]
        cell?.configData(details: categoryDetails.leaguesArray[indexPath.row]);

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categoryDetails = leaguesCategoriesArray[indexPath.section]
        let leagueDetails = categoryDetails.leaguesArray[indexPath.row]
        callValidateLeagueAPI(leagueDetails: leagueDetails)

    }
   
    
        
}
