//
//  SelectCricketPlayersViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 13/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

let kPlayerConditionNotSatisfyColor = UIColor(red: 215.0/255, green: 51.0/255 , blue: 97.0/255, alpha: 1)

let kPlayerConditionSatisfyColor = UIColor(red: 226.0/255, green: 144.0/255 , blue: 36.0/255, alpha: 1)

let kPlayerSelectionMaxLimitColor = UIColor(red: 21.0/255, green: 143.0/255 , blue: 255.0/255, alpha: 1)


class SelectCricketPlayersViewController: UIViewController {
    
    @IBOutlet weak var team2ImgView: UIImageView!
    @IBOutlet weak var team1ImgView: UIImageView!
    @IBOutlet weak var wicketKeeperIcon: UIImageView!
    @IBOutlet weak var batsmanIcon: UIImageView!
    @IBOutlet weak var bowlerIcon: UIImageView!
    @IBOutlet weak var allRounderIcon: UIImageView!
    @IBOutlet weak var creditSortIcon: UIImageView!
    @IBOutlet weak var pointSortIcon: UIImageView!
    @IBOutlet weak var teamSortIcon: UIImageView!
    @IBOutlet weak var sliderLeadingConstraint: NSLayoutConstraint!

    @IBOutlet weak var teamButton: UIButton!
    @IBOutlet weak var previewButton: UIButton!
    @IBOutlet weak var pointsButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var playing11Button: UIButton!
    @IBOutlet weak var lblAllRounderTitle: UILabel!
    @IBOutlet weak var lblBowlerTitle: UILabel!
    @IBOutlet weak var lblBatsmanTitle: UILabel!
    @IBOutlet weak var lblWicketTitle: UILabel!
    @IBOutlet weak var lblWicketCount: UILabel!
    @IBOutlet weak var lblAllRounderCount: UILabel!
    @IBOutlet weak var lblBatsmanCount: UILabel!
    @IBOutlet weak var lblBowlCount: UILabel!
    @IBOutlet weak var lblPickMessage: UILabel!
    @IBOutlet weak var lblRemainingTime: UILabel!
    @IBOutlet weak var lblSelectedTeamPlayerCount: UILabel!
    @IBOutlet weak var lblCreditLeft: UILabel!
    @IBOutlet weak var lblTeam2PlayerCount: UILabel!
    @IBOutlet weak var lblTeam1PlayerCount: UILabel!

    @IBOutlet weak var collectionContainerView: UIView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!

    lazy var totalWicketKeeperArray = Array<PlayerDetails>()
    lazy var totalBatsmanArray = Array<PlayerDetails>()
    lazy var totalBowlerArray = Array<PlayerDetails>()
    lazy var totalAllRounderArray = Array<PlayerDetails>()
    var totalPlayerArray = Array<PlayerDetails>()
    var selectedPlayerList = Array<PlayerDetails>()
    
    var matchDetails: MatchDetails?
    var leagueDetails: LeagueDetails?
    var timer: Timer?
    
    lazy var tempList = Array<PlayerDetails>()
    lazy var selectedPlayerRole = PlayerRole.WicketKeeper.rawValue
    lazy var playerSortedType = false
    lazy var teamSortedType = false
    lazy var pointsSortedType = true
    lazy var creditsSortedType = true
    lazy var selectedLeagueType = 0
    lazy var isComeFromEditPlayerTeamScreen = false
    lazy var isPlayerPlaying = false
    lazy var isEditTeam = false
    var teamDetails: TeamDetails?

    
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        previewButton.gradient()
        upperView.bottomTabShadow()
        collectionContainerView.bottomTabShadow()
        view.headerGradient()
        bottomView.bottomTabShadow()
        setupDefaultProperties()
        showTeamInformation()
        wicketButtonTapped(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

        
    //MARK:- Custom Methods
    func setupDefaultProperties() {
        
        lblWicketCount.text = "0"
        lblAllRounderCount.text = "0"
        lblBatsmanCount.text = "0"
        lblBowlCount.text = "0"
        collectionView.isHidden = true
        isPlayerPlaying = true
       nextButton.isEnabled = false
       nextButton.removeTabGradient()
       nextButton.backgroundColor = UIColor.appGrayColor()

        teamButton.setTitleColor(UIColor.appBlackColor(), for: .normal)
        pointsButton.setTitleColor(UIColor.appBlackColor(), for: .normal)
        creditButton.setTitleColor(UIColor.appBlackColor(), for: .normal)
        selectedPlayerRole = PlayerRole.WicketKeeper.rawValue
        NotificationCenter.default.addObserver(self, selector: #selector(playerSelectionUpdateNotification), name: .playerSelectionUpdateNotification, object: nil)
        collectionView.register(UINib(nibName: "CricketPlayerListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CricketPlayerListCollectionViewCell")
                
//        let urlString = kMatchPlayersURL + "?matchId=" + matchDetails!.matchID
        let urlString = kMatchPlayersURL + "?localTeam=" + matchDetails!.team1ID + "&visitorTeam=" + matchDetails!.team2ID

        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.callGetPlayerListAPI(urlString: urlString, isNeedToShowLoader: true)
        }
            
        showMatchClosingTime()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.showMatchClosingTime()
        }
    }
    
    func showTeamInformation() {
                
        guard let details = matchDetails else {
            return
        }
        
        if details.team1Image.count > 0 {
            if let url = URL(string: details.team1Image){
                team1ImgView.setImage(with: url, placeholder: UIImage(named: "TeamImagePlaceholder"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.team1ImgView.image = image
                    }
                    else{
                        self?.team1ImgView.image = UIImage(named: "TeamImagePlaceholder")
                    }
                })
            }
        }
        else{
            team1ImgView.image = UIImage(named: "TeamImagePlaceholder")
        }

        
        if details.team2Image.count > 0 {
            if let url = URL(string: details.team2Image){
                team2ImgView.setImage(with: url, placeholder: UIImage(named: "TeamImagePlaceholder"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.team2ImgView.image = image
                    }
                    else{
                        self?.team2ImgView.image = UIImage(named: "TeamImagePlaceholder")
                    }
                })
            }
        }
        else{
            team2ImgView.image = UIImage(named: "TeamImagePlaceholder")
        }
    }
    
    func getSelectedTypePlayerList(selectedRow: Int) -> Array<PlayerDetails> {
        
        var playersArray: Array<PlayerDetails>?
        if selectedRow == 0{
            playersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.WicketKeeper.rawValue
            })
        }
        else if selectedRow == 1{
            playersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.Batsman.rawValue
            })
        }
        else if selectedRow == 2{
            playersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.AllRounder.rawValue
            })
        }
        else if selectedRow == 3{
            playersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.Bowler.rawValue
            })
        }
        if playersArray != nil{
            return playersArray!
        }
        return []
    }
    
    @objc func playerSelectionUpdateNotification()  {
        getSelectedPlayerCounts()
        
        if selectedPlayerList.count >= 11 {
            nextButton.isEnabled = true
            nextButton.gradient()
        }
        else{
            nextButton.isEnabled = false
            nextButton.removeTabGradient()
            nextButton.backgroundColor = UIColor.appGrayColor()
        }
    }
    
    func getSelectedPlayerCounts(){
        
        selectedPlayerList = totalPlayerArray.filter { (details) -> Bool in
            return details.isSelected
        }
        
        let playerCredits = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + (Float(nextPlayerDetails.playerCredits) ?? 0.0)
        })
        
        let firstTeamPlayers = selectedPlayerList.filter { (details) -> Bool in
            details.teamID == matchDetails?.team1ID
        }
        
        let secondTeamPlayers = selectedPlayerList.filter { (details) -> Bool in
            details.teamID == matchDetails?.team2ID
        }
        
        let wicketPlayersArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.WicketKeeper.rawValue
        })
        
        let batsmanPlayersArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.Batsman.rawValue
        })
        
        let allRounderPlayerArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.AllRounder.rawValue
        })
        
        let bowlerPlayersArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.Bowler.rawValue
        })
        
        
        lblCreditLeft.text = "\(100.0 - playerCredits)"
        lblSelectedTeamPlayerCount.text = "\(selectedPlayerList.count)/11"
        lblTeam1PlayerCount.text = "\(firstTeamPlayers.count)"
        lblTeam2PlayerCount.text = "\(secondTeamPlayers.count)"
        lblWicketCount.text = "(" + String(wicketPlayersArray.count) + ")"
        lblBatsmanCount.text = "(" + String(batsmanPlayersArray.count) + ")"
        lblAllRounderCount.text = "(" + String(allRounderPlayerArray.count) + ")"
        lblBowlCount.text = "(" + String(bowlerPlayersArray.count) + ")"
        collectionView.reloadData()
    }
    
    func modifySelectedPlayerDetails()  {
        
        for selectedPlayermDetails in selectedPlayerList{
            for playermDetails in totalPlayerArray{
                if selectedPlayermDetails.playerKey == playermDetails.playerKey{
                    playermDetails.isSelected = true
                    playermDetails.isCaption = selectedPlayermDetails.isCaption
                    playermDetails.isViceCaption = selectedPlayermDetails.isViceCaption
                }
            }
        }
        playerSelectionUpdateNotification()
    }
    
    // MARK:- API Related Method
    
    func callGetPlayerListAPI(urlString: String, isNeedToShowLoader: Bool)  {
        
        if isNeedToShowLoader {
            if !AppHelpers.isInterNetConnectionAvailable(){
                return;
            }
            AppHelpers.sharedInstance.displayLoader()
        }
        
        weak var waekSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                var statusCode = result!["code"].stringValue
                statusCode = "200"
                
                if statusCode == "200" {
                    DispatchQueue.main.async {
                        if let playerList = result!["players"].array{
                            waekSelf?.totalPlayerArray = PlayerDetails.getPlayerDetailsArray(responseArray: playerList)
                            waekSelf?.modifySelectedPlayerDetails()
                            waekSelf?.showMatchClosingTime()
                            waekSelf?.totalWicketKeeperArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 0)
                            waekSelf?.totalBatsmanArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 1)
                            waekSelf?.totalAllRounderArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 2)
                            waekSelf?.totalBowlerArray = waekSelf!.getSelectedTypePlayerList(selectedRow: 3)
                            waekSelf?.sortPlayerBasedOnLineupsOut()
                            waekSelf?.collectionView.isHidden = false
                        }
                    }
                }
            }
            else{
                AppHelpers.showCustomAlertView(message: kErrorMsg)
            }
        }
    }
    //MARK:- -IBAction Methods
    @IBAction func wicketButtonTapped(_ sender: Any?) {
        
        lblPickMessage.text = "Pick 1-3 Wicket-Keeper"
        
        selectedPlayerRole = PlayerRole.WicketKeeper.rawValue
        wicketKeeperIcon.image = UIImage(named: "WicketKeeperSelectedIcon")
        batsmanIcon.image = UIImage(named: "BatsmanIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerIcon")
        bowlerIcon.image = UIImage(named: "BowlerIcon")
        
        lblWicketTitle.textColor = UIColor.appThemeColorColor()
        lblBatsmanTitle.textColor = UIColor.appBlackColor()
        lblBowlerTitle.textColor = UIColor.appBlackColor()
        lblAllRounderTitle.textColor = UIColor.appBlackColor()
        
        lblWicketCount.textColor = UIColor.appThemeColorColor()
        lblBatsmanCount.textColor = UIColor.appGrayColor()
        lblBowlCount.textColor = UIColor.appGrayColor()
        lblAllRounderCount.textColor = UIColor.appGrayColor()
        
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        sliderLeadingConstraint.constant = lblWicketTitle.frame.origin.x - 7
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    @IBAction func batsmanButtonTapped(_ sender: Any?) {
        lblPickMessage.text = "Pick 3-6 Batsmen"
        
        selectedPlayerRole = PlayerRole.Batsman.rawValue
        
        lblWicketTitle.textColor = UIColor.appBlackColor()
        lblBatsmanTitle.textColor = UIColor.appThemeColorColor()
        lblBowlerTitle.textColor = UIColor.appBlackColor()
        lblAllRounderTitle.textColor = UIColor.appBlackColor()
        
        lblWicketCount.textColor = UIColor.appGrayColor()
        lblBatsmanCount.textColor = UIColor.appThemeColorColor()
        lblBowlCount.textColor = UIColor.appGrayColor()
        lblAllRounderCount.textColor = UIColor.appGrayColor()
        
        wicketKeeperIcon.image = UIImage(named: "WicketKeeperIcon")
        batsmanIcon.image = UIImage(named: "BatsmanSelectedIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerIcon")
        bowlerIcon.image = UIImage(named: "BowlerIcon")
          
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        sliderLeadingConstraint.constant = lblBatsmanTitle.frame.origin.x - 7
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }

    }
    
    @IBAction func allRounderButtonTapped(_ sender: Any?) {
        lblPickMessage.text = "Pick 1-4 All Rounders"
        
        selectedPlayerRole = PlayerRole.AllRounder.rawValue
        
        allRounderIcon.layer.borderColor = kPlayerSelectionMaxLimitColor.cgColor
        lblWicketTitle.textColor = UIColor.appBlackColor()
        lblBatsmanTitle.textColor = UIColor.appBlackColor()
        lblBowlerTitle.textColor = UIColor.appBlackColor()
        lblAllRounderTitle.textColor = UIColor.appThemeColorColor()
        
        lblWicketCount.textColor = UIColor.appGrayColor()
        lblBatsmanCount.textColor = UIColor.appGrayColor()
        lblBowlCount.textColor = UIColor.appGrayColor()
        lblAllRounderCount.textColor = UIColor.appThemeColorColor()
        
        wicketKeeperIcon.image = UIImage(named: "WicketKeeperIcon")
        batsmanIcon.image = UIImage(named: "BatsmanIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerSelectedIcon")
        bowlerIcon.image = UIImage(named: "BowlerIcon")

        let indexPath = IndexPath(item: 2, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        sliderLeadingConstraint.constant = lblAllRounderTitle.frame.origin.x - 7
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }

    }
    
    @IBAction func bowlerButtonTapped(_ sender: Any?) {
        
        lblPickMessage.text = "Pick 3-6 Bowlers"

        selectedPlayerRole = PlayerRole.Bowler.rawValue
        
        lblWicketTitle.textColor = UIColor.appBlackColor()
        lblBatsmanTitle.textColor = UIColor.appBlackColor()
        lblBowlerTitle.textColor = UIColor.appThemeColorColor()
        lblAllRounderTitle.textColor = UIColor.appBlackColor()
        
        lblWicketCount.textColor = UIColor.appGrayColor()
        lblBatsmanCount.textColor = UIColor.appGrayColor()
        lblBowlCount.textColor = UIColor.appThemeColorColor()
        lblAllRounderCount.textColor = UIColor.appGrayColor()
        
        wicketKeeperIcon.image = UIImage(named: "WicketKeeperIcon")
        batsmanIcon.image = UIImage(named: "BatsmanIcon")
        allRounderIcon.image = UIImage(named: "AllRounderPlayerIcon")
        bowlerIcon.image = UIImage(named: "BowlerSelectedIcon")
        
        let indexPath = IndexPath(item: 3, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        sliderLeadingConstraint.constant = lblBowlerTitle.frame.origin.x - 4
        UIView.animate(withDuration: 0.2) {
            self.upperView.layoutIfNeeded()
        }
    }
    
    @IBAction func teamPreviewButtonTapped(_ sender: Any) {
        
        if selectedPlayerList.count == 0 {
            AppHelpers.showCustomAlertView(message: "Please select atleast one player")
            return
        }
        
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "CricketTeamPreviewViewController") as! CricketTeamPreviewViewController
        teamPreviewVC.selectedMatchDetails = matchDetails
        teamPreviewVC.totalPlayerArray = selectedPlayerList
        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    @IBAction func creditButtonTapped(_ sender: Any){

        let wicketKeeperPlayerListArray = totalWicketKeeperArray
        let batsmanPlayerListArray = totalBatsmanArray
        let allRounderPlayerListArray = totalAllRounderArray
        let bowlerPlayerListArray = totalBowlerArray
        
        let sortedKeeperArray =  wicketKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.playerCredits)! < Float(playerDetails.playerCredits)!
            }
            else{
                return Float(nextPlayerDetails.playerCredits)! > Float(playerDetails.playerCredits)!
            }
        })
        
        let sortedBatsmanArray =  batsmanPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.playerCredits)! < Float(playerDetails.playerCredits)!
            }
            else{
                return Float(nextPlayerDetails.playerCredits)! > Float(playerDetails.playerCredits)!
            }
        })
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if creditsSortedType{
                return Float(nextPlayerDetails.playerCredits)! < Float(playerDetails.playerCredits)!
            }
            else{
                return Float(nextPlayerDetails.playerCredits)! > Float(playerDetails.playerCredits)!
            }
        })
        
        let sortedBowlerArray =  bowlerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if creditsSortedType{
                return Float(nextPlayerDetails.playerCredits)! < Float(playerDetails.playerCredits)!
            }
            else{
                return Float(nextPlayerDetails.playerCredits)! > Float(playerDetails.playerCredits)!
            }
        })

        pointSortIcon.image = UIImage(named: "SortDefaultIcon")
        creditSortIcon.image = UIImage(named: "SortDefaultIcon")
        teamSortIcon.image = UIImage(named: "SortDefaultIcon")

        if creditsSortedType {
            creditSortIcon.image = UIImage(named: "SortUpArraow")
        }
        else{
            creditSortIcon.image = UIImage(named: "SortDownArraow")
        }
        
        creditsSortedType = !creditsSortedType
        totalWicketKeeperArray = sortedKeeperArray
        totalBatsmanArray = sortedBatsmanArray
        totalBowlerArray = sortedBowlerArray
        totalAllRounderArray = sortedAllrounderArray
        collectionView.reloadData()        
        playing11Button.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
    }
    
    @IBAction func pointsButtonTapped(_ sender: Any){

        let wicketKeeperPlayerListArray = totalWicketKeeperArray
        let batsmanPlayerListArray = totalBatsmanArray
        let allRounderPlayerListArray = totalAllRounderArray
        let bowlerPlayerListArray = totalBowlerArray
        
        let sortedKeeperArray =  wicketKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.playerPoints)! > Float(playerDetails.playerPoints)!
            }
            else{
                return Float(nextPlayerDetails.playerPoints)! < Float(playerDetails.playerPoints)!
            }
        })
        
        let sortedBatsmanArray =  batsmanPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            
            if pointsSortedType{
                return Float(nextPlayerDetails.playerPoints)! > Float(playerDetails.playerPoints)!
            }
            else{
                return Float(nextPlayerDetails.playerPoints)! < Float(playerDetails.playerPoints)!
            }
        })
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if pointsSortedType{
                return Float(nextPlayerDetails.playerPoints)! > Float(playerDetails.playerPoints)!
            }
            else{
                return Float(nextPlayerDetails.playerPoints)! < Float(playerDetails.playerPoints)!
            }
        })
        
        let sortedBowlerArray =  bowlerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if pointsSortedType{
                return Float(nextPlayerDetails.playerPoints)! > Float(playerDetails.playerPoints)!
            }
            else{
                return Float(nextPlayerDetails.playerPoints)! < Float(playerDetails.playerPoints)!
            }
        })
        
        pointSortIcon.image = UIImage(named: "SortDefaultIcon")
        creditSortIcon.image = UIImage(named: "SortDefaultIcon")
        teamSortIcon.image = UIImage(named: "SortDefaultIcon")

        if pointsSortedType {
            pointSortIcon.image = UIImage(named: "SortUpArraow")
        }
        else{
            pointSortIcon.image = UIImage(named: "SortDownArraow")
        }
        
        pointsSortedType = !pointsSortedType
        totalWicketKeeperArray = sortedKeeperArray
        totalBatsmanArray = sortedBatsmanArray
        totalBowlerArray = sortedBowlerArray
        totalAllRounderArray = sortedAllrounderArray
        collectionView.reloadData()
    }
    
    @IBAction func teamSortButtonTapped(_ sender: Any) {

        
        let wicketKeeperPlayerListArray = totalWicketKeeperArray
        let batsmanPlayerListArray = totalBatsmanArray
        let allRounderPlayerListArray = totalAllRounderArray
        let bowlerPlayerListArray = totalBowlerArray
        
        let sortedKeeperArray =  wicketKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if teamSortedType{
                return nextPlayerDetails.teamID == matchDetails?.team1ID
            }
            else{
                return nextPlayerDetails.teamID == matchDetails?.team2ID
            }
        })
        
        let sortedBatsmanArray =  batsmanPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if teamSortedType{
                return nextPlayerDetails.teamID == matchDetails?.team1ID
            }
            else{
                return nextPlayerDetails.teamID == matchDetails?.team2ID
            }
        })
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if teamSortedType{
                return nextPlayerDetails.teamID == matchDetails?.team1ID
            }
            else{
                return nextPlayerDetails.teamID == matchDetails?.team2ID
            }
        })
        
        let sortedBowlerArray =  bowlerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            if teamSortedType{
                return nextPlayerDetails.teamID == matchDetails?.team1ID
            }
            else{
                return nextPlayerDetails.teamID == matchDetails?.team2ID
            }
        })

        pointSortIcon.image = UIImage(named: "SortDefaultIcon")
        creditSortIcon.image = UIImage(named: "SortDefaultIcon")

        if teamSortedType {
            teamSortIcon.image = UIImage(named: "SortUpArraow")
        }
        else{
            teamSortIcon.image = UIImage(named: "SortDownArraow")
        }
        
        teamSortedType = !teamSortedType
        totalWicketKeeperArray = sortedKeeperArray
        totalBatsmanArray = sortedBatsmanArray
        totalBowlerArray = sortedBowlerArray
        totalAllRounderArray = sortedAllrounderArray
        collectionView.reloadData()
        playing11Button.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)

        collectionView.reloadData()
        sortPlayerBasedOnLineupsOut()
    }
        
        
    @IBAction func playing11ButtonTapped(_ sender: Any?) {
        isPlayerPlaying = !isPlayerPlaying
        sortPlayerBasedOnLineupsOut()
    }
    
    func sortPlayerBasedOnLineupsOut() {
        if isPlayerPlaying{
            playing11Button.setTitleColor(UIColor(red: 60.0/255, green: 196.0/255, blue: 266.0/255, alpha: 1), for: .normal)
        }
        else{
            playing11Button.setTitleColor(UIColor(red: 127.0/255, green: 132.0/255, blue: 134.0/255, alpha: 1), for: .normal)
        }
        
        let wicketKeeperPlayerListArray = totalWicketKeeperArray
        let batsmanPlayerListArray = totalBatsmanArray
        let allRounderPlayerListArray = totalAllRounderArray
        let bowlerPlayerListArray = totalBowlerArray
        
        let sortedKeeperArray =  wicketKeeperPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return playerDetails.isPlaying != isPlayerPlaying
        })
        
        let sortedBatsmanArray =  batsmanPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return playerDetails.isPlaying != isPlayerPlaying
        })
        
        let sortedAllrounderArray =  allRounderPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return playerDetails.isPlaying != isPlayerPlaying
        })
        
        let sortedBowlerArray =  bowlerPlayerListArray.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return playerDetails.isPlaying != isPlayerPlaying
        })
        
        pointSortIcon.image = UIImage(named: "SortDefaultIcon")
        creditSortIcon.image = UIImage(named: "SortDefaultIcon")
        
        totalWicketKeeperArray = sortedKeeperArray
        totalBatsmanArray = sortedBatsmanArray
        totalBowlerArray = sortedBowlerArray
        totalAllRounderArray = sortedAllrounderArray
        
        collectionView.reloadData()
    }
        
    @IBAction func nextButtonTapped(_ sender: Any) {
        if selectedPlayerList.count < 11{
            AppHelpers.showCustomAlertView(message: "Please select 11 players")
            return;
        }
        
        let wicketPlayersArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.WicketKeeper.rawValue
        })
        
        let batsmanPlayersArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.Batsman.rawValue
        })
        
        let bowlerPlayersArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.Bowler.rawValue
        })
        
        let allRounderPlayerArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            playerDetails.playerRole == PlayerRole.AllRounder.rawValue
        })
        
        
        let chooseCaptainsVC = storyboard?.instantiateViewController(withIdentifier: "ChooseCricketCaptainsViewController") as! ChooseCricketCaptainsViewController
        chooseCaptainsVC.totalPlayerArray.append(contentsOf: wicketPlayersArray)
        chooseCaptainsVC.totalPlayerArray.append(contentsOf: batsmanPlayersArray)
        chooseCaptainsVC.totalPlayerArray.append(contentsOf: allRounderPlayerArray)
        chooseCaptainsVC.totalPlayerArray.append(contentsOf: bowlerPlayersArray)
        chooseCaptainsVC.matchDetails = matchDetails
        chooseCaptainsVC.leagueDetails = leagueDetails
        chooseCaptainsVC.totalPlayerArray = selectedPlayerList
        chooseCaptainsVC.isEditTeam = isEditTeam
        chooseCaptainsVC.teamDetails = teamDetails
        
        let captainsArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            return playerDetails.isCaption
        })
        
        let viceCaptainsArray = selectedPlayerList.filter({ (playerDetails) -> Bool in
            return playerDetails.isViceCaption
        })

        if captainsArray.count > 0 {
            chooseCaptainsVC.captionDetails = captainsArray[0]
        }
        
        if viceCaptainsArray.count > 0 {
            chooseCaptainsVC.viceCaptionDetails = viceCaptainsArray[0]
        }
        
        navigationController?.pushViewController(chooseCaptainsVC, animated: true)
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    
    //MARK:- Match Timer Methods
    @objc func showMatchClosingTime()  {
        
        guard let details = matchDetails else {
            return;
        }
        
        if details.isClosed  {
            timer?.invalidate()
            timer = nil
            lblRemainingTime.text = "Leagues Closed"
            AppHelpers.showMatchClosePopup()
        }
        else {
            lblRemainingTime.text = AppHelpers.getMatchClosingTime(matchTimestamp: details.startDateUnix)
        }
    }
    

    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension SelectCricketPlayersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CricketPlayerListCollectionViewCell", for: indexPath) as? CricketPlayerListCollectionViewCell
        var playerList: Array<PlayerDetails>?
        
        if indexPath.row == 0 {
            playerList = totalWicketKeeperArray
        }
        else if indexPath.row == 1 {
            playerList = totalBatsmanArray
        }
        else if indexPath.row == 2 {
            playerList = totalAllRounderArray
        }
        else if indexPath.row == 3 {
            playerList = totalBowlerArray
        }
        
        cell?.configData(playerList: playerList!, gameType: MatchType.Cricket.rawValue, mathDetails: matchDetails!, existingPlayerList: selectedPlayerList)
        return cell!;
    }    
}
