//
//  AddCashViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 22/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class AddCashViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lblCurrentBalance: UILabel!
    @IBOutlet weak var enterPromoCodeView: UIView!
    @IBOutlet weak var enterAmountView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var addCashButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var button100: UIButton!
    @IBOutlet weak var button300: UIButton!
    @IBOutlet weak var button500: UIButton!
    @IBOutlet weak var promoCodeTxtField: UITextField!
    @IBOutlet weak var applyButton: UIButton!
    
    @IBOutlet weak var promoCollectionViewContainer: UIView!
    lazy var promoCodesArray = Array<PromotionCodeDetails>()
    lazy var selectedAmount = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        showShadows()
        let totalCash = (Float(UserDetails.sharedInstance.unusedAmount) ?? 0) + (Float(UserDetails.sharedInstance.totalWinningCash) ?? 0)
        lblCurrentBalance.text = "₹\(Int(totalCash))"
        amountTxtField.text = "\(selectedAmount)"
    }
    
    func showShadows() {
              
        containerView.appbackgroudColor()
        containerView.topViewCorners()
//        promoCollectionViewContainer.bottomTabShadow()
        promoCollectionViewContainer.appbackgroudColor()
        
        bottomView.bottomTabShadow()
        
        button100.gradient()
        button300.gradient()
        button500.gradient()
        addCashButton.gradient()
        applyButton.gradient()

        enterPromoCodeView.layer.cornerRadius = 5
        enterPromoCodeView.layer.borderColor = UIColor.appbackgroudColor().cgColor
        enterPromoCodeView.layer.borderWidth = 1.0
        
        enterAmountView.layer.cornerRadius = 5
        enterAmountView.layer.borderColor = UIColor.appbackgroudColor().cgColor
        enterAmountView.layer.borderWidth = 1.0
        
        collectionView.register(UINib(nibName: "PromoCodeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PromoCodeCollectionViewCell")
        callGetUserPrmomotion(isShowLoader: true)
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCashButtonTapped(_ sender: Any) {
        
        amountTxtField.resignFirstResponder()
        promoCodeTxtField.resignFirstResponder()

        let paymentOptionsVC = storyboard?.instantiateViewController(withIdentifier: "PaymentMethodsViewController") as! PaymentMethodsViewController
        paymentOptionsVC.selectedAmount = selectedAmount
        navigationController?.pushViewController(paymentOptionsVC, animated: true)
    }
    
    
    func callGetUserPrmomotion(isShowLoader: Bool) {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        if isShowLoader{
            AppHelpers.sharedInstance.displayLoader()
        }
    
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: kGetPromocodesURL) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                if status == "200"{
                    if let promocodesArray = result!["promocodes"].array {
                        weakSelf?.promoCodesArray = PromotionCodeDetails.getPromoCodeArray(dataArray: promocodesArray)
                    }
                    
                    UserDetails.sharedInstance.parseUserDetails(response: result!["userDetails"])

                    weakSelf?.collectionView.reloadData()
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
    
    @IBAction func applyButtonTapped(_ sender: Any) {
    
    }
    
    @IBAction func button100Tapped(_ sender: Any) {
        
        selectedAmount = selectedAmount + 100
        amountTxtField.text = "\(selectedAmount)"
    }
    
    @IBAction func button300Tapped(_ sender: Any) {
        
        selectedAmount = selectedAmount + 300
        amountTxtField.text = "\(selectedAmount)"
    }
    
    @IBAction func button500Tapped(_ sender: Any) {
    
        selectedAmount = selectedAmount + 500
        amountTxtField.text = "\(selectedAmount)"
    }
    
    // MARK:- Text Filed Delegate
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == amountTxtField {
            selectedAmount = Int(amountTxtField.text ?? "0") ?? 0
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if textField == amountTxtField {
            if string == "" {
                return true;
            }
            else if string == " " {
               return false
            }
           
            let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
           
            if (textField.text?.count)! < 10 {
               return isNumber
            }
            return false
        }

        return true
    }
    
}

extension AddCashViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promoCodesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if promoCodesArray.count == 1{
            return CGSize(width: collectionView.frame.width , height: 100)
        }
        return CGSize(width: 300, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromoCodeCollectionViewCell", for: indexPath) as! PromoCodeCollectionViewCell
        
        let details = promoCodesArray[indexPath.item]
        cell.configData(details: details)
        
        return cell
    }
    
}
