//
//  LeagueTeamsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 21/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class LeagueTeamsViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    var selectedMatchDetails: MatchDetails?
    var selectedLeagueDetails: LeagueDetails?
    
    lazy var myTeamsArray = Array<JoinedLeaguesTeamsDetails>()
    lazy var otherTeamsArray = Array<JoinedLeaguesTeamsDetails>()
    
    lazy var matchType = MatchType.Cricket.rawValue
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        containerView.topViewCorners()
        refreshControl.addTarget(self, action: #selector(refreshPlayerPoints), for: UIControl.Event.valueChanged)
        tblView.addSubview(refreshControl)
        tblView.alwaysBounceVertical = true;
        
        tblView.register(UINib(nibName: "TeamsRanksTableViewCell", bundle: nil), forCellReuseIdentifier: "TeamsRanksTableViewCell")
        tblView.tableFooterView = UIView()
        callGetLeagueTeams(isNeedToShowLoader: true)
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func refreshPlayerPoints(){
        refreshControl.endRefreshing()
        callGetLeagueTeams(isNeedToShowLoader: true)
    }
    
    //MARK:- API Related Methods
    func callGetLeagueTeams(isNeedToShowLoader: Bool)  {
        guard let matchDtails = selectedMatchDetails else { return}
        guard let details = selectedLeagueDetails else { return}

        if isNeedToShowLoader {
            if !AppHelpers.isInterNetConnectionAvailable(){
                return;
            }
            AppHelpers.sharedInstance.displayLoader()
        }
        
        var urlString = ""
        if matchType == MatchType.Cricket.rawValue {
            urlString = kCricketLeagueJoinedTeams + matchDtails.matchID + "&leagueId=\(details.leagueId)"
        }
        
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: urlString) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()

            if result != nil{
                let statusCode = result!["code"].stringValue
                if statusCode == "200" {
                    if let teamsArray = result!["myTeams"].array  {
                        weakSelf?.myTeamsArray = JoinedLeaguesTeamsDetails.getAllJoinedLeagueTeamsArray(dataArray: teamsArray)
                    }

                    if let teamsArray = result!["otherTeams"].array  {
                        weakSelf?.otherTeamsArray = JoinedLeaguesTeamsDetails.getAllJoinedLeagueTeamsArray(dataArray: teamsArray)
                    }
                    weakSelf?.tblView.reloadData()
                }
            }
        }
    }
    
    func getTeamPlayer(teamID: String, userID: String)  {

        guard let details = selectedMatchDetails else { return}
        AppHelpers.sharedInstance.displayLoader()

        let params = ["matchId": details.matchID, "teamId": teamID, "userId": userID]

        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kTeamPlayersURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? kErrorMsg
                if statusCode == "200" {
                    var playerDetailsArray = Array<PlayerDetails>()
                    for index in 0 ..< 11{
                        if let playerDetails = TeamDetails.getPlayerDetails(key: "p\(index+1)", playersDetails: result!["players"]){
                            playerDetailsArray.append(playerDetails)
                        }
                    }
                    
                    let teamPreviewVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "CricketTeamPreviewViewController") as! CricketTeamPreviewViewController
                    teamPreviewVC.selectedMatchDetails = weakSelf?.selectedMatchDetails
                    teamPreviewVC.totalPlayerArray = playerDetailsArray
                    weakSelf?.navigationController?.pushViewController(teamPreviewVC, animated: true)
                }
                else{
                    AppHelpers.showCustomAlertView(message: message )
                }

            }
        }
    }
    
}


extension LeagueTeamsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if otherTeamsArray.count > 0 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 5, width: tableView.bounds.size.width, height: 24))
        titleLabel.textColor = UIColor.appBlackColor()
        titleLabel.font = UIFont(name: "Arial", size: 15)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        
        if section == 0{
            titleLabel.text = "My Teams"
        }
        else{
            titleLabel.text = "Others Teams"
        }
        
        titleLabel.textAlignment = .center
        return headerView
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return myTeamsArray.count
        }
        else if section == 1{
            return otherTeamsArray.count
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "TeamsRanksTableViewCell") as? TeamsRanksTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: "TeamsRanksTableViewCell") as? TeamsRanksTableViewCell
        }
        
        cell?.selectionStyle = .none
        if indexPath.section == 0 {
            let details = myTeamsArray[indexPath.row]
            cell?.configData(details: details, isMatchClosed: selectedMatchDetails?.isClosed ?? false, totoalWinners: Int(selectedLeagueDetails!.totalWinners) ?? 0, matchStatus: selectedMatchDetails?.matchStatus ?? "")
        }
        else{
            let details = otherTeamsArray[indexPath.row]
            cell?.configData(details: details, isMatchClosed: selectedMatchDetails?.isClosed ?? false, totoalWinners: Int(selectedLeagueDetails!.totalWinners) ?? 0, matchStatus: selectedMatchDetails?.matchStatus ?? "")
        }
        if selectedLeagueDetails?.leagueType == LeagueType.Practice.rawValue {
            cell?.lblWinningZone.isHidden = true
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let details = myTeamsArray[indexPath.row]
            getTeamPlayer(teamID: details.teamID, userID: details.userID)
        }
        else{
            let details = otherTeamsArray[indexPath.row]
            getTeamPlayer(teamID: details.teamID, userID: details.userID)
        }
    }
}



