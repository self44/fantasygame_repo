//
//  CricketTeamPreviewViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class CricketTeamPreviewViewController: UIViewController {

    @IBOutlet weak var lblTotalCredit: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTotalCreditTitle: UILabel!
    @IBOutlet weak var batsmanView: UIView!
    @IBOutlet weak var wicketKeeperView: UIView!
    @IBOutlet weak var bowlerView: UIView!
    @IBOutlet weak var allRounderView: UIView!
    @IBOutlet weak var seeAllButton: UIButton!
    
    var totalPlayerArray = Array<PlayerDetails>()
    var selectedMatchDetails: MatchDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        containerView.topViewCorners()
        showPlayerPoints()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func seeAllButtonTapped(_ sender: Any) {
        let teamPreviewVC = storyboard?.instantiateViewController(withIdentifier: "Playing22ViewController") as! Playing22ViewController
        teamPreviewVC.selectedMatchDetails = selectedMatchDetails
        navigationController?.pushViewController(teamPreviewVC, animated: true)
    }
    
    func showPlayerPoints() {
        
        guard let matchDetails = selectedMatchDetails else {
            return;
        }
        
        if matchDetails.isClosed {
            seeAllButton.isHidden = false
        }
        else{
            seeAllButton.isHidden = true
        }
        
        let wicketPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            return playerDetails.playerRole == PlayerRole.WicketKeeper.rawValue
        })
        
        let batsmanPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            return playerDetails.playerRole == PlayerRole.Batsman.rawValue
        })
        
        let bowlerPlayersArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            return playerDetails.playerRole == PlayerRole.Bowler.rawValue
        })
        
        let allRounderPlayerArray = totalPlayerArray.filter({ (playerDetails) -> Bool in
            return playerDetails.playerRole == PlayerRole.AllRounder.rawValue
        })
        
        
        var viewWidth = UIScreen.main.bounds.width/5 - 8;

        if (batsmanPlayersArray.count >= 6) || (bowlerPlayersArray.count >= 6){
            viewWidth = UIScreen.main.bounds.width/6 - 10;
        }

        var totalPoint: Float = 0
        if wicketPlayersArray.count == 0{
            wicketKeeperView.isHidden = true
        }
        else if wicketPlayersArray.count == 1{
            let playerInfoView = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            let deatails = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: deatails, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            wicketKeeperView.addSubview(playerInfoView)
        }
        else if wicketPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            wicketKeeperView.addSubview(playerInfoView1)
            wicketKeeperView.addSubview(playerInfoView2)
        }
        else if wicketPlayersArray.count == 3{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 10))
            
                        
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details3 = wicketPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            wicketKeeperView.addSubview(playerInfoView1)
            wicketKeeperView.addSubview(playerInfoView2)
            wicketKeeperView.addSubview(playerInfoView3)
        }
        else if wicketPlayersArray.count == 4{
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = wicketPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = wicketPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details3 = wicketPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details4 = wicketPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            wicketKeeperView.addSubview(playerInfoView1)
            wicketKeeperView.addSubview(playerInfoView2)
            wicketKeeperView.addSubview(playerInfoView3)
            wicketKeeperView.addSubview(playerInfoView4)
        }
        
        if batsmanPlayersArray.count == 0{
            batsmanView.isHidden = true
        }
        else if batsmanPlayersArray.count == 1{
            let playerInfoView = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            batsmanView.addSubview(playerInfoView)
        }
        else if batsmanPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))

            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            batsmanView.addSubview(playerInfoView1)
            batsmanView.addSubview(playerInfoView2)
        }
        else if batsmanPlayersArray.count == 3{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            
            batsmanView.addSubview(playerInfoView1)
            batsmanView.addSubview(playerInfoView2)
            batsmanView.addSubview(playerInfoView3)
        }
        else if batsmanPlayersArray.count == 4{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
                let details1 = batsmanPlayersArray[0]
                totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

                let details2 = batsmanPlayersArray[1]
                totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
                
                let details3 = batsmanPlayersArray[2]
                totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
                
                let details4 = batsmanPlayersArray[3]
                totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            batsmanView.addSubview(playerInfoView1)
            batsmanView.addSubview(playerInfoView2)
            batsmanView.addSubview(playerInfoView3)
            batsmanView.addSubview(playerInfoView4)
            
        }
        else if batsmanPlayersArray.count == 5{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView5 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = batsmanPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = batsmanPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details3 = batsmanPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details4 = batsmanPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details5 = batsmanPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: details5, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            batsmanView.addSubview(playerInfoView1)
            batsmanView.addSubview(playerInfoView2)
            batsmanView.addSubview(playerInfoView3)
            batsmanView.addSubview(playerInfoView4)
            batsmanView.addSubview(playerInfoView5)
            
        }
        else if batsmanPlayersArray.count == 6{
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*3 - 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
             
             let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - 2*viewWidth - 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
             
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
            
             let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))

             let playerInfoView5 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth + 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView6 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + 2*viewWidth + 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))

             let details1 = batsmanPlayersArray[0]
             totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

             let details2 = batsmanPlayersArray[1]
             totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
             
             let details3 = batsmanPlayersArray[2]
             totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
             
             let details4 = batsmanPlayersArray[3]
             totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details5 = batsmanPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: details5, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details6 = batsmanPlayersArray[5]
            totalPoint = totalPoint + playerInfoView6.showPlayerInformation(details: details6, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            
            batsmanView.addSubview(playerInfoView1)
            batsmanView.addSubview(playerInfoView2)
            batsmanView.addSubview(playerInfoView3)
            batsmanView.addSubview(playerInfoView4)
            batsmanView.addSubview(playerInfoView5)
            batsmanView.addSubview(playerInfoView6)

        }
        
        if bowlerPlayersArray.count == 0{
            bowlerView.isHidden = true
        }
        else if bowlerPlayersArray.count == 1{
            
            let playerInfoView = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            bowlerView.addSubview(playerInfoView)
        }
        else if bowlerPlayersArray.count == 2{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
        }
        else if bowlerPlayersArray.count == 3{
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
        }
        else if bowlerPlayersArray.count == 4{
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))

            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details4 = bowlerPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            
        }
        else if bowlerPlayersArray.count == 5{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5 - 8) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5 - 4) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2 + 4) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView5 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5 + 8) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = bowlerPlayersArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = bowlerPlayersArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details3 = bowlerPlayersArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details4 = bowlerPlayersArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details5 = bowlerPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: details5, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
        }
        else if bowlerPlayersArray.count == 6{
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*3 - 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
             
             let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - 2*viewWidth - 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
             
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
            
             let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))

             let playerInfoView5 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth + 3*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView6 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + 2*viewWidth + 5*viewWidth/14) , y: 15, width: viewWidth, height: viewWidth + 10))

             let details1 = bowlerPlayersArray[0]
             totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

             let details2 = bowlerPlayersArray[1]
             totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
             
             let details3 = bowlerPlayersArray[2]
             totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
             
             let details4 = bowlerPlayersArray[3]
             totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            let details5 = bowlerPlayersArray[4]
            totalPoint = totalPoint + playerInfoView5.showPlayerInformation(details: details5, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details6 = bowlerPlayersArray[5]
            totalPoint = totalPoint + playerInfoView6.showPlayerInformation(details: details6, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            
            bowlerView.addSubview(playerInfoView1)
            bowlerView.addSubview(playerInfoView2)
            bowlerView.addSubview(playerInfoView3)
            bowlerView.addSubview(playerInfoView4)
            bowlerView.addSubview(playerInfoView5)
            bowlerView.addSubview(playerInfoView6)

        }
        
        if allRounderPlayerArray.count == 0{
            allRounderView.isHidden = true
        }
        else if allRounderPlayerArray.count == 1{
            
            let playerInfoView = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            allRounderView.addSubview(playerInfoView)
        }
        else if allRounderPlayerArray.count == 2{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
                        
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            
        }
        else if allRounderPlayerArray.count == 3{
            
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth/2) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
        }
        else if allRounderPlayerArray.count == 4{
            let playerInfoView1 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth*2.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView2 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 - viewWidth - viewWidth/6) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView3 = PlayerPointsView(frame: CGRect(x: UIScreen.main.bounds.width/2 + viewWidth/6 , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let playerInfoView4 = PlayerPointsView(frame: CGRect(x: (UIScreen.main.bounds.width/2 + viewWidth*1.5) , y: 15, width: viewWidth, height: viewWidth + 10))
            
            let details1 = allRounderPlayerArray[0]
            totalPoint = totalPoint + playerInfoView1.showPlayerInformation(details: details1, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details2 = allRounderPlayerArray[1]
            totalPoint = totalPoint + playerInfoView2.showPlayerInformation(details: details2, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details3 = allRounderPlayerArray[2]
            totalPoint = totalPoint + playerInfoView3.showPlayerInformation(details: details3, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)

            let details4 = allRounderPlayerArray[3]
            totalPoint = totalPoint + playerInfoView4.showPlayerInformation(details: details4, gameType: MatchType.Cricket.rawValue, isMatchClosed: matchDetails.isClosed, team1Name: matchDetails.team1ShortName)
            
            allRounderView.addSubview(playerInfoView1)
            allRounderView.addSubview(playerInfoView2)
            allRounderView.addSubview(playerInfoView3)
            allRounderView.addSubview(playerInfoView4)
        }
        
        lblTotalCredit.text = String(totalPoint)
    }

}
