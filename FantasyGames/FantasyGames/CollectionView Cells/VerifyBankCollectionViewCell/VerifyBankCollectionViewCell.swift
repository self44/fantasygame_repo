//
//  VerifyBankCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 11/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import CropViewController
import SwiftyJSON

class VerifyBankCollectionViewCell: UICollectionViewCell, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, CropViewControllerDelegate {
   
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtFieldAccountNumber: UITextField!
    @IBOutlet weak var txtFieldIFSCCode: UITextField!
    @IBOutlet weak var txtFieldBankName: UITextField!
    @IBOutlet weak var txtFieldBranchName: UITextField!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
   
    var selectedImage: UIImage?

    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if txtFieldAccountNumber.isEmpty() {
            AppHelpers.showCustomErrorAlertView(message: "Please enter your account number")
            return;
        }
        else if txtFieldIFSCCode.isEmpty() {
            AppHelpers.showCustomErrorAlertView(message: "Please enter your IFSC Code")
            return;
        }
        else if txtFieldBankName.isEmpty() || txtFieldBranchName.isEmpty() {
            AppHelpers.showCustomErrorAlertView(message: "Please enter corrct IFSC Code")
            return;
        }
        callUpdateBankInformation()
    }
    
    @IBAction func cameraButtonTapped(_ sender: Any) {
           
           weak var weakSelf = self
           let actionSheet: UIAlertController = UIAlertController(title: "Please choose an option", message: nil, preferredStyle: .actionSheet)
           
           let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
           }
           actionSheet.addAction(cancelActionButton)
           
           let saveActionButton = UIAlertAction(title: "Camera", style: .default)
           { _ in
               weakSelf?.cameraButtonTapped()
           }
           actionSheet.addAction(saveActionButton)
           
           let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
           { _ in
               weakSelf?.galleryButtonTapped()
           }
           actionSheet.addAction(deleteActionButton)
           
           if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
               navigationVC.present(actionSheet, animated: true, completion: nil)
           }
       }
       

       //MARK:- Image Picker Related Methods
      
       func galleryButtonTapped() {
          
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.sourceType = .photoLibrary
           if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
               navigationVC.present(imagePicker, animated: true, completion: nil)
           }
       }
      
       func cameraButtonTapped() {
          
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.sourceType = .camera
           if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
               navigationVC.present(imagePicker, animated: true, completion: nil)
           }
       }
      
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
           picker.dismiss(animated: true, completion: nil)
           selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
           presentCropViewController()
       }

       func presentCropViewController() {
          
           let cropViewController = CropViewController(croppingStyle: .default, image: selectedImage!)
           cropViewController.aspectRatioPreset = .preset16x9
           cropViewController.delegate = self
           if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
               navVC.present(cropViewController, animated: true, completion: nil)
           }
       }
      
       func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
           selectedImage = image
           imgView.image = selectedImage
           if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
               navVC.dismiss(animated: true, completion: nil)
           }
      }

       func callUpdateBankInformation() {
           if !AppHelpers.isInterNetConnectionAvailable() {
               return
           }
           
           AppHelpers.sharedInstance.displayLoader()
           let params = ["ifscCode": txtFieldIFSCCode.text!, "bankName": txtFieldBankName.text!, "branchName": txtFieldBranchName.text!, "accountNumber": txtFieldAccountNumber.text!]
           guard let imageData = selectedImage!.jpegData(compressionQuality: 0.1) else { return }
           
           WebServiceHandler.performMultipartRequest(urlString: kSaveBankDetailsURL, fileName: "image", params: params, imageData: imageData, accessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
               AppHelpers.sharedInstance.removeLoader()
                 if result != nil{
                     let status = result!["code"].stringValue
                     let message = result!["message"].string
                     if status == "200"{
                       AppHelpers.showCustomAlertView(message: message ?? "Bank details uploaded successfully")
                     }
                     else{
                         AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                     }
                 }
                 else{
                     AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
                 }
           }
       }
       
    func configData() {
        photoView.layer.cornerRadius = 5.0
        photoView.layer.borderColor = UIColor.appbackgroudColor().cgColor
        
        txtFieldBankName.isEnabled = false
        txtFieldBranchName.isEnabled = false
        submitButton.gradient()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFieldIFSCCode {
            if textField.text!.count == 10{
                if string.count != 0{
                    getIFSCCode(ifscCode: txtFieldIFSCCode.text! + string)
                }
                else{
                    txtFieldBankName.text = ""
                    txtFieldBranchName.text = ""
                }
            }
            if textField.text!.count > 10{
                if string.count != 0{
                    return false
                }
            }
        }
        else{
            if (textField.text!.count > 19) && (string.count != 0){
                return false
            }
        }
        
        return true

    }
    
    
    // MARK:- API Releated Methods
    
    func getIFSCCode(ifscCode: String) {
        if !AppHelpers.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelpers.sharedInstance.displayLoader()
        weak var weakSelf = self
        
        WebServiceHandler.performGETRequest(withURL: kGetIFSCCodeURL + ifscCode) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                if status == "200"{
                    if let responseStr = result!["ifscDetails"].string{
                        let response = JSON(parseJSON: responseStr)
                        if let bankName = response["BANK"].string{
                            weakSelf?.txtFieldBankName.text = bankName
                        }
                        
                        if let branchName = response["BRANCH"].string{
                            weakSelf?.txtFieldBranchName.text = branchName
                        }

                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
