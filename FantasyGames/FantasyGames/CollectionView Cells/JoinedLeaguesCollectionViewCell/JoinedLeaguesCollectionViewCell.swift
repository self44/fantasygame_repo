//
//  JoinedLeaguesCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 21/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class JoinedLeaguesCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    
    lazy var leagueDetailsArray = Array<LeagueDetails>()
    var selectMatchDetails: MatchDetails?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configData(matchDetails: MatchDetails?, leaguesArray: Array<LeagueDetails>) {
        
        leagueDetailsArray = leaguesArray
        selectMatchDetails = matchDetails
        tblView.register(UINib(nibName: "JoinedLeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "JoinedLeagueTableViewCell")
        tblView.appbackgroudColor()
        tblView.reloadData()
    }
    
    //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leagueDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "JoinedLeagueTableViewCell") as? JoinedLeagueTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "JoinedLeagueTableViewCell") as? JoinedLeagueTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        cell?.winnersButton.tag = indexPath.row;
        cell?.winnersButton.addTarget(self, action: #selector(winnersButtonTapped(button:)), for: .touchUpInside)
        cell?.contentView.appbackgroudColor()
        let details = leagueDetailsArray[indexPath.row]
        cell?.configData(details: details, matchDetails: selectMatchDetails!)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        guard let matchDetails = selectMatchDetails else {
            return;
        }

        let storybaord = UIStoryboard(name: "Main", bundle: nil)
        let leagueVC = storybaord.instantiateViewController(withIdentifier: "LeagueTeamsViewController") as! LeagueTeamsViewController
        leagueVC.selectedMatchDetails = matchDetails
        leagueVC.selectedLeagueDetails = leagueDetailsArray[indexPath.row]
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(leagueVC, animated: true)
        }
    }
    
    @objc func winnersButtonTapped(button: UIButton) {
        let leagueDetails = leagueDetailsArray[button.tag]

        if leagueDetails.leagueType == LeagueType.Practice.rawValue {
            return
        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let leagueWinnerVC = storyboard.instantiateViewController(withIdentifier: "LeagueWinnersListViewController") as! LeagueWinnersListViewController
        leagueWinnerVC.leagueDetails = leagueDetails
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.present(leagueWinnerVC, animated: true, completion: nil)
        }
    }
}

