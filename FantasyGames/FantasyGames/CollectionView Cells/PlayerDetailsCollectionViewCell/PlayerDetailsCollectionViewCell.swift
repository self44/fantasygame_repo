//
//  PlayerDetailsCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class PlayerDetailsCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var innerView: UIView!
    
    var playerDetails: PlayerDetails?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configData(details: PlayerDetails) {
        innerView.cellShadow()
        playerDetails = details
        tblView.register(UINib(nibName: "PlayerPointsTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerPointsTableViewCell")
        tblView.register(UINib(nibName: "PlayerInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerInfoTableViewCell")
        tblView.reloadData()
    }
}


extension PlayerDetailsCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    
   //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard playerDetails != nil else {
            return 0
        }
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 116
        }
        return 50
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let details = playerDetails else {
            return UITableViewCell()
        }

        if indexPath.row == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "PlayerInfoTableViewCell") as? PlayerInfoTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "PlayerInfoTableViewCell") as? PlayerInfoTableViewCell
            }
            cell?.selectionStyle = .none
            cell?.lblPlayerName.text = details.playerName
            cell?.lblPlayerRole.text = details.playerRole

            cell?.lblPlayerCredits.text = details.playerCredits
            cell?.lblPlayerTotalPoints.text = details.playerScore
            var placeholder = "MalePlayerPhotoPlaceholder"
            if details.gender == "f" {
                placeholder = "FemalePlayerPhotoPlaceholder"
            }

            if details.playerImage.count > 0 {
                if let url = URL(string: details.playerImage){
                    cell?.playerImgView.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                        // Report progress
                    }, completion: { [weak self] image in
                        if (image != nil){
                            cell?.playerImgView.image = image
                        }
                        else{
                            cell?.playerImgView.image = UIImage(named: placeholder)
                        }
                    })
                }
            }
            else{
                cell?.playerImgView.image = UIImage(named: placeholder)
            }

            
            return cell!
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "PlayerPointsTableViewCell") as? PlayerPointsTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "PlayerPointsTableViewCell") as? PlayerPointsTableViewCell
            }
            cell?.selectionStyle = .none
                        
            cell?.lblPoints.text = ""
            if indexPath.row == 1 {
                cell?.lblEventType.text = "Playing 11"
                if Float(details.pointsStarting) ?? 0 > 0 {
                    cell?.lblActualScore.text = "YES"
                }
                cell?.lblPoints.text = details.pointsStarting
            }
            else if indexPath.row == 2 {
                cell?.lblEventType.text = "Runs"
                cell?.lblActualScore.text = details.battingRuns
                cell?.lblPoints.text = details.pointsBattingRuns
            }
            else if indexPath.row == 3 {
                cell?.lblEventType.text = "4's"
                cell?.lblActualScore.text = details.battingFours
                cell?.lblPoints.text = details.pointsBoundaryBonus
            }
            else if indexPath.row == 4 {
                cell?.lblEventType.text = "6's"
                cell?.lblActualScore.text = details.battingSixes
                cell?.lblPoints.text = details.pointsBattingSixes
            }
            else if indexPath.row == 5 {
                cell?.lblEventType.text = "50"
                if (details.pointsBatting100 == "") || (details.pointsBatting100 == "0"){
                    cell?.lblActualScore.text = String((Int(details.battingRuns) ?? 0)/50)
                    cell?.lblPoints.text = details.pointsBatting50
                }
                else{
                    cell?.lblActualScore.text = "0"
                    cell?.lblPoints.text = "0"
                }
            }
            else if indexPath.row == 6 {
                cell?.lblEventType.text = "100"
                cell?.lblActualScore.text = String((Int(details.battingRuns) ?? 0)/100)
                cell?.lblPoints.text = details.pointsBatting100
            }
            else if indexPath.row == 7 {
                cell?.lblEventType.text = "S/R"
                cell?.lblActualScore.text = details.battingRate
                cell?.lblPoints.text = details.pointsBattingRate
            }
            else if indexPath.row == 8 {
                cell?.lblEventType.text = "Duck"
                if (Int(details.pointsBattingDuck) ?? 0 > 0) {
                    cell?.lblActualScore.text = "YES"
                }
                else{
                    cell?.lblActualScore.text = "NO"
                }
                
                cell?.lblPoints.text = details.pointsBattingDuck
            }
            else if indexPath.row == 9 {
                cell?.lblEventType.text = "Wkts"
                cell?.lblActualScore.text = details.bowlingWickets
                cell?.lblPoints.text = details.pointsBowlingWickets
            }
            else if indexPath.row == 10 {
                cell?.lblEventType.text = "Maiden Over"
                cell?.lblActualScore.text = details.bowlingMaidens
                cell?.lblPoints.text = details.pointsBowlingMaidens
            }
            else if indexPath.row == 11 {
                cell?.lblEventType.text = "E/R"
                cell?.lblActualScore.text = details.bowlingEconomy
                cell?.lblPoints.text = details.pointsBowlingEconomy
            }
            else if indexPath.row == 12 {
                cell?.lblEventType.text = "Bonus"
                cell?.lblActualScore.text = details.bowlingEconomy
                if (details.pointsBowlingWicket5 != "") && (details.pointsBowlingWicket5 != "0") {
                    cell?.lblPoints.text = details.pointsBowlingWicket5
                }
                else{
                    cell?.lblPoints.text = details.pointsBowlingWicket4
                }
//                cell?.lblPoints.text = "Remaining"
            }
            else if indexPath.row == 13 {
                cell?.lblEventType.text = "Catch"
                cell?.lblActualScore.text = details.fieldingCatch
                cell?.lblPoints.text = details.pointsFieldingCatch
            }
            else if indexPath.row == 14 {
                cell?.lblEventType.text = "Run Out/Stumping"
                cell?.lblActualScore.text = details.fieldingRunout + "/" + details.fieldingStumped
                cell?.lblPoints.text = details.pointsFieldingRunout
            }
            return cell!
        }
    }
       
    
}
