//
//  PromoCodeCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 09/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class PromoCodeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var lblExpiry: UILabel!
    
    @IBOutlet weak var innerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(details: PromotionCodeDetails) {
        
        innerView.cellShadow()
        
        lblPromoCode.text = details.promoCode
        lblMessage.text = details.message
        lblExpiry.text = "Valid till: " + details.expiryDate
        
    }

}
