//
//  VerifyPanDetailsCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 11/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import CropViewController

class VerifyPanDetailsCollectionViewCell: UICollectionViewCell, UIImagePickerControllerDelegate,UINavigationControllerDelegate, CropViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
   
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldPanCardNumber: UITextField!
    @IBOutlet weak var txtFieldDob: UITextField!
    @IBOutlet weak var txtFieldState: UITextField!
    @IBOutlet weak var photoView: UIView!
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
   
    let statsArray = AppHelpers.getAllState()
    
    lazy var selectedDate = ""
    let datePicker = UIDatePicker()
    let statePicker = UIPickerView()

    var selectedImage: UIImage?

    func configData() {
        photoView.layer.cornerRadius = 5.0
        photoView.layer.borderColor = UIColor.appbackgroudColor().cgColor
        
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        txtFieldDob.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.maximumDate = date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        submitButton.gradient()
        statePicker.delegate = self;
        txtFieldState.inputView = statePicker
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        selectedDate = dateFormatter.string(from: sender.date)
        txtFieldDob.text = AppHelpers.getFormatedDateWithoutTime(dateString: selectedDate)
    }

    
    @IBAction func submitButtonTapped(_ sender: Any) {
    
        if txtFieldName.isEmpty() {
            AppHelpers.showCustomErrorAlertView(message: "Please enter your pan name")
            return
        }
        else if txtFieldPanCardNumber.isEmpty() {
            AppHelpers.showCustomErrorAlertView(message: "Please enter your pan number")
            return
        }
        else if selectedDate.count == 0 {
            AppHelpers.showCustomErrorAlertView(message: "Please enter select your date of birth")
            return
        }
        else if selectedImage == nil {
            AppHelpers.showCustomErrorAlertView(message: "Please upload your PAN image too")
            return
        }
        
//        else if txtFieldState.isEmpty() {
//            AppHelpers.showCustomErrorAlertView(message: "Please enter select your state")
//            return
//        }
//

        callUpdatePanInformation()
    }
    
    @IBAction func cameraButtonTapped(_ sender: Any) {
        
        weak var weakSelf = self
        let actionSheet: UIAlertController = UIAlertController(title: "Please choose an option", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheet.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            weakSelf?.cameraButtonTapped()
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            weakSelf?.galleryButtonTapped()
        }
        actionSheet.addAction(deleteActionButton)
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(actionSheet, animated: true, completion: nil)
        }
    }
    

    //MARK:- Image Picker Related Methods
   
    func galleryButtonTapped() {
       
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
   
    func cameraButtonTapped() {
       
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(imagePicker, animated: true, completion: nil)
        }
    }
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        presentCropViewController()
    }

    func presentCropViewController() {
       
        let cropViewController = CropViewController(croppingStyle: .default, image: selectedImage!)
        cropViewController.aspectRatioPreset = .preset16x9
        cropViewController.delegate = self
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.present(cropViewController, animated: true, completion: nil)
        }
    }
   
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
       
        selectedImage = image
        imgView.image = selectedImage
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.dismiss(animated: true, completion: nil)
        }
   }

    func callUpdatePanInformation() {
        if !AppHelpers.isInterNetConnectionAvailable() {
            return
        }
        
        let params = ["panName": txtFieldName.text!, "panDOB": selectedDate, "panNumber": txtFieldPanCardNumber.text!]
        guard let imageData = selectedImage!.jpegData(compressionQuality: 0.5) else { return }
        AppHelpers.sharedInstance.displayLoader()

        WebServiceHandler.performMultipartRequest(urlString: kSavePanDetailsURL, fileName: "image", params: params, imageData: imageData, accessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
              if result != nil{
                  let status = result!["code"].stringValue
                  let message = result!["message"].string
                  if status == "200"{
                    AppHelpers.showCustomAlertView(message: message ?? "Pan details uploaded successfully")
                  }
                  else{
                      AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                  }
              }
              else{
                  AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
              }
        }
    }
    

    
    //MARK:- Picker View Data Source and Delegates
       func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           return statsArray.count
       }
       
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           let details = statsArray[row]
           return details["stateName"].string
       }
       
       func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           let details = statsArray[row]
           txtFieldState.text = details["stateName"].string
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
