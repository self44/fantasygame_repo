//
//  JoinedContestCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 04/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class JoinedContestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tblView: UITableView!
    
    lazy var liveMatchsArray = Array<MatchDetails>()
    lazy var upcomingMatchsArrays = Array<MatchDetails>()
    lazy var completedMatchsArrays = Array<MatchDetails>()
    lazy var selectedGameTypeTab = MatchType.Cricket.rawValue
    let refreshControl = UIRefreshControl()
    var timer: Timer?

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
        
    func configData(liveArray: Array<MatchDetails>?,upcomingArray: Array<MatchDetails>?, completedArray: Array<MatchDetails>?) {

        tblView.register(UINib(nibName: "UpcomingMatchsTableViewCell", bundle: nil), forCellReuseIdentifier: "UpcomingMatchsTableViewCell")
            
        tblView.appbackgroudColor()

        if liveArray != nil {
            liveMatchsArray = liveArray!
        }
        
        if upcomingArray != nil {
            upcomingMatchsArrays = upcomingArray!
        }

        if completedArray != nil {
            completedMatchsArrays = completedArray!
        }
        
        tblView.reloadData()
        
        refreshControl.addTarget(self, action: #selector(refreshMatches), for: UIControl.Event.valueChanged)
        tblView.addSubview(refreshControl)
        
        if upcomingArray?.count != 0 {
            startTimer()
        }

    }
    
    // MARK:- Timer Handlers Methods
    
    func startTimer()  {
        refereshMatchClosingTime()
        weak var weakSelf = self
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.refereshMatchClosingTime()
        }
    }

    @objc func refereshMatchClosingTime()  {
        let visibleCellsArray = tblView.visibleCells
        for visibleCell  in visibleCellsArray{
            if let cell = visibleCell as? UpcomingMatchsTableViewCell{
                if cell.tag > upcomingMatchsArrays.count {
                    continue
                }
                let details = upcomingMatchsArrays[cell.tag]
                let raminingTime = AppHelpers.getMatchClosingTime(matchTimestamp: details.startDateUnix)
                
                if raminingTime.count == 0{
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeUpcomingMatchFromLocal"), object: details)
                }
                cell.configData(details: details, contestType: ContestType.UpcomingContest.rawValue)
            }
        }
    }
    
    @objc func refreshMatches() {
        refreshControl.endRefreshing()
        NotificationCenter.default.post(name: .myContestTabTappedNotification, object: nil)
    }
}

extension JoinedContestCollectionViewCell: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if ((liveMatchsArray.count != 0) && (upcomingMatchsArrays.count != 0) && (completedMatchsArrays.count != 0)){
            return 3
        }
        else if ((liveMatchsArray.count != 0) && (upcomingMatchsArrays.count != 0)){
            return 2
        }
        else if ((upcomingMatchsArrays.count != 0) && (completedMatchsArrays.count != 0)){
            return 2
        }
        else if ((liveMatchsArray.count != 0) && (completedMatchsArrays.count != 0)){
            return 2
        }
        else if (liveMatchsArray.count != 0){
            return 1
        }
        else if (upcomingMatchsArrays.count != 0){
            return 1
        }
        else if (completedMatchsArrays.count != 0){
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            if liveMatchsArray.count != 0 {
                return liveMatchsArray.count
            }
            else if upcomingMatchsArrays.count != 0 {
                return upcomingMatchsArrays.count
            }
            else if completedMatchsArrays.count != 0 {
                return completedMatchsArrays.count
            }
        }
        else if section == 1{
            
            if (liveMatchsArray.count != 0) && (upcomingMatchsArrays.count != 0) {
                return upcomingMatchsArrays.count
            }
            else if completedMatchsArrays.count != 0 {
                return completedMatchsArrays.count
            }
        }
        else if section == 2{
            return completedMatchsArrays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40))
        let titleLabel = UILabel(frame: CGRect(x: 12, y: 10, width: tableView.bounds.size.width, height: 24))
        titleLabel.textColor = UIColor.appBlackColor()
        titleLabel.font = UIFont(name: "Arial-Bold", size: 15)
        headerView.appbackgroudColor()
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        
        if section == 0{
            if liveMatchsArray.count != 0 {
                if liveMatchsArray.count > 1{
                    titleLabel.text = "Live Matches"
                }
                else{
                    titleLabel.text = "Live Match"
                }
            }
            else if upcomingMatchsArrays.count != 0 {
                if upcomingMatchsArrays.count > 1{
                    titleLabel.text = "Upcoming Matches"
                }
                else{
                    titleLabel.text = "Upcoming Match"
                }
            }
            else if completedMatchsArrays.count != 0 {
                if completedMatchsArrays.count > 1{
                    titleLabel.text = "Completed Matches"
                }
                else{
                    titleLabel.text = "Completed Match"
                }

            }
        }
        else if section == 1{
            
            if (liveMatchsArray.count != 0) && (upcomingMatchsArrays.count != 0) {
                if upcomingMatchsArrays.count > 1{
                    titleLabel.text = "Upcoming Matches"
                }
                else{
                    titleLabel.text = "Upcoming Match"
                }
            }
            else if completedMatchsArrays.count != 0 {
                if completedMatchsArrays.count > 1{
                    titleLabel.text = "Completed Matches"
                }
                else{
                    titleLabel.text = "Completed Match"
                }
            }
        }
        else if section == 2{
            if completedMatchsArrays.count > 1{
                titleLabel.text = "Completed Matches"
            }
            else{
                titleLabel.text = "Completed Match"
            }
        }
        
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingMatchsTableViewCell") as? UpcomingMatchsTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "UpcomingMatchsTableViewCell") as? UpcomingMatchsTableViewCell
        }

        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        var mathchDetails: MatchDetails?
        var contestType = 0
        cell?.tag = 2100000;

        if indexPath.section == 0{
            if liveMatchsArray.count != 0 {
                mathchDetails = liveMatchsArray[indexPath.row]
                contestType = ContestType.LiveContest.rawValue
            }
            else if upcomingMatchsArrays.count != 0 {
                mathchDetails = upcomingMatchsArrays[indexPath.row]
                contestType = ContestType.UpcomingContest.rawValue
                cell?.tag = indexPath.row
            }
            else if completedMatchsArrays.count != 0 {
                mathchDetails = completedMatchsArrays[indexPath.row]
                contestType = ContestType.CompletedContest.rawValue
            }
        }
        else if indexPath.section == 1{
            
            if (liveMatchsArray.count != 0) && (upcomingMatchsArrays.count != 0) {
                mathchDetails = upcomingMatchsArrays[indexPath.row]
                contestType = ContestType.UpcomingContest.rawValue
                cell?.tag = indexPath.row
            }
            else if completedMatchsArrays.count != 0 {
                mathchDetails = completedMatchsArrays[indexPath.row]
                contestType = ContestType.CompletedContest.rawValue
            }
        }
        else if indexPath.section == 2{
            mathchDetails = completedMatchsArrays[indexPath.row]
            contestType = ContestType.CompletedContest.rawValue
        }
        
        if mathchDetails != nil{
            cell?.configData(details: mathchDetails!,contestType: contestType)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        var mathchDetails: MatchDetails?
        var isNeedToshowMyTeams = false
   

        if indexPath.section == 0{
            if liveMatchsArray.count != 0 {
                mathchDetails = liveMatchsArray[indexPath.row]
                isNeedToshowMyTeams = true
            }
            else if upcomingMatchsArrays.count != 0 {
                mathchDetails = upcomingMatchsArrays[indexPath.row]
                if mathchDetails?.isClosed ?? false{
                    isNeedToshowMyTeams = true
                }
            }
            else if completedMatchsArrays.count != 0 {
                isNeedToshowMyTeams = true
                mathchDetails = completedMatchsArrays[indexPath.row]
            }
        }
        else if indexPath.section == 1{
            
            if liveMatchsArray.count == 0 {
                isNeedToshowMyTeams = true
                mathchDetails = completedMatchsArrays[indexPath.row]
            }
            else if upcomingMatchsArrays.count != 0 {
                mathchDetails = upcomingMatchsArrays[indexPath.row]
                if mathchDetails?.isClosed ?? false{
                    isNeedToshowMyTeams = true
                }
            }
            else if completedMatchsArrays.count != 0 {
                isNeedToshowMyTeams = true
                mathchDetails = completedMatchsArrays[indexPath.row]
            }
        }
        else if indexPath.section == 2{
            mathchDetails = completedMatchsArrays[indexPath.row]
            isNeedToshowMyTeams = true
        }
        
        let joinedWinnerRankVC = storyboard.instantiateViewController(withIdentifier: "JoinedLeaguesViewController") as! JoinedLeaguesViewController
        joinedWinnerRankVC.matchType = selectedGameTypeTab
        joinedWinnerRankVC.matchDetails = mathchDetails
        joinedWinnerRankVC.isNeedToshowMyTeams = isNeedToshowMyTeams
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.pushViewController(joinedWinnerRankVC, animated: true)
        }
    }
}
