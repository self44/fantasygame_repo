//
//  UpcomingMatchesCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 07/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class UpcomingMatchesCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var tblView: UITableView!
    lazy var matchDetailsArray = Array<MatchDetails>()
    var timer: Timer?
    let refreshControl = UIRefreshControl()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configData(upcomingMatchArray: Array<MatchDetails>) {
        matchDetailsArray = upcomingMatchArray
        tblView.register(UINib(nibName: "UpcomingMatchsTableViewCell", bundle: nil), forCellReuseIdentifier: "UpcomingMatchsTableViewCell")
        tblView.appbackgroudColor()
        tblView.reloadData()

        refreshControl.addTarget(self, action: #selector(refreshMatches), for: UIControl.Event.valueChanged)
        tblView.addSubview(refreshControl)

        startTimer()
    }
    
    @objc func refreshMatches() {
        refreshControl.endRefreshing()
        NotificationCenter.default.post(name: .upcomingDataRefreshNotification, object: nil)
    }
}



extension UpcomingMatchesCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    
   //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40))
        let titleLabel = UILabel(frame: CGRect(x: 12, y: 10, width: tableView.bounds.size.width, height: 24))
        titleLabel.textColor = UIColor.appBlackColor()
        titleLabel.font = UIFont(name: "Arial-Bold", size: 15)
        headerView.appbackgroudColor()
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        titleLabel.text = "Select a Match"
        
        return headerView
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingMatchsTableViewCell") as? UpcomingMatchsTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "UpcomingMatchsTableViewCell") as? UpcomingMatchsTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        cell?.tag = indexPath.row
        let details = matchDetailsArray[indexPath.row]
        cell?.configData(details: details, contestType: ContestType.UpcomingContest.rawValue)
        return cell!
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let details = matchDetailsArray[indexPath.row]
        if details.matchStatus == MatchStatus.VisiblelOnly.rawValue{
            return;
        }
        let storybaord = UIStoryboard(name: "Main", bundle: nil)
        let leagueVC = storybaord.instantiateViewController(withIdentifier: "LeagueViewController") as! LeagueViewController
        leagueVC.matchDetails = details
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(leagueVC, animated: true)
        }
    }
    
    
    // MARK:- Timer Handlers Methods
    func startTimer()  {
        refereshMatchClosingTime()
        weak var weakSelf = self
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.refereshMatchClosingTime()
        }
    }

    @objc func refereshMatchClosingTime()  {
        let visibleCellsArray = tblView.visibleCells
        for visibleCell  in visibleCellsArray{
            if let cell = visibleCell as? UpcomingMatchsTableViewCell{
                let details = matchDetailsArray[cell.tag]
                let raminingTime = AppHelpers.getMatchClosingTime(matchTimestamp: details.startDateUnix)
                
                if raminingTime.count == 0{
//                    NotificationCenter.default.post(name: .removeClosedMatch, object: details.matchID)
                }
                cell.configData(details: details, contestType: ContestType.UpcomingContest.rawValue)
            }
        }
    }
    
}
