//
//  ProfileAvatarCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 04/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class ProfileAvatarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgUrl: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
