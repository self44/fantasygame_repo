//
//  CricketPlayerListCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 13/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class CricketPlayerListCollectionViewCell: UICollectionViewCell {
    
    lazy var playerListArray = Array<PlayerDetails>()
    lazy var selectedGameType = 0
    
    var selectedMathDetails: MatchDetails?
    lazy var selectedPlayerList = Array<PlayerDetails>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var tblView: UITableView!
    
    func configData(playerList: Array<PlayerDetails>, gameType: Int, mathDetails: MatchDetails, existingPlayerList: Array<PlayerDetails>)  {
        selectedMathDetails = mathDetails;
        selectedPlayerList = existingPlayerList
        playerListArray = playerList
        selectedGameType = gameType
        tblView.register(UINib(nibName: "PlayerDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerDetailsTableViewCell")
        tblView.reloadData()
    }    
    
    @objc func addPlayerButtonTapped(button: UIButton){
        let playerDetails = playerListArray[button.tag]
        updatePlayerSelectionArray(playerDetails: playerDetails)
    }
    
    func updatePlayerSelectionArray(playerDetails: PlayerDetails) {
        var isRecordRemoves = false;
        
        if playerDetails.isSelected {
            isRecordRemoves = true;
        }
        
        playerDetails.isSelected = !playerDetails.isSelected
        
        for i in (0..<selectedPlayerList.count)
        {
            let details = selectedPlayerList[i]
            if details.playerKey == playerDetails.playerKey {
                selectedPlayerList.remove(at: i)
                break
            }
        }
        
        if playerDetails.isSelected {
            selectedPlayerList.append(playerDetails)
        }
        
        if selectedGameType == MatchType.Cricket.rawValue {
            if let validationMsg = AppHelpers.validateCricketLeague(selectedPlayerList: selectedPlayerList,teamName: playerDetails.teamName, maxCredit: UserDetails.sharedInstance.maxCreditLimitForClassic, isRecordRemove: isRecordRemoves ){
               AppHelpers.showCustomErrorAlertView(message: validationMsg)
                if playerDetails.isSelected{
                    selectedPlayerList.removeLast()
                    playerDetails.isSelected = false
                }
            }
        }
        else if selectedGameType == MatchType.Kabaddi.rawValue {
         
            
        }
        else if selectedGameType == MatchType.Football.rawValue{
            
            
        }
        
        NotificationCenter.default.post(name: .playerSelectionUpdateNotification, object: nil)

        tblView.reloadData()
    }
    
    @objc func playerInfoButtonTapped(button: UIButton){

    }
    
}


extension CricketPlayerListCollectionViewCell: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 51.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return playerListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "PlayerDetailsTableViewCell") as? PlayerDetailsTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "PlayerDetailsTableViewCell") as? PlayerDetailsTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        cell?.addPlayerButton.tag = indexPath.row
        cell?.playerPointInfoButton.tag = indexPath.row
        cell?.addPlayerButton.addTarget(self, action: #selector(addPlayerButtonTapped(button:)), for: .touchUpInside)
        cell?.playerPointInfoButton.addTarget(self, action: #selector(playerInfoButtonTapped(button:)), for: .touchUpInside)
        cell?.contentView.backgroundColor = UIColor.white
        
        if indexPath.row < playerListArray.count {
            let details = playerListArray[indexPath.row]
            if details.isSelected{
                cell?.contentView.backgroundColor = UIColor.appbackgroudColor()
            }
            cell?.configDetails(details: details, gameType: selectedGameType)
        }

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerDetails = playerListArray[indexPath.row]
        updatePlayerSelectionArray(playerDetails: playerDetails)
    }
    
    
    func grayOutCricketClassicCell(cell: PlayerDetailsTableViewCell, details: PlayerDetails) {
        
        let minPointsPlayerDetails =  selectedPlayerList.sorted(by: { (nextPlayerDetails, playerDetails) -> Bool in
            return Float(nextPlayerDetails.playerCredits)! < Float(playerDetails.playerCredits)!
        })
        
        let totalPlayerFromTeamArray = selectedPlayerList.filter { (playerDetails) -> Bool in
            playerDetails.teamName == details.teamName
        }

        let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
            creditSum + Float(nextPlayerDetails.playerCredits)!
        })
        
        let maxCredit = UserDetails.sharedInstance.maxCreditLimitForClassic
        let remingPoints = maxCredit - totalPoints
        
        if totalPlayerFromTeamArray.count >= 7 {
            cell.grayOutView.isHidden = false

        }
        else if selectedPlayerList.count >= 11 {
            cell.grayOutView.isHidden = false
        }
        else if minPointsPlayerDetails.count == 0{
            // Do noting in this case
        }
        else if Float(minPointsPlayerDetails[0].playerCredits)! > remingPoints {
            cell.grayOutView.isHidden = false
        }
        else{
                        
            let totalWicketKeeperArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.WicketKeeper.rawValue
            }
            
            let totalBatsmenArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.Batsman.rawValue
            }
            
            let totalAllRounderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.AllRounder.rawValue
            }
            
            let totalBowlerArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.playerRole == PlayerRole.Bowler.rawValue
            }

            
            if (selectedPlayerList.count == 11) && (totalWicketKeeperArray.count == 0){
                cell.grayOutView.isHidden = false
            }
            else if (selectedPlayerList.count == 11) && (totalAllRounderArray.count == 0){
                cell.grayOutView.isHidden = false
            }
            else if (selectedPlayerList.count >= 7) && (totalBatsmenArray.count == 0) && (totalWicketKeeperArray.count == 0){
                if (details.playerRole != PlayerRole.WicketKeeper.rawValue) && (details.playerRole != PlayerRole.Batsman.rawValue){
                    cell.grayOutView.isHidden = false
                }
            }
            else if (selectedPlayerList.count >= 7) && (totalBatsmenArray.count == 0) && (totalAllRounderArray.count == 0){
                if (details.playerRole != PlayerRole.AllRounder.rawValue) && (details.playerRole != PlayerRole.Batsman.rawValue){
                    cell.grayOutView.isHidden = false
                }
            }
            else if (selectedPlayerList.count >= 7) && (totalBowlerArray.count == 0) && (totalWicketKeeperArray.count == 0){
                if (details.playerRole != PlayerRole.WicketKeeper.rawValue) && (details.playerRole != PlayerRole.Bowler.rawValue){
                    cell.grayOutView.isHidden = false
                }
            }
            else if (selectedPlayerList.count >= 7) && (totalBowlerArray.count == 0) && (totalAllRounderArray.count == 0){
                if (details.playerRole != PlayerRole.AllRounder.rawValue) && (details.playerRole != PlayerRole.Bowler.rawValue){
                    cell.grayOutView.isHidden = false
                }
            }
            else if (selectedPlayerList.count >= 8) && ((11 -  selectedPlayerList.count) < (3 - totalBatsmenArray.count)){
                if details.playerRole != PlayerRole.Batsman.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count > 3){
                cell.grayOutView.isHidden = false
            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count > 3){
                cell.grayOutView.isHidden = false
            }
            else if (selectedPlayerList.count == 10) && (totalBatsmenArray.count < 3){
                if details.playerRole != PlayerRole.Batsman.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (selectedPlayerList.count == 10) && (totalBowlerArray.count < 3){
                if details.playerRole != PlayerRole.Bowler.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (selectedPlayerList.count == 10) && (totalAllRounderArray.count < 1){
                if details.playerRole != PlayerRole.AllRounder.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (selectedPlayerList.count == 10) && (totalWicketKeeperArray.count < 1){
                if details.playerRole != PlayerRole.WicketKeeper.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalWicketKeeperArray.count > 1){
                cell.grayOutView.isHidden = false
            }
            else if (totalBowlerArray.count == 6) && (totalWicketKeeperArray.count > 1){
                cell.grayOutView.isHidden = false
            }
            else if (totalBatsmenArray.count == 6) && (totalAllRounderArray.count > 1){
                cell.grayOutView.isHidden = false
            }
            else if (totalBowlerArray.count == 6) && (totalAllRounderArray.count > 1){
                cell.grayOutView.isHidden = false
            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count == 3) && (totalAllRounderArray.count == 0) && (totalAllRounderArray.count == 0){
                
                if (details.playerRole != PlayerRole.WicketKeeper.rawValue) && (details.playerRole != PlayerRole.AllRounder.rawValue) {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count == 3) && (totalAllRounderArray.count == 0) && (totalAllRounderArray.count == 0){
                    
                if (details.playerRole != PlayerRole.WicketKeeper.rawValue) && (details.playerRole != PlayerRole.AllRounder.rawValue) {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count >= 3) && (totalAllRounderArray.count >= 1){
                
                if details.playerRole != PlayerRole.WicketKeeper.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (totalBatsmenArray.count == 6) && (totalBowlerArray.count >= 3) && (totalWicketKeeperArray.count >= 1){
                
                if details.playerRole != PlayerRole.AllRounder.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count >= 3) && (totalAllRounderArray.count >= 1){
                
                if details.playerRole != PlayerRole.WicketKeeper.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if (totalBowlerArray.count == 6) && (totalBatsmenArray.count >= 3) && (totalWicketKeeperArray.count >= 1){
                
                if details.playerRole != PlayerRole.AllRounder.rawValue {
                    cell.grayOutView.isHidden = false
                }
            }
            else if details.playerRole == PlayerRole.WicketKeeper.rawValue {
                if totalWicketKeeperArray.count >= 4{
                    cell.grayOutView.isHidden = false
                }
            }else if details.playerRole == PlayerRole.Batsman.rawValue {
                if totalBatsmenArray.count >= 6{
                    cell.grayOutView.isHidden = false
                }
            }
            else if details.playerRole == PlayerRole.AllRounder.rawValue {
                if totalAllRounderArray.count >= 4{
                    cell.grayOutView.isHidden = false
                }
            }
            else if details.playerRole == PlayerRole.Bowler.rawValue {
                if totalBowlerArray.count >= 6{
                    cell.grayOutView.isHidden = false

                }
            }
        }
    }
    
}
