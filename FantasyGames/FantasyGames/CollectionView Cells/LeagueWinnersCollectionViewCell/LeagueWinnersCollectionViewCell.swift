//
//  LeagueWinnersCollectionViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 06/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class LeagueWinnersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRankUpTo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(details: WinnerRanksDetails) {
        
    }

}
