//
//  PlayerPointDetailsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class PlayerPointDetailsViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var totalPlayerArray = Array<PlayerDetails>()
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        containerView.topViewCorners()

        collectionView.register(UINib(nibName: "PlayerDetailsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlayerDetailsCollectionViewCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
                    
            let indexPath = IndexPath(item: self.selectedIndex, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
    }
//    override func viewDidAppear(_ animated: Bool) {
//        let indexPath = IndexPath(item: selectedIndex, section: 0)
//        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
//    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension PlayerPointDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalPlayerArray.count
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: containerView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerDetailsCollectionViewCell", for: indexPath) as! PlayerDetailsCollectionViewCell
        let details = totalPlayerArray[indexPath.row]
        cell.configData(details: details)
        return cell
    }
    
}
