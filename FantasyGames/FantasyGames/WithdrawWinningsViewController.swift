//
//  WithdrawWinningsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 12/03/20.
//  Copyright © 2020 App Creatives Technologies. All rights reserved.
//

import UIKit

protocol WithdrawWinningsViewControllerDelegates {
    func updateWithdrawRequestStatus()
}
class WithdrawWinningsViewController: UIViewController {

    @IBOutlet weak var lblWalletAmount: UILabel!
    @IBOutlet weak var enterAmountView: UIView!
    @IBOutlet weak var txtFieldAmount: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var withdrawButton: UIButton!
    var delegate: WithdrawWinningsViewControllerDelegates?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func withdrawButtonTapped(_ sender: Any) {
        let enteredAmount = Int(txtFieldAmount.text ?? "0") ?? 0
        if enteredAmount < 100 {
            AppHelpers.showCustomErrorAlertView(message: "You can not withdraw less than ₹100")
            return
        }
        else if enteredAmount > 10000 {
            AppHelpers.showCustomErrorAlertView(message: "You can not withdraw more than ₹10000")
            return
        }
        
        callUpdateUserProfileAPI()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func callUpdateUserProfileAPI()  {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelpers.sharedInstance.displayLoader()
        
        let params = ["amount": txtFieldAmount.text!,
                      "type": "1",
                      ] as [String : Any]
        
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kAddWithdrawRequestURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? ""
                
                if statusCode == "200"{
                    let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action -> Void in
                        weakSelf!.navigationController?.popViewController(animated: true)
                    }))
                    weakSelf?.present(alert, animated: true, completion: nil)
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
    
}
