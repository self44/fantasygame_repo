//
//  ReferAndEarnViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 01/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class ReferAndEarnViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
