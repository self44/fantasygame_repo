//
//  PaymentMethodsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 09/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PaymentMethodsViewController: UIViewController  {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblAddedAmount: UILabel!

    lazy var selectedAmount = 0
    var promoCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        containerView.topViewCorners()
        tblView.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreTableViewCell")
        lblAddedAmount.text = "₹\(selectedAmount)"
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func getPaytmChecksum() {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
    
        AppHelpers.sharedInstance.displayLoader()

        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kAddCashURL, andParameters: ["amount": String(selectedAmount), "promo": promoCode], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                
                if status == "200"{
                    if result!["data"].dictionary != nil {
                        weakSelf?.sharePayTmPaymentDetails(response: result!["data"])
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
    
    func sharePayTmPaymentDetails(response: JSON) {

        let merchantConfig = PGMerchantConfiguration.default();
        var odrDict = Parameters()
        odrDict["CALLBACK_URL"] = response["CALLBACK_URL"].string ?? ""
        odrDict["CHANNEL_ID"] = response["CHANNEL_ID"].string ?? ""
        odrDict["CHECKSUMHASH"] = response["CHECKSUMHASH"].string ?? ""
        odrDict["CUST_ID"] = response["CUST_ID"].string ?? ""
        odrDict["INDUSTRY_TYPE_ID"] = response["INDUSTRY_TYPE_ID"].string ?? ""
        odrDict["MID"] = response["MID"].string ?? ""
        odrDict["ORDER_ID"] = response["ORDER_ID"].string ?? ""
        odrDict["TXN_AMOUNT"] = response["TXN_AMOUNT"].string ?? ""
        odrDict["WEBSITE"] = response["WEBSITE"].string ?? ""
        
        let order: PGOrder = PGOrder(params: odrDict)
        let transactionController = PGTransactionViewController.init(transactionFor: order)
        transactionController?.serverType = eServerTypeStaging
        transactionController?.merchant = merchantConfig
        transactionController?.delegate = self
        self.navigationController?.pushViewController(transactionController!, animated: true)
    }
    
    
}

extension PaymentMethodsViewController: UITableViewDelegate, UITableViewDataSource{
    
   //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as? MoreTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "MoreTableViewCell") as? MoreTableViewCell
        }
        cell!.rightArrow.isHidden = false
        if indexPath.row == 0 {
            cell!.lblTitle.text = "Paytm"
        }
        else{
            cell!.rightArrow.isHidden = true
            cell!.lblTitle.text = "Other options are coming soon..."
        }
        cell?.selectionStyle = .none
        return cell!
    }
     
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedAmount == 0 {
            AppHelpers.showCustomErrorAlertView(message: "Please select the amount greater than zero")
            return;
        }
        if indexPath.row == 0 {
            getPaytmChecksum()
        }
    }
}


extension PaymentMethodsViewController: PGTransactionDelegate{
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        print(responseString)
        AppHelpers.showCustomErrorAlertView(message: responseString)
        navigationController?.popViewController(animated: true)
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        navigationController?.popViewController(animated: true)
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        AppHelpers.showCustomErrorAlertView(message: error.localizedDescription)
        navigationController?.popViewController(animated: true)

    }
    
    func didFinishCASTransaction(controller: PGTransactionViewController, response: [NSObject : AnyObject]) {
        AppHelpers.showCustomErrorAlertView(message: "error.localizedDescription")
        navigationController?.popViewController(animated: true)

    }
    
}
