//
//  WebServiceHandler.swift
//  Posh_IT
//
//  Created by Vikash Rajput on 3/8/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let osVersion = UIDevice.current.systemVersion
let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

class WebServiceHandler: NSObject {
    
    class func performGETRequest(withURL urlString: String, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        
        let apiToken = UserDetails.sharedInstance.accessToken

        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "accessToken": apiToken,
            "platform": "iOS",
            "osVersion": osVersion,
            "versionCode": appVersion,
            "versionName": buildVersion,
        ]
        
        Alamofire.request(urlString, method : .get, parameters : nil, encoding : JSONEncoding.default , headers : headers).responseData { dataResponse in
            
            let statusCode = dataResponse.response?.statusCode
            
            if statusCode == 200{
                let responseJSON = JSON(dataResponse.data!)
                print(responseJSON)
                if responseJSON.dictionary != nil{

                    let statusCodeOnResponse = responseJSON.dictionary!["code"]?.stringValue
                    
                    if let unserMaintenance = responseJSON.dictionary!["under_maintenance"]?.stringValue {
                        if unserMaintenance == "yes"{
                            if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                if navVC.visibleViewController is AppMaintenanceViewController{
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let underMaintenanceVC = storyboard.instantiateViewController(withIdentifier: "AppMaintenanceViewController")
                                    navVC.pushViewController(underMaintenanceVC, animated: true)
                                }
                            }
                        }
                    }
                    
                    if let maxTeamLimit = responseJSON.dictionary!["max_teams"]?.stringValue {
                        UserDetails.sharedInstance.maxTeamLimitForCricket = Int(maxTeamLimit) ?? 4;
                    }
                    
                    if let iOSMinVersion = responseJSON.dictionary!["ios_version_min"]?.stringValue {
                        UserDetails.sharedInstance.iOSMaxVersion = iOSMinVersion
                        
                        if let iOSMaxVersion = responseJSON.dictionary!["ios_version_max"]?.stringValue {
                            UserDetails.sharedInstance.iOSMaxVersion = iOSMaxVersion
                            if UserDetails.sharedInstance.isLogin{
                                
                                if !UserDetails.sharedInstance.isForceUpdateViewOpen{
                                    
                                    if appVersion < iOSMinVersion{
                                        let appUpdateView = UpdateAppView(frame: APPDELEGATE.window!.frame)
                                        appUpdateView.isForceUpdateEnable = true
                                        appUpdateView.configData()
                                        UserDetails.sharedInstance.isForceUpdateViewOpen = true
                                        APPDELEGATE.window?.addSubview(appUpdateView)
                                    }
                                    else if appVersion < iOSMaxVersion{
                                        let serverTime: String = (UserDefaults.standard.value(forKey: "lastForceUpdateTime") ?? "0") as! String
                                        
                                        if UserDetails.sharedInstance.serverTimeStemp.count == 0{
                                            UserDetails.sharedInstance.serverTimeStemp = "0"
                                        }
                                            
                                        let updateDuration = responseJSON.dictionary!["ios_update_duration"]?.stringValue ?? "0"
                                        if (Double(UserDetails.sharedInstance.serverTimeStemp)! - Double(serverTime)!) > (Double(updateDuration) ?? 0){
                                            
                                            let appUpdateView = UpdateAppView(frame: APPDELEGATE.window!.frame)
                                            appUpdateView.isForceUpdateEnable = false
                                            appUpdateView.configData()
                                            APPDELEGATE.window?.addSubview(appUpdateView)
                                            UserDetails.sharedInstance.isForceUpdateViewOpen = true
                                            UserDefaults.standard.set(UserDetails.sharedInstance.serverTimeStemp, forKey: "lastForceUpdateTime")
                                            UserDefaults.standard.synchronize()
                                    }
                                    }
                                }
                                
                                if let iOSUpdateUrl = responseJSON.dictionary!["ios_update_url"]?.stringValue {
                                    UserDetails.sharedInstance.updateAppUrl = iOSUpdateUrl
                                }
                            }
                        }
                    }
                    
                    let serverTimestemp = responseJSON.dictionary!["serverTimestamp"]?.stringValue ?? "0"
                    if (serverTimestemp.count != 0) && (serverTimestemp != "0") {
                        UserDetails.sharedInstance.serverTimeStemp = serverTimestemp
                    }
                    
                    if let notificationsCount = responseJSON.dictionary!["new_notifications"]?.stringValue{
                        UserDetails.sharedInstance.notificationCount = notificationsCount;
                    }
                    
                    if let referralAmount = responseJSON.dictionary!["referral_amount"]?.stringValue {
                        UserDetails.sharedInstance.referralAmount = referralAmount
                        UserDefaults.standard.set(referralAmount, forKey: "referral_amount")
                        UserDefaults.standard.synchronize()
                    }

                    if statusCodeOnResponse == "200" {
                        if (urlString == kMatchURL) || (urlString.contains(kCricketLeagueURL)){
                            CoreDataFileHandler.updateValueInCoreData(urlString: urlString, response: dataResponse.data!)
                        }
                        
                        completionBlock(responseJSON, dataResponse.error)
                    }
                    else{
                        completionBlock(responseJSON, dataResponse.error)
                    }
                }
                else{
                    print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                    completionBlock(nil, dataResponse.error)
                }
            }
            else{
                print(dataResponse.error?.localizedDescription ?? "HTTP Error")
                completionBlock(nil, dataResponse.error)
            }
        }
    }
    
    class func performPOSTRequest(urlString: String, andParameters params: Parameters?, andAcessToken accessToken: String?, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        
        var apiToken = accessToken
        if apiToken == nil {
            apiToken = ""
        }
        
        let osVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "accessToken": apiToken!,
            "platform": "iOS",
            "osVersion": osVersion,
            "versionCode": appVersion,
            "versionName": buildVersion
        ]
        print(params)
        Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in

                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    let responseJSON = JSON(response.data!)
                    print(responseJSON)
                    if responseJSON.dictionary != nil{
                        let statusCodeOnResponse = responseJSON.dictionary!["code"]?.stringValue
                        
                        if let unserMaintenance = responseJSON.dictionary!["under_maintenance"]?.stringValue {
                            if unserMaintenance == "yes"{
                                if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                    if navVC.visibleViewController is AppMaintenanceViewController{
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let underMaintenanceVC = storyboard.instantiateViewController(withIdentifier: "AppMaintenanceViewController")
                                        navVC.pushViewController(underMaintenanceVC, animated: true)
                                    }
                                }
                            }
                        }
                        
                        if let maxTeamLimit = responseJSON.dictionary!["max_teams"]?.stringValue {
                            UserDetails.sharedInstance.maxTeamLimitForCricket = Int(maxTeamLimit) ?? 4;
                        }
                                                
                        if let iOSMinVersion = responseJSON.dictionary!["ios_version_min"]?.stringValue {
                            UserDetails.sharedInstance.iOSMaxVersion = iOSMinVersion
                            
                            if let iOSMaxVersion = responseJSON.dictionary!["ios_version_max"]?.stringValue {
                                UserDetails.sharedInstance.iOSMaxVersion = iOSMaxVersion
                                if UserDetails.sharedInstance.isLogin{
                                    if !UserDetails.sharedInstance.isForceUpdateViewOpen{
                                        
                                        if appVersion < iOSMinVersion{
                                            
                                            let appUpdateView = UpdateAppView(frame: APPDELEGATE.window!.frame)
                                            appUpdateView.isForceUpdateEnable = true
                                            appUpdateView.configData()
                                            UserDetails.sharedInstance.isForceUpdateViewOpen = true
                                            APPDELEGATE.window?.addSubview(appUpdateView)
                                        }
                                        else if appVersion < iOSMaxVersion{
                                            let serverTime: String = (UserDefaults.standard.value(forKey: "lastForceUpdateTime") ?? "0") as! String
                                            
                                            if UserDetails.sharedInstance.serverTimeStemp.count == 0{
                                                UserDetails.sharedInstance.serverTimeStemp = "0"
                                            }
                                            let updateDuration = responseJSON.dictionary!["ios_update_duration"]?.stringValue ?? "0"
                                            if (Double(UserDetails.sharedInstance.serverTimeStemp)! - Double(serverTime)!) > (Double(updateDuration) ?? 0){
                                                let appUpdateView = UpdateAppView(frame: APPDELEGATE.window!.frame)
                                                appUpdateView.isForceUpdateEnable = false
                                                appUpdateView.configData()
                                                APPDELEGATE.window?.addSubview(appUpdateView)
                                                UserDetails.sharedInstance.isForceUpdateViewOpen = true
                                                UserDefaults.standard.set(UserDetails.sharedInstance.serverTimeStemp, forKey: "lastForceUpdateTime")
                                                UserDefaults.standard.synchronize()
                                            }
                                        }
                                    }                                    
                                }
                            }
                        }
                        
                        let serverTimestemp = responseJSON.dictionary!["serverTimestamp"]?.stringValue ?? "0"
                        if (serverTimestemp.count != 0) && (serverTimestemp != "0") {
                            UserDetails.sharedInstance.serverTimeStemp = serverTimestemp
                        }

                        if let notificationsCount = responseJSON.dictionary!["new_notifications"]?.stringValue{
                            UserDetails.sharedInstance.notificationCount = notificationsCount;
                        }
                        
                        if let referralAmount = responseJSON.dictionary!["referral_amount"]?.stringValue {
                            UserDetails.sharedInstance.referralAmount = referralAmount
                            UserDefaults.standard.set(referralAmount, forKey: "referral_amount")
                            UserDefaults.standard.synchronize()
                        }
       
                        if statusCodeOnResponse == "200" {
                            if (urlString == kMatchURL) || (urlString == kCricketLeagueURL){
                                CoreDataFileHandler.updateValueInCoreData(urlString: urlString, response: response.data!)
                            }
                            completionBlock(responseJSON, response.error)
                        }
                        else{
                            completionBlock(responseJSON, response.error)
                        }
                    }
                    else{
                        print(response.error?.localizedDescription ?? "HTTP Error")
                        completionBlock(nil, response.error)
                    }
                }
                else{
                    completionBlock(nil, response.error)
                }
        }
    }

    
    class func performMultipartRequest(urlString: String, fileName: String, params: Parameters?, imageData: Data, accessToken: String?, completion completionBlock: @escaping (_ result: JSON?, _ error: Error?) -> Void) {
        
        var apiToken = accessToken
        if apiToken == nil {
            apiToken = ""
        }
        
        let osVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
        
        let headers: HTTPHeaders = [
            "accessToken": apiToken!,
            "platform": "iOS",
            "osVersion": osVersion,
            "versionCode": appVersion,
            "versionName": buildVersion
        ]
        
        print(params!)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(imageData, withName: fileName, fileName: "imageName.png", mimeType: "image/jpeg")
            
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    if response.error != nil{
                        completionBlock(nil, response.error)
                        return
                    }

                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        let responseJSON = JSON(response.data!)
                        if responseJSON.dictionary != nil{
                            let statusCodeOnResponse = responseJSON.dictionary!["status"]?.string
                            
                            if let unserMaintenance = responseJSON.dictionary!["under_maintenance"]?.stringValue {
                                if unserMaintenance == "yes"{
                                    if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
                                        if navVC.visibleViewController is AppMaintenanceViewController{
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            let underMaintenanceVC = storyboard.instantiateViewController(withIdentifier: "AppMaintenanceViewController")
                                            navVC.pushViewController(underMaintenanceVC, animated: true)
                                        }
                                    }
                                }
                            }
                            
                                                        
                            let serverTimestemp = responseJSON.dictionary!["serverTimestamp"]?.stringValue ?? "0"
                            if (serverTimestemp.count != 0) && (serverTimestemp != "0") {
                                UserDetails.sharedInstance.serverTimeStemp = serverTimestemp
                            }

                            if let notificationsCount = responseJSON.dictionary!["new_notifications"]?.stringValue{
                                UserDetails.sharedInstance.notificationCount = notificationsCount;
                            }
                            
                            if let referralAmount = responseJSON.dictionary!["referral_amount"]?.stringValue {
                                UserDetails.sharedInstance.referralAmount = referralAmount
                                UserDefaults.standard.set(referralAmount, forKey: "referral_amount")
                                UserDefaults.standard.synchronize()
                            }
                            
                            if statusCodeOnResponse == "200" {
                                CoreDataFileHandler.updateValueInCoreData(urlString: urlString, response: response.data!)
                                let savedJson = CoreDataFileHandler.getDetailsFromCoreData(urlString: urlString)
                                completionBlock(savedJson, response.error)
                            }
                            else{
                                completionBlock(responseJSON, response.error)
                            }
                        }
                        else{
                            print(response.error?.localizedDescription ?? "HTTP Error")
                            completionBlock(nil, response.error)
                        }
                    }
                    else{
                        print(response.error?.localizedDescription ?? "HTTP Error")
                        completionBlock(nil, response.error)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionBlock(nil, error)
            }
        }
    }
}



extension String {

    static func <(lhs: String, rhs: String) -> Bool {
        return lhs.compare(rhs, options: .numeric) == .orderedAscending
    }
}
