//
//  CoreDataFileHandler.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class CoreDataFileHandler: NSObject {

    class func saveDetailsInCoreData(urlString: String, responseData: Data){
        
        let managedContext = APPDELEGATE.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Response", in: managedContext)!
        let response = NSManagedObject(entity: entity, insertInto: managedContext)
        response.setValue(urlString, forKeyPath: "urlString")
        response.setValue(responseData, forKeyPath: "responseJson")
        response.setValue(Date(), forKeyPath: "updateTime")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    class func getDetailsFromCoreData(urlString: String) -> JSON?{
        
        let managedContext = APPDELEGATE.persistentContainer.viewContext

        let entity = NSEntityDescription.entity(forEntityName: "Response", in: managedContext)!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "urlString == %@", urlString)
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            if result.count > 0 {
                if let responseDetails = result[0] as? Response{
                    let createTime = responseDetails.updateTime
                    
                    let createTimestemp = createTime?.timeIntervalSince1970
                    let currentTimestemp = Date().timeIntervalSince1970
                    let totalTimeDifference = currentTimestemp - createTimestemp!
                    if totalTimeDifference > 7200{
                        return nil
                    }
                    
                    let resultData = responseDetails.responseJson
                    let responseJSON = JSON(resultData as AnyObject);
                    return responseJSON
                }
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return nil
    }
    
    
    class func updateValueInCoreData(urlString: String, response: Data){
        
        let managedContext = APPDELEGATE.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Response", in: managedContext)!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "urlString == %@", urlString)
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            if result.count > 0 {
                
                if let responseDetails = result[0] as? Response{
                    
                    responseDetails.setValue(urlString, forKeyPath: "urlString")
                    responseDetails.setValue(response, forKeyPath: "responseJson")
                    responseDetails.setValue(Date(), forKeyPath: "updateTime")
                }
                try managedContext.save()
            }
            else{
                CoreDataFileHandler.saveDetailsInCoreData(urlString: urlString, responseData: response)
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
}
