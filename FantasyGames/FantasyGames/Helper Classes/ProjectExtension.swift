//
//  ProjectExtension.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 04/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class ProjectExtension: NSObject {

    
}

extension UIView{
    
    func bottomTabShadow()  {
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.35
        self.layer.shadowRadius = 6.0
        self.layer.cornerRadius = 12.0
        self.layer.masksToBounds = false
        
    }
    
    func topViewCorners()  {
        self.layer.cornerRadius = 12.0
        self.layer.masksToBounds = true
    }
    
    func headerGradient() {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(red: 30.0/255, green: 60.0/255, blue: 175.0/255, alpha: 1).cgColor, UIColor(red: 100.0/255, green: 60.0/255, blue: 215.0/255, alpha: 1).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func progressGradient() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.cornerRadius = 4.0
//        gradient.colors = [UIColor(red: 150.0/255, green: 150.0/255, blue: 200.0/255, alpha: 1).cgColor, UIColor(red: 30.0/255, green: 60.0/255, blue: 175.0/255, alpha: 1).cgColor]
        
        gradient.colors = [UIColor(red: 97.0/255, green: 162.0/255, blue: 50.0/255, alpha: 1).cgColor, UIColor(red: 30.0/255, green: 60.0/255, blue: 175.0/255, alpha: 1).cgColor]

        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        self.layer.insertSublayer(gradient, at: 0)
    }

    
    func removeProgressGradient() {
        self.layer.sublayers = self.layer.sublayers?.filter { theLayer in
              !theLayer.isKind(of: CAGradientLayer.classForCoder())
        }
    }

    func appbackgroudColor() {
        self.backgroundColor = UIColor(red: 236.0/255, green: 236.0/255, blue: 236.0/255, alpha: 1)
    }
    
    func notificationViewBackgroundColor() {
        self.backgroundColor = UIColor(red: 92.0/255, green: 154.0/255, blue: 0.0/255, alpha: 1)
    }
    
    func cellShadow()  {
        self.layer.cornerRadius = 5.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 2.5
        self.layer.masksToBounds = false
    }
    
    func roundReactShadow()  {
        self.layer.cornerRadius = 10.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 2.5
        self.layer.masksToBounds = false
    }

    
    func roundViewCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }

    func customTabBottomShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 0.9
        self.layer.shadowOpacity = 0.6
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:2)
    }
}



extension UITextField{
    func isEmpty() -> Bool{
        if self.text?.count == 0 {
            return true
        }
        else{
            return false
        }
    }
    
    func textCount() -> Int {
        return self.text?.count ?? 0
    }
}


extension UIColor{
    
    class func selectedTabColor() -> UIColor {
        return UIColor.darkGray
    }

    func selectedTabColor() {
        
    }
    
    func unselectedTabColor() {
        
    }
    
    class func appBlackColor() -> UIColor {
        return UIColor(red: 40.0/255, green: 40.0/255, blue: 40.0/255, alpha: 1)
    }
    
    class func appThemeColorColor() -> UIColor {
        return UIColor(red: 30.0/255, green: 60.0/255, blue: 175.0/255, alpha: 1)
    }
    
    
    class func appbackgroudColor() -> UIColor {
        return UIColor(red: 240.0/255, green: 240.0/255, blue: 240.0/255, alpha: 1)
    }

    class func appGrayColor() -> UIColor {
        return UIColor(red: 122.0/255, green: 122.0/255, blue: 122.0/255, alpha: 1)
    }
    
}

extension UIButton{
    func gradient() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor(red: 30.0/255, green: 60.0/255, blue: 175.0/255, alpha: 1).cgColor, UIColor(red: 100.0/255, green: 60.0/255, blue: 215.0/255, alpha: 1).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.cornerRadius = 6
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func tabGradient() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor(red: 30.0/255, green: 60.0/255, blue: 175.0/255, alpha: 1).cgColor, UIColor(red: 100.0/255, green: 60.0/255, blue: 215.0/255, alpha: 1).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.cornerRadius = self.frame.height/2
        self.layer.insertSublayer(gradient, at: 0)
    }

    func removeTabGradient() {        
        self.layer.sublayers = self.layer.sublayers?.filter { theLayer in
              !theLayer.isKind(of: CAGradientLayer.classForCoder())
        }
    }
    
    func showShadowOnCirleButton()  {
        self.layer.cornerRadius = self.frame.width/2
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.35
        self.layer.shadowRadius = 3
        self.layer.masksToBounds = false
    }
}



extension Notification.Name {

    static let upcompingTabTappedNotification = Notification.Name("upcompingTabTappedNotification")
    static let myContestTabTappedNotification = Notification.Name("myContestTabTappedNotification")
    static let myAccountTabTappedNotification = Notification.Name("myAccountTabTappedNotification")
    static let moreTabTappedNotification = Notification.Name("moreTabTappedNotification")
    
    static let upcomingDataRefreshNotification = Notification.Name("upcomingDataRefreshNotification")
    static let removeClosedMatch = Notification.Name("removeClosedMatch")

    static let myContestDataRefreshNotification = Notification.Name("myContestDataRefreshNotification")

    static let playerSelectionUpdateNotification = Notification.Name("PlayerSelectionUpdateNotification")

}


extension String{

    func getShortName() -> String {
                    
        let playerNameArray = self.components(separatedBy: " ")
        var nameplayerNameArray = ""
         
        if playerNameArray.count > 2 {
            let firstIndexName = playerNameArray[0]
            let secondIndexName = playerNameArray[1]

            if firstIndexName.count > 2{
                nameplayerNameArray = String(firstIndexName.prefix(1))
                nameplayerNameArray = nameplayerNameArray + "."
            }
            
            if secondIndexName.count > 2{
                nameplayerNameArray = nameplayerNameArray + String(secondIndexName.prefix(1))
                nameplayerNameArray = nameplayerNameArray + "."
                for i in 2 ..< playerNameArray.count{
                    nameplayerNameArray = nameplayerNameArray + " " +  playerNameArray[i]
                }
            }
            else{
                for i in 1 ..< playerNameArray.count{
                    nameplayerNameArray = nameplayerNameArray + " " +  playerNameArray[i]
                }
            }
        }
        else if playerNameArray.count > 1 {
            let name = playerNameArray[0]
            if name.count > 2{
                nameplayerNameArray = String(name.prefix(1))
                nameplayerNameArray = nameplayerNameArray + "."
                for i in 1 ..< playerNameArray.count{
                    nameplayerNameArray = nameplayerNameArray + " " +  playerNameArray[i]
                }
            }
        }
        else{
            return self
        }
        if nameplayerNameArray == "" {
            return self
        }
        return nameplayerNameArray
    }
}
