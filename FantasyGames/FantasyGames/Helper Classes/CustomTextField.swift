//
//  CustomTextField.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 13/12/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

protocol CustomTextFieldDelegates: UITextFieldDelegate {
    func textField(_ textField: UITextField, didDeleteBackwardAnd wasEmpty: Bool)
}


class CustomTextField: UITextField {

    override func deleteBackward() {
           // see if text was empty
           let wasEmpty = text == nil || text! == ""

           // then perform normal behavior
           super.deleteBackward()

           // now, notify delegate (if existent)
           (delegate as? CustomTextFieldDelegates)?.textField(self, didDeleteBackwardAnd: wasEmpty)
       }
       

       override func draw(_ rect: CGRect) {
           let placeholderText = placeholder
           if (placeholderText != nil) {
            attributedPlaceholder = NSAttributedString(string: placeholderText!, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
           }

       }
    

}
