//
//  AppHelpers.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import Reachability
import SwiftyJSON

class AppHelpers: NSObject {

    static let sharedInstance = AppHelpers()
    var appLoaderView: AppLoaderView?
    var alertView: CustomAlertView?


    class func resetDefaults() {
        
        UserDetails.sharedInstance.accessToken = ""
        AppHelpers.getGeustToken()

        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if (key != "lastForceUpdateTime") && !key.contains("WizRocket"){
                defaults.removeObject(forKey: key)
            }
        }

        UserDetails.sharedInstance.firstName = ""
        UserDetails.sharedInstance.lastName = ""
        UserDetails.sharedInstance.userName = ""
        UserDetails.sharedInstance.phoneNumber = ""
        UserDetails.sharedInstance.dateOfBirth = ""
        UserDetails.sharedInstance.emailAdress = ""
    }

    
    class func makeTabBarAsRootViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController")
        let navVC = UINavigationController(rootViewController: tabBarVC)
        navVC.isNavigationBarHidden = true
        APPDELEGATE.window?.rootViewController = navVC
    }
    
    class func makePhoneAsRootViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarVC = storyboard.instantiateViewController(withIdentifier: "PhoneNumberViewController")
        let navVC = UINavigationController(rootViewController: tabBarVC)
        navVC.isNavigationBarHidden = true
        APPDELEGATE.window?.rootViewController = navVC
        
        
    }

    
    class func saveUserDetails() {
        UserDefaults.standard.set(UserDetails.sharedInstance.userName, forKey: kUserName)
        UserDefaults.standard.set(UserDetails.sharedInstance.firstName, forKey: kFirstName)
        UserDefaults.standard.set(UserDetails.sharedInstance.lastName, forKey: kLastName)
        UserDefaults.standard.set(UserDetails.sharedInstance.emailAdress, forKey: kEmailAdress)
        UserDefaults.standard.set(UserDetails.sharedInstance.gender, forKey: kGender)
        UserDefaults.standard.set(UserDetails.sharedInstance.dateOfBirth, forKey: kDOB)
        UserDefaults.standard.set(UserDetails.sharedInstance.phoneNumber, forKey: kPhoneNumber)
        UserDefaults.standard.set(UserDetails.sharedInstance.imageURL, forKey: kImageURL)
        UserDefaults.standard.set(UserDetails.sharedInstance.accessToken, forKey: kAccessToken)
        UserDefaults.standard.set(UserDetails.sharedInstance.panVerified, forKey: "panVerified")
        UserDefaults.standard.set(UserDetails.sharedInstance.emailVerified, forKey: "emailVerified")
        UserDefaults.standard.set(UserDetails.sharedInstance.imageURL, forKey: "imagePath")

        UserDefaults.standard.set(UserDetails.sharedInstance.phoneNumber, forKey: "phoneNumber")
        UserDefaults.standard.set(UserDetails.sharedInstance.bankVerified, forKey: "bankVerified")
        UserDefaults.standard.set(UserDetails.sharedInstance.bonusCash, forKey: "bonusCash")
        UserDefaults.standard.set(UserDetails.sharedInstance.unusedAmount, forKey: "unusedAmount")
        UserDefaults.standard.set(UserDetails.sharedInstance.totalWinningCash, forKey: "totalWinningCash")
        UserDefaults.standard.set(UserDetails.sharedInstance.totalContestJoined, forKey: "totalContestJoined")
        UserDefaults.standard.set(UserDetails.sharedInstance.totalContestWon, forKey: "totalContestWon")
    }
    
    class func getUserDetails() {

        if let userName = UserDefaults.standard.value(forKey: kUserName) as? String{
            UserDetails.sharedInstance.userName = userName
        }

        if let firstName = UserDefaults.standard.value(forKey: kFirstName) as? String{
            UserDetails.sharedInstance.firstName = firstName
        }
        
        if let lastName = UserDefaults.standard.value(forKey: kLastName) as? String{
            UserDetails.sharedInstance.lastName = lastName
        }

        if let emailAdress = UserDefaults.standard.value(forKey: kEmailAdress) as? String{
            UserDetails.sharedInstance.emailAdress = emailAdress
        }

        if let phoneNumber = UserDefaults.standard.value(forKey: kPhoneNumber) as? String{
            UserDetails.sharedInstance.phoneNumber = phoneNumber
        }

        if let imageURL = UserDefaults.standard.value(forKey: kImageURL) as? String{
            UserDetails.sharedInstance.imageURL = imageURL
        }
        
        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String{
            UserDetails.sharedInstance.accessToken = accessToken
        }
        
        if let gender = UserDefaults.standard.value(forKey: kGender) as? String{
            UserDetails.sharedInstance.gender = gender
        }
        
        if let panVerified = UserDefaults.standard.value(forKey: "panVerified") as? String{
            UserDetails.sharedInstance.panVerified = panVerified
        }
        
        if let emailVerified = UserDefaults.standard.value(forKey: "emailVerified") as? String{
            UserDetails.sharedInstance.emailVerified = emailVerified
        }

        if let phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as? String{
            UserDetails.sharedInstance.phoneNumber = phoneNumber
        }

        if let bankVerified = UserDefaults.standard.value(forKey: "bankVerified") as? String{
            UserDetails.sharedInstance.bankVerified = bankVerified
        }

        if let bonusCash = UserDefaults.standard.value(forKey: "bonusCash") as? String{
            UserDetails.sharedInstance.bonusCash = bonusCash
        }

        if let unusedAmount = UserDefaults.standard.value(forKey: "unusedAmount") as? String{
            UserDetails.sharedInstance.unusedAmount = unusedAmount
        }
        
        if let totalWinningCash = UserDefaults.standard.value(forKey: "totalWinningCash") as? String{
            UserDetails.sharedInstance.totalWinningCash = totalWinningCash
        }
        
        if let totalContestJoined = UserDefaults.standard.value(forKey: "totalContestJoined") as? String{
            UserDetails.sharedInstance.totalContestJoined = totalContestJoined
        }
        
        if let totalContestWon = UserDefaults.standard.value(forKey: "totalWinningCash") as? String{
            UserDetails.sharedInstance.totalContestWon = totalContestWon
        }
    }
    
    class func showCustomAlertView(message: String) {
        AppHelpers.sharedInstance.alertView?.removeFromSuperview()
        
        AppHelpers.sharedInstance.alertView = CustomAlertView(frame: CGRect(x: 0, y: -80, width: UIScreen.main.bounds.width, height: 60))
        AppHelpers.sharedInstance.alertView?.messageDisplay(message: message, color: UIColor(red: 230.0/255, green: 190.0/255, blue: 70.0/255, alpha: 1))
        APPDELEGATE.window?.addSubview(AppHelpers.sharedInstance.alertView!)
        AppHelpers.sharedInstance.alertView?.showAnimation()
    }
    
    class func showCustomErrorAlertView(message: String) {

        AppHelpers.sharedInstance.alertView?.removeFromSuperview()
        AppHelpers.sharedInstance.alertView = CustomAlertView(frame: CGRect(x: 0, y: -80, width: UIScreen.main.bounds.width, height: 60))
        AppHelpers.sharedInstance.alertView?.messageDisplay(message: message, color: UIColor(red: 230.0/255, green: 190.0/255, blue: 70.0/255, alpha: 1))
        APPDELEGATE.window?.addSubview(AppHelpers.sharedInstance.alertView!)
        AppHelpers.sharedInstance.alertView?.showAnimation()
    }
    
    
    class func isApplicationRunningOnIphoneX() -> Bool{
        
        if UIDevice().userInterfaceIdiom == .phone {
            
            if (UIScreen.main.bounds.height >= 812 ){
                return true
            }
        }

        return false
    }

    
    class func getAllState() ->Array<JSON>  {
        let url = Bundle.main.url(forResource: "IndianStates", withExtension: "plist")!
        let soundsData = try! Data(contentsOf: url)
        let myPlist = try! PropertyListSerialization.propertyList(from: soundsData, options: [], format: nil) as? Array<Dictionary<String, String>>
        var statsArray = Array<JSON>()
        
        for details in myPlist! {
            let json = JSON(details)
            statsArray.append(json)
        }
        return statsArray
    }

    class func getTransactionFormattedDate(dateString: String) -> String{
            
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: dateString){
            dateFormatter.dateFormat = "dd MMMM, yyyy"
            let dateStr = dateFormatter.string(from: date)
            return dateStr
        }
        return ""
    }
    
    
    class func sortDateArray(dataArray: [String]) -> [String] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MM, yyyy"
        var convertedArray: [Date] = []
        for dat in dataArray {
            let date = dateFormatter.date(from: dat)
            if let date = date {
                convertedArray.append(date)
            }
        }

        let dateArray = convertedArray.sorted(by: { $0.compare($1) == .orderedDescending })
        var convertedStringArray: [String] = []
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        for dat in dateArray {
            let date = dateFormatter.string(from: dat)
            convertedStringArray.append(date)
        }
        return convertedStringArray
    }
    
    class func getFormatedDateWithoutTime(dateString: String) -> String{
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: dateString){
            dateFormatter.dateFormat = "dd MMMM, yyyy"
            let dateStr = dateFormatter.string(from: date)
            return dateStr
        }
        return ""
    }
    
    class func isNotValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return !emailTest.evaluate(with: email)
    }

    
    class func getGeustToken() {
        if !AppHelpers.isInterNetConnectionAvailable() {
            return
        }
        let parms = ["type":"guest"]

        WebServiceHandler.performPOSTRequest(urlString: kAuthURL, andParameters: parms, andAcessToken: "") { (result, error) in
            if result != nil{
                if let accessToken = result!["accessTokoen"].string{
                    UserDetails.sharedInstance.accessToken = accessToken
                }
            }
        }
    }
    

    class func isInterNetConnectionAvailable() -> Bool {
        
        do{
            let reachability = try Reachability().connection
            
            if reachability == .cellular {
                print("Reachable via Mobile Data")
                return true
            }
            if reachability == .wifi {
                print("Reachable via WiFi")
                return true
            }else {
                print("Reachable via None")
                AppHelpers.showCustomErrorAlertView(message: kNoInternetConnection)
                return false
            }
        } catch{
            print("Failed to reachable")
        }
        
        return false
    }

    func displayLoader() {
        if (appLoaderView != nil) {
            removeLoader()
        }
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            guard let weakObj = weakSelf else { return }
            weakObj.appLoaderView = AppLoaderView(frame: UIScreen.main.bounds)
            APPDELEGATE.window?.addSubview(weakObj.appLoaderView!)
        }
    }
    
    func removeLoader() {
        
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.appLoaderView?.removeFromSuperview()
            weakSelf?.appLoaderView = nil;
        }
    }
    
    class func showMatchClosePopup() {
        
    }
   
    
    class func getMatchClosingTime(matchTimestamp: String) -> String {

        let serverUnixTime = Double(UserDetails.sharedInstance.serverTimeStemp) ?? 0
        let matchClosingUnixTime = Double(matchTimestamp) ?? 0
        var totalSeconds = 0.0
        
        if matchClosingUnixTime > serverUnixTime {
            totalSeconds = matchClosingUnixTime - serverUnixTime
        }
        else{
            return "00:00"
        }
        
        let hours = Int(totalSeconds/3600)
        let days = Int(hours/24)
        
        let remaingSecs = (totalSeconds.truncatingRemainder(dividingBy: 3600))
        let minutes = Int(remaingSecs / 60)
//        print("remaingSecs",remaingSecs)
        let secs = Int((remaingSecs.truncatingRemainder(dividingBy: 60)))
        
        var minStr = ""
        var secStr = ""
        
        if (minutes < 10) {
            minStr = "0" + String(minutes)
        }
        else{
            minStr = String(minutes)
        }
        
        if (secs < 10) {
            secStr = "0" + String(secs)
        }
        else{
            secStr = String(secs)
        }
        
        if days > 1{
//            return "\(hours)h \(minStr)m \(secStr)s left"

            return "\(days) days left"
        }
        else{
            if hours > 0{
//                return "\(hours) h \(minStr) m \(secStr) s left"
                return "\(hours)h \(minStr)m left"
            }
            else{
                return "\(minStr)m \(secStr)s left"
            }
        }
    }
    
    class func validateCricketLeague(selectedPlayerList: Array<PlayerDetails>, teamName: String, maxCredit: Float, isRecordRemove: Bool) -> String? {

        if isRecordRemove{
            return nil
        }
        else if selectedPlayerList.count > 11 {
            return max11Player
        }
        else{
            
            let lastPlayerDetails = selectedPlayerList.last
            let team1PlayesList = selectedPlayerList.filter { (playerDetails) -> Bool in
                playerDetails.teamName == teamName
            }

            if team1PlayesList.count > 7{
                return max7PlayerFromTeam
            }
            else if (selectedPlayerList.count - team1PlayesList.count)  > 7{
                return max7PlayerFromTeam
            }
            else{
                
                let totalBatsmanArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerRole == PlayerRole.Batsman.rawValue
                }
                
                let totalBowlerArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerRole == PlayerRole.Bowler.rawValue
                }
                
                let totalAllrounderArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerRole == PlayerRole.AllRounder.rawValue
                }
                
                let totalWicketKeeperArray = selectedPlayerList.filter { (playerDetails) -> Bool in
                    playerDetails.playerRole == PlayerRole.WicketKeeper.rawValue
                }
                
                let totalPoints = selectedPlayerList.reduce(0, { creditSum, nextPlayerDetails in
                    creditSum + Float(nextPlayerDetails.playerCredits)!
                })
                
                if (totalPoints > maxCredit) {
                    let remaingPoints = totalPoints - Float(lastPlayerDetails!.playerCredits)!
                    return "Sorry, you have only " + String(maxCredit - remaingPoints) + " credits left.";
                }
                else if totalBatsmanArray.count > 6{
                    return max6Batsman
                }
                else if totalBowlerArray.count > 6{
                    return max6Bowler
                }
                else if totalAllrounderArray.count > 4{
                    return max4Allrounder
                }
                else if totalWicketKeeperArray.count > 4{
                    return max4WicketKeeper
                }
                    
                let remainingWicketKeeper = 1 - totalWicketKeeperArray.count
                let remainingBatsmantKeeper = 3 - totalBatsmanArray.count
                let remainingAllRounderKeeper = 1 - totalAllrounderArray.count
                let remainingBowlerKeeper = 3 - totalBowlerArray.count

                var temp = 0
                
                if remainingWicketKeeper > 0 {
                    temp = temp + remainingWicketKeeper
                }
                
                if remainingBatsmantKeeper > 0 {
                    temp = temp + remainingBatsmantKeeper
                }

                if remainingAllRounderKeeper > 0 {
                    temp = temp + remainingAllRounderKeeper
                }

                if remainingBowlerKeeper > 0 {
                    temp = temp + remainingBowlerKeeper
                }
                
                if selectedPlayerList.count >= 6 {
                    if temp > 11 - selectedPlayerList.count{
                        if remainingAllRounderKeeper > 0 {
                            return pickOneAllRounder
                        }
                        else if remainingBowlerKeeper > 0 {
                            return min3Bowler
                        }
                        else if remainingWicketKeeper > 0 {
                            return pickOneKeeper
                        }
                        else if remainingBatsmantKeeper > 0 {
                            return min3Batsman
                        }
                    }
                }
            }
        }
        
        return nil
    }
    
    
    class func getPlayerRole(details: PlayerDetails) -> String {
        if details.playerRole == PlayerRole.WicketKeeper.rawValue{
            return "WK"
        }
        else if details.playerRole == PlayerRole.Batsman.rawValue{
            return "BAT"
        }
        else if details.playerRole == PlayerRole.AllRounder.rawValue{
            return "ALR"
        }
        else if details.playerRole == PlayerRole.Bowler.rawValue{
            return "BOW"
        }
    
        return ""
    }
    
    class func redirectToCreateCricketTeam(details: MatchDetails, leagueDetails: LeagueDetails?, isEditTeam: Bool, teamDetails: TeamDetails?) {

        let storybaord = UIStoryboard(name: "Main", bundle: nil)
        let selectPlayerVC = storybaord.instantiateViewController(withIdentifier: "SelectCricketPlayersViewController") as! SelectCricketPlayersViewController
        selectPlayerVC.matchDetails = details
        selectPlayerVC.leagueDetails = leagueDetails
        selectPlayerVC.teamDetails = teamDetails
        selectPlayerVC.isEditTeam = isEditTeam
        if teamDetails != nil {
            selectPlayerVC.selectedPlayerList = teamDetails!.playersArray
        }

        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(selectPlayerVC, animated: true)
        }
    }
    
    class func redirectToAddCash(amount: Int) {
                            
        let storybaord = UIStoryboard(name: "Main", bundle: nil)
        let addCashVC = storybaord.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
        addCashVC.selectedAmount = amount
        
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(addCashVC, animated: true)
        }
    }
    
    class func redirectToJoinLeagueConfirmation(details: MatchDetails, leagueDetails: LeagueDetails, teamsArray: Array<TeamDetails>, matchType: Int) {
                            
        let storybaord = UIStoryboard(name: "Main", bundle: nil)
        let confirmationVC = storybaord.instantiateViewController(withIdentifier: "JoinLeagueConfirmationViewController") as! JoinLeagueConfirmationViewController
        confirmationVC.matchDetails = details
        confirmationVC.leagueDetails = leagueDetails
        confirmationVC.userTeamsArray = teamsArray
        confirmationVC.matchType = matchType
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
            navVC.pushViewController(confirmationVC, animated: true)
        }
    }
    
    class func getMatchTime(timeResult: String) -> String {
        if let timeResult = Double(timeResult) {
            let date = Date(timeIntervalSince1970: timeResult)
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
            dateFormatter.timeZone = .current
            let localDate = dateFormatter.string(from: date)
            return localDate
        }
        return ""
    }
    
}


