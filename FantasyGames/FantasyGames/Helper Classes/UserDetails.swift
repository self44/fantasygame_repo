//
//  UserDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserDetails: NSObject {

    static let sharedInstance = UserDetails()

    lazy var userName = ""
    lazy var firstName = ""
    lazy var lastName = ""
    lazy var emailAdress = ""
    lazy var phoneNumber = ""
    lazy var city = ""
    lazy var zipCode = ""
    lazy var accessToken = ""
    lazy var deviceToken = ""
    lazy var imageURL = ""
    lazy var address = ""
    lazy var serverTimeStemp = "0"
    lazy var referralAmount = "0"
    lazy var gender = ""
    lazy var dateOfBirth = ""
    lazy var bonusCash = "0"
    lazy var totalWinningCash = "0"
    lazy var unusedAmount = "0"
    lazy var totalContestWon = "0"
    lazy var totalContestJoined = "0"
    lazy var totalMatchJoined = "0"
    lazy var totalSeriedJoined = "0"
    lazy var iOSMinVersion = ""
    lazy var iOSMaxVersion = ""
    lazy var updateAppUrl = ""
    lazy var notificationCount = ""
    lazy var ifscCode = ""
    lazy var bankName = ""
    lazy var branchName = ""
    lazy var bankImage = ""
    lazy var panName = ""
    lazy var panNumber = ""
    lazy var panDOB = ""
    lazy var panImage = ""

    lazy var isForceUpdateViewOpen: Bool = false
    lazy var isLogin = false
    lazy var isServerTimeRefereshed = false
    lazy var isEmailVerified: Bool = false
    lazy var phoneVerified = "0"
    lazy var panVerified = "0"
    lazy var bankVerified = "0"
    lazy var emailVerified = "0"
    
    let maxCreditLimitForClassic: Float = 100.0
//    let maxCreditLimitForBatting: Float = 45.0
//    let maxCreditLimitForBowling: Float = 45.0
//    lazy var maxTeamLimitForKabaddi: Int = 4
//    lazy var maxTeamLimitForFootball: Int = 4
//
    lazy var maxTeamLimitForCricket: Int = 4
    
//    lazy var maxTeamLimitForBatting: Int = 6
//    lazy var maxTeamLimtForBowling: Int = 6

    var timer: Timer?
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    
    @objc func updateServerTime()  {
        if (UserDetails.sharedInstance.serverTimeStemp.count == 0) || (UserDetails.sharedInstance.serverTimeStemp == "0"){
            return
        }

        if let serverTime = UInt64((UserDetails.sharedInstance.serverTimeStemp)){
            UserDetails.sharedInstance.serverTimeStemp = String(serverTime + 1)
        }
    }
    
    func startTimer()  {
        if timer != nil {
            return
        }

        updateServerTime()
        weak var weakSelf = self

        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            guard let weakObj = weakSelf else{
                return;
            }
            UIApplication.shared.endBackgroundTask(weakObj.backgroundTaskIdentifier!)
        })
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.updateServerTime()
        }
    }

    func parseUserDetails(response: JSON) {
        
        if let username = response["username"].string {
            UserDetails.sharedInstance.userName = username
        }
        
        if let email = response["email"].string {
            UserDetails.sharedInstance.emailAdress = email
        }
        
        if let gender = response["gender"].string {
            UserDetails.sharedInstance.gender = gender
        }
        
        if let dob = response["dob"].string {
            UserDetails.sharedInstance.dateOfBirth = dob
        }
        
        if let phoneNumber = response["phone"].string {
            UserDetails.sharedInstance.phoneNumber = phoneNumber
        }
        
        if let image = response["imagePath"].string {
            UserDetails.sharedInstance.imageURL = image
        }
        
        if let imageURL = response["image"].string {
            UserDetails.sharedInstance.imageURL = imageURL
        }

        if let ifscCode = response["ifscCode"].string {
            UserDetails.sharedInstance.dateOfBirth = ifscCode
        }
        
        UserDetails.sharedInstance.unusedAmount = String(format: "%.2f", response["unusedAmount"].floatValue)
        UserDetails.sharedInstance.bonusCash = String(format: "%.2f", response["bonusCash"].floatValue)
        UserDetails.sharedInstance.totalWinningCash = String(format: "%.2f", response["winningAmount"].floatValue)

        UserDetails.sharedInstance.phoneVerified = response["phoneVerified"].stringValue
        UserDetails.sharedInstance.emailVerified = response["emailVerified"].stringValue
        UserDetails.sharedInstance.panVerified = response["panVerified"].stringValue
        UserDetails.sharedInstance.bankVerified = response["bankVerified"].stringValue
        UserDetails.sharedInstance.notificationCount = response["notifications"].stringValue

        UserDetails.sharedInstance.totalContestJoined = response["totalContestJoined"].stringValue
        UserDetails.sharedInstance.totalContestWon = response["totalContestWon"].stringValue
    }
    
}
