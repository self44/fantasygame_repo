//
//  LocalMessages.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import Foundation

let kAlert = "Fantasyug"
let kLogoutMsg = "Are you sure, you want to logout?"
let kLogout = "Logout"


let kNoInternetConnection = "Check your Internet connection"
let kErrorMsg = "Something went wrong"

let SentVerificationMessage = "We have sent a verification code on  \n+91"


let EnterPhoneNumber = "Please enter your phone number."
let EnterOTP = "Please enter the OTP."
let MinPhoneNumber = "Phone number should have minimum 10 digits."
let UpdateUserName = "Please enter the user name"


let EnterFirstName = "Please enter your first name"
let EnterLastName = "Please enter your last name"
let SelectDateOfBirth = "Please select your date of birth"
let SelectGender = "Please select your gender"
let EnterEmail = "Please enter your email address"
let EnterValidEmail = "Please enter valid email address"

let maxLimitDefenders = "Not more than 4 Defenders"
let max2AllRoundersKabaddi = "You can select only 2 All Rounders"
let maxLimitRaider = "Not more than 3 Raiders"
let maxTeamPlayerForKabaddi = "Maximum 5 players from one team"
let max7ForKabaddiPlayer = "You have selected 7 players. Click on Next to select Captain & Vice Captain."
let picOneAllRoundersKabaddi = "Pick at least one All Rounder"

let max11Player = "Max 11 players allowed!\n Your team looks great! Go ahead, save your team."

let max7PlayerFromTeam = "Maximum 7 player from one team."

let max6Batsman = "Not more than 6 Batsmen."



let max6Bowler = "Not more than 6 Bowlers"

let max4Allrounder = "Not more than 4 all-rounders"

let max4WicketKeeper = "Not more than 1 Wicket-Keeper"

let pickOneKeeper = "Pick one Wicket-Keeper!"

let pickOneAllRounder = "Pick atleast 1 all-rounder"

let min3Batsman = "Pick minimun 3 batsman"

let min3Bowler = "Pick atleast 3 bowlers"

let max5BowlerFanticy = "Not more than 5 Bowler!\nWho will score the runs?"

let max3PlayerFromTeam = "Maximum 3 player from one team."

let bowlerSelectionPopUp = "This is batting fantasy! You want to choose a bowler?"

let battingSelectionPopUp = "This is bowling fantasy! You want to choose a batsman?"

let keeperCannotSelect = "You can't select keeper in bowling fantasy."

let SelectAadharImage = "Please select aadhar images."

let SelectMin3Bowlers = "You need to select atleast 3 Bowlers."

let SelectMin3Batsman = "You need to select atleast 3 batsmen."



