//
//  Constant.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import Foundation
import UIKit


enum MatchType: Int {
    case Cricket = 10000
    case Kabaddi = 20000
    case Football = 30000
}

enum SelectedTab: Int {
    case UpcomingMatches = 10000
    case myContest = 20000
    case myAccount = 30000
    case more = 40000
}

enum WebViewType: Int {
    
    case PointSysytem = 1000
    case HowToPlay
    case PrivacyPolices
    case TermsAndConditions
    case WithdrawalPolicies
    case AboutUs
    case FAQ
    case HelpDesk
}

enum ContestType: Int {
    case LiveContest = 100
    case UpcomingContest = 200
    case CompletedContest = 300
}

enum MatchStatus: String {
    case Inactive = "0"
    case VisiblelOnly = "1"
    case Active = "2"
    case MatchClosed = "3"
    case Live = "4"
    case PointsInReview = "5"
    case ReadyForCreditAssign = "6"
    case ReadyForDistribution = "7"
    case Completed = "8"
    case Abandoned = "9"
}


enum LeagueType: String {
    case Cash = "1"
    case Practice = "2"
    case FreeRoll = "3"
    case DepositerFreeRoll = "4"
}

enum PlayerRole: String {
    case Batsman = "Batsman"
    case Bowler = "Bowler"
    case WicketKeeper = "Wicketkeeper"
    case AllRounder = "Allrounder"
    case Raider = "raider"
    case Defender = "defender"
    case MidFielder = "midfielder"
    case GoalKeeper = "goalkeeper"
    case Sticker = "forward"
}


let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

let BASEURL = "http://3.6.23.104:4000/" // SERVER URL

//let BASEURL = "http://127.0.0.1:3000/"
//let BASEURL = "http://172.20.10.11:4000/"
//let BASEURL = "http://192.168.43.5:4000/"
//let BASEURL = "http://192.168.1.212:4000/"
//let BASEURL = "http://192.168.0.107:4000/"
//let BASEURL = "http://192.168.0.111:4000/"
//let BASEURL = "http://192.168.1.28:4000/"


//applinks:myfantasy.app.link
//applinks:myfantasy-alternate.app.link

let kUserTransaction = BASEURL + "users/txns"

let kAuthURL = BASEURL + "users/auth"
let kLoginURL = BASEURL + "users/login"
let kEditURL = BASEURL + "users/edit"
let kMatchURL = BASEURL + "cricket/match"
let kMatchPlayersURL = BASEURL + "cricket/matchPlayers"

let kGetNotificationURL = BASEURL + "users/getNotificationList"

let kUserProfile = BASEURL + "users/getUserProfile"
let kUpdateProfileURL = BASEURL + "users/updateProfile"
let kGetCricketTeamURL = BASEURL + "cricket/team"
let kGetUpdateTeamURL = BASEURL + "cricket/updateTeam"
let kCricketLeagueURL = BASEURL + "cricket/league?matchId="
let kValidateCricketLeagueURL = BASEURL + "cricket/leaguPreview"
let kCricketJoinLeague = BASEURL + "cricket/joinleague"
let kCricketJoinedAllMatched = BASEURL + "cricket/joinedmatches"
let kCricketJoinedLeagueURL = BASEURL + "cricket/joinedleague/"
let kSavePanDetailsURL = BASEURL + "users/uploadpan"
let kSaveBankDetailsURL = BASEURL + "users/uploadBank"
let kLeagueWiningsURL = BASEURL + "cricket/leagueWining"
let kUploadProfilePicURL = BASEURL + "users/uploadProfilePic"


let kCricketLeagueJoinedTeams = BASEURL + "cricket/leaguejoinedteams?matchId="
let kGetCricketWinnersURL = BASEURL + "cricket/team/"
let kGetCricketPlaying22URL = BASEURL + "cricket/points/"
let kTeamPlayersURL = BASEURL + "cricket/teamPlayers"
let kGetPromocodesURL = BASEURL + "users/getPromocode"
let kAddCashURL = BASEURL + "users/addcash"
let kGetIFSCCodeURL = BASEURL + "users/getIfscCode/"
let kGetKYCdetailsURL = BASEURL + "users/kycInfo"

let kAddWithdrawRequestURL = BASEURL + "users/addRequest"
let kCancelWithdrawRequestURL = BASEURL + "users/cancelRequest"
let kGetHtmlPagesURL = BASEURL + "users/getHtmlPages"



let kUserID = "user_id"
let kUserName = "user_name"
let kLastName = "FirstName"
let kFirstName = "FirstName"
let kEmailAdress = "user_name"
let kPhoneNumber = "user_name"
let kDeviceToken = "user_name"
let kImageURL = "user_name"
let kAccessToken = "access_token"
let kGender = "gender"
let kDOB = "DOB"




let kCancel = "Cancel"
let kAddCash = "Add Cash"


