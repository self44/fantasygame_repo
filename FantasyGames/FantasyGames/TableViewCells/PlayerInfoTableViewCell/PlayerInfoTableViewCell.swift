//
//  PlayerInfoTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 07/01/20.
//  Copyright © 2020 App Creatives Technologies. All rights reserved.
//

import UIKit

class PlayerInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblPlayerCredits: UILabel!
    
    @IBOutlet weak var lblPlayerRole: UILabel!
    @IBOutlet weak var playerImgView: UIImageView!
    @IBOutlet weak var lblPlayerTotalPoints: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
