//
//  TransactionsTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 17/12/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

enum TransactionType: String {
    case LeagueJoined = "1"
    case MatchRefund = "2"
    case BonusAdded = "3"
    case UnusedAdded = "4"
    case WinningsAdded = "5"
    case Deposit = "6"
    case AdminDeposit = "7"
    case AdminCredit = "8"

}
    
class TransactionsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(details: UserTransactionDetails) {
                    
        lblMessage.text = details.message
        lblAmount.text = details.amount
        
        if (details.type == TransactionType.LeagueJoined.rawValue) || (details.type == TransactionType.AdminCredit.rawValue) {
            lblAmount.text = "-₹" + details.amount
        }
        else if (details.type == TransactionType.BonusAdded.rawValue) || (details.type == TransactionType.MatchRefund.rawValue) || (details.type == TransactionType.UnusedAdded.rawValue) || (details.type == TransactionType.BonusAdded.rawValue) || (details.type == TransactionType.WinningsAdded.rawValue) || (details.type == TransactionType.Deposit.rawValue) || (details.type == TransactionType.AdminDeposit.rawValue){
            lblAmount.text = "+₹" + details.amount
            lblAmount.textColor = UIColor(red: 230.0/255, green: 190.0/255, blue: 70.0/255, alpha: 1)
        }
    
    }
    
}
