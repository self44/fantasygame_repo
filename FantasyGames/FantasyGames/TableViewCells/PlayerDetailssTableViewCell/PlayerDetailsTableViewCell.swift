//
//  PlayerDetailssTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 13/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import MapleBacon

class PlayerDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var announcedDotView: UIView!
    @IBOutlet weak var grayOutView: UIView!
    @IBOutlet weak var playerPlayingStatus: UILabel!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var playerPointInfoButton: UIButton!
    @IBOutlet weak var addPlayerButton: UIButton!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var playerImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configDetails(details: PlayerDetails, gameType: Int)  {
        
        lblPlayerName.text = details.playerShortName
        lblCredits.text = details.playerCredits
        
        if details.isSelected {
//            addPlayerButton.setImage(UIImage(named: "RemovePlayerIcon"), for: .normal)
            backgroundColor = UIColor(red: 213.0/255, green: 213.0/255, blue: 213.0/255, alpha: 1)
        }
        else{
//            addPlayerButton.setImage(UIImage(named: "AddPlayerIcon"), for: .normal)
            backgroundColor = UIColor.white
        }
        
        lblTeam.text = details.teamName
        lblPoints.text = details.playerPoints
        if details.isPlaying{
            playerPlayingStatus.isHidden = false
            announcedDotView.isHidden = false
        }
        else{
            playerPlayingStatus.isHidden = true
            announcedDotView.isHidden = true
        }

        var placeholder = "MalePlayerPhotoPlaceholder"
        if details.gender == "f" {
            placeholder = "FemalePlayerPhotoPlaceholder"
        }
        
        if details.playerImage.count > 0 {
            if let url = URL(string: details.playerImage){
                playerImageView.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerImageView.image = image
                    }
                    else{
                        self?.playerImageView.image = UIImage(named: placeholder)
                    }
                })
            }
        }
        else{
            playerImageView.image = UIImage(named: placeholder)
        }
    }

    
    
    
}
