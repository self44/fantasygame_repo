//
//  PlayerPointsTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class PlayerPointsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblActualScore: UILabel!
    @IBOutlet weak var lblEventType: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
