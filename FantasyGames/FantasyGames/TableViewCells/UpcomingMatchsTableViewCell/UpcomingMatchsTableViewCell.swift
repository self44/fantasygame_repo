//
//  UpcomingMatchsTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 05/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class UpcomingMatchsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var liveView: UIView!
    @IBOutlet weak var liveBlinkingView: UIView!
    @IBOutlet weak var lblLive: UILabel!
    @IBOutlet weak var team1ImgView: UIImageView!
    @IBOutlet weak var team2ImgView: UIImageView!
    @IBOutlet weak var lblMatchTime: UILabel!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblSeriesName: UILabel!
    @IBOutlet weak var lblLineupsOut: UILabel!
    @IBOutlet weak var lblTeam2Name: UILabel!
    @IBOutlet weak var lblTeam1Name: UILabel!
    @IBOutlet weak var lblTimeLeft: UILabel!
    
    var timer: Timer?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configData(details: MatchDetails, contestType: Int) {
        
        liveView.layer.cornerRadius = 9.0
        liveView.layer.borderWidth = 1.0
        liveView.layer.borderColor = liveBlinkingView.backgroundColor!.cgColor
        liveView.backgroundColor = UIColor.white
        liveBlinkingView.layer.cornerRadius = 6.0
        liveBlinkingView.clipsToBounds = true
        
        lblSeriesName.text = details.seriesName.uppercased()
        lblTeam1Name.text = details.team1ShortName
        lblTeam2Name.text = details.team2ShortName
        lblMatchTime.text = details.matchTime
        lblLineupsOut.text = ""
        contentView.appbackgroudColor()
        innerView.cellShadow()
        
        liveView.isHidden = true
        lblLive.isHidden = true
        lblMatchTime.isHidden = true
        liveBlinkingView.isHidden = true
        transparentView.isHidden = true
        lblTimeLeft.isHidden = false
        lblTimeLeft.textColor = UIColor.appThemeColorColor()

        if details.matchStatus == MatchStatus.VisiblelOnly.rawValue {
            transparentView.isHidden = false
            transparentView.backgroundColor = UIColor(red: 220.0/255, green: 220.0/255, blue: 220.0/255, alpha: 0.35)
        }

        
        if contestType == ContestType.LiveContest.rawValue {
            liveView.isHidden = false
            liveBlinkingView.isHidden = false
            lblLive.isHidden = false
            lblTimeLeft.isHidden = true
            startLiveTimer()
        }
        else if contestType == ContestType.UpcomingContest.rawValue {
            let closingTimeLeft = AppHelpers.getMatchClosingTime(matchTimestamp: details.startDateUnix)
            lblTimeLeft.text = closingTimeLeft
            lblMatchTime.isHidden = false
            if details.showLineupsOut == "1" {
                lblLineupsOut.text = "LINEUPS OUT"
            }
            
            if details.matchStatus == MatchStatus.MatchClosed.rawValue{
                lblTimeLeft.text = "League Closed"
                lblTimeLeft.textColor = liveBlinkingView.backgroundColor
                lblMatchTime.isHidden = true
            }
            
        }
        else if contestType == ContestType.CompletedContest.rawValue {
            if (details.matchStatus == MatchStatus.PointsInReview.rawValue) || (details.matchStatus == MatchStatus.ReadyForCreditAssign.rawValue) || (details.matchStatus == MatchStatus.ReadyForDistribution.rawValue){
                lblTimeLeft.text = "Points in review"
                lblTimeLeft.textColor = liveBlinkingView.backgroundColor
            }
            else{
                lblTimeLeft.text = details.matchCompleteMessage
            }
        }
        
        showTeamImages(details: details)
    }
    
    func showTeamImages(details: MatchDetails) {
           
         if details.team1Image.count > 0 {
             if let url = URL(string: details.team1Image){
                 team1ImgView.setImage(with: url, placeholder: UIImage(named: "TeamImagePlaceholder"), progress: { received, total in
                     // Report progress
                 }, completion: { [weak self] image in
                     if (image != nil){
                         self?.team1ImgView.image = image
                     }
                     else{
                         self?.team1ImgView.image = UIImage(named: "TeamImagePlaceholder")
                     }
                 })
             }
         }
         else{
             team1ImgView.image = UIImage(named: "TeamImagePlaceholder")
         }
                 
         if details.team2Image.count > 0 {
             if let url = URL(string: details.team2Image){
                 team2ImgView.setImage(with: url, placeholder: UIImage(named: "TeamImagePlaceholder"), progress: { received, total in
                     // Report progress
                 }, completion: { [weak self] image in
                     if (image != nil){
                         self?.team2ImgView.image = image
                     }
                     else{
                         self?.team2ImgView.image = UIImage(named: "TeamImagePlaceholder")
                     }
                 })
             }
         }
         else{
             team2ImgView.image = UIImage(named: "TeamImagePlaceholder")
         }
    }
    
    func startLiveTimer()  {
        if timer == nil {
            weak var weakSelf = self
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true){_ in
                weakSelf?.showLiveAnimation()
            }
        }
    }

    @objc func showLiveAnimation()  {
        weak var weakSelf = self

        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            weakSelf?.liveBlinkingView.alpha = 0.0
        }, completion: {
            (finished: Bool) -> Void in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                weakSelf?.liveBlinkingView.alpha = 1.0
            }, completion: nil)
        })
    }
}
