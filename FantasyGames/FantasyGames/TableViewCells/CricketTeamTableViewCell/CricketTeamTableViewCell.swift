//
//  CricketTeamTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class CricketTeamTableViewCell: UITableViewCell {
    @IBOutlet weak var upperView: UIView!
    
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var viceCaptainImgView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var captainImgView: UIImageView!

    @IBOutlet weak var lblTeam1PlayerCount: UILabel!
    @IBOutlet weak var lblTeam2PlayerCount: UILabel!
    @IBOutlet weak var lblTeam1Name: UILabel!
    @IBOutlet weak var lblTeam2Name: UILabel!

    @IBOutlet weak var lblWicketKeeprCount: UILabel!
    @IBOutlet weak var lblBatsmanCount: UILabel!
    @IBOutlet weak var lblAllRoundersCount: UILabel!
    @IBOutlet weak var lblBowlerCount: UILabel!
    @IBOutlet weak var lblUserTeamName: UILabel!
    @IBOutlet weak var lblCaptainName: UILabel!
    @IBOutlet weak var lblViceCaptainName: UILabel!

    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var cloneButton: UIButton!
    @IBOutlet weak var previewButton: UIButton!
    
    @IBOutlet weak var teamNameLeadingConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(details: TeamDetails, selectedTeam: TeamDetails?, isForJoineLeague: Bool) {
        innerView.cellShadow()
        selectButton.isSelected = false

        if selectedTeam?.teamNumber == details.teamNumber{
            selectButton.isSelected = true
        }
           
        if !isForJoineLeague {
            teamNameLeadingConstraint.constant = 15.0
            selectButton.isHidden = true
        }
        
        lblUserTeamName.text = "Team: (\(details.teamNumber))"
        
        lblCaptainName.text = details.captainName
        lblViceCaptainName.text = details.viceCaptainName
        
        var placeholder = "MalePlayerPhotoPlaceholder"
        if details.gender == "f" {
            placeholder = "FemalePlayerPhotoPlaceholder"
        }
        
        showCaptainImage(playerImage: details.captainImageName, placeholder: placeholder)
        showViceCaptainImage(playerImage: details.viceCaptainImageName, placeholder: placeholder)
        
        lblWicketKeeprCount.text = "(\(details.totalWicketKeeprCount))"
        lblBatsmanCount.text = "(\(details.totalBatsmanCount))"
        lblAllRoundersCount.text = "(\(details.totalAllRounderCount))"
        lblBowlerCount.text = "(\(details.totalBowlerCount))"
        
        lblTeam1Name.text = details.team1Name
        lblTeam2Name.text = details.team2Name
        lblTeam1PlayerCount.text = "(\(details.team1PlayerCount))"
        lblTeam2PlayerCount.text = "(\(details.team2PlayerCount))"
        weak var weakSelf = self

        DispatchQueue.main.async {
                    
            weakSelf?.upperView.roundViewCorners(corners:[.topLeft, .topRight], radius: 5)
            weakSelf?.bottomView.roundViewCorners(corners:[.bottomLeft, .bottomRight], radius: 5)

        }
    }
    
    func showCaptainImage(playerImage: String, placeholder: String) {
        if playerImage.count > 0 {
            if let url = URL(string: playerImage){
                captainImgView.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.captainImgView.image = image
                    }
                    else{
                        self?.captainImgView.image = UIImage(named: placeholder)
                    }
                })
            }
        }
        else{
            captainImgView.image = UIImage(named: placeholder)
        }
    }
    
    func showViceCaptainImage(playerImage: String, placeholder: String) {
        if playerImage.count > 0 {
            if let url = URL(string: playerImage){
                viceCaptainImgView.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.viceCaptainImgView.image = image
                    }
                    else{
                        self?.viceCaptainImgView.image = UIImage(named: placeholder)
                    }
                })
            }
        }
        else{
            viceCaptainImgView.image = UIImage(named: placeholder)
        }
    }
}
