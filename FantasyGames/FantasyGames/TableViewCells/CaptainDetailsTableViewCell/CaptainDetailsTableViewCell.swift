//
//  CaptainDetailsTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 11/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class CaptainDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var viceCaptainButton: UIButton!
    @IBOutlet weak var captainButton: UIButton!

    @IBOutlet weak var lblPlayerPoints: UILabel!
    @IBOutlet weak var lblPlayerRole: UILabel!
    
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var lblPlayerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(details: PlayerDetails, gameType: Int){
        
        contentView.backgroundColor = UIColor.white
        captainButton.backgroundColor = UIColor.white
        viceCaptainButton.backgroundColor = UIColor.white
        captainButton.setTitle("C", for: .normal)
        viceCaptainButton.setTitle("VC", for: .normal)
        captainButton.layer.borderWidth = 1.0
        viceCaptainButton.layer.borderWidth = 1.0
        captainButton.layer.borderColor = UIColor.appGrayColor().cgColor
        viceCaptainButton.layer.borderColor = UIColor.appGrayColor().cgColor
        captainButton.setTitleColor(UIColor.appGrayColor(), for: .normal)
        viceCaptainButton.setTitleColor(UIColor.appGrayColor(), for: .normal)
        contentView.backgroundColor = UIColor.white
        
        lblTeamName.text = details.teamName
        lblPlayerName.text = details.playerShortName
        lblPlayerRole.text = AppHelpers.getPlayerRole(details: details).uppercased()
        lblPlayerPoints.text = details.playerPoints + " pts "
        var placeholder = "MalePlayerPhotoPlaceholder"
        if details.gender == "f" {
            placeholder = "FemalePlayerPhotoPlaceholder"
        }

        if details.playerImage.count > 0 {
            if let url = URL(string: details.playerImage){
                playerImage.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerImage.image = image
                    }
                    else{
                        self?.playerImage.image = UIImage(named: placeholder)
                    }
                })
            }
        }
        else{
            playerImage.image = UIImage(named: placeholder)
        }        
    }
    
}
