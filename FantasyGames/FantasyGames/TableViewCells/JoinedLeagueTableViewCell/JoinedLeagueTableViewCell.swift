//
//  JoinedLeagueTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 21/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class JoinedLeagueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var lblTotalWinners: UILabel!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblJoiningTeams: UILabel!
    @IBOutlet weak var lblPricePool: UILabel!
    @IBOutlet weak var lblEntryFee: UILabel!
    @IBOutlet weak var lblLeagueName: UILabel!
   
    @IBOutlet weak var downArrowImgView: UIImageView!
    @IBOutlet weak var winnersButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(details: LeagueDetails, matchDetails: MatchDetails) {
        innerView.cellShadow()
        lblEntryFee.text = "Entry Fee: ₹" + details.joiningAmount
        lblLeagueName.text = details.leagueName
        lblPricePool.text = "₹\(details.pricePool)"
        lblTotalWinners.text = details.totalWinners
        lblRank.text = "#\(details.rank)"
        downArrowImgView.isHidden = false
        
        if (details.totalWinners == "0") || (details.totalWinners == "") {
            downArrowImgView.isHidden = true
        }
        
        if matchDetails.isClosed {
            lblJoiningTeams.text = details.totalJoined
        }
        else{
            lblJoiningTeams.text = "\(details.totalJoined)/\(details.maxPlayers)"
        }
        weak var weakSelf = self

        DispatchQueue.main.async {
            weakSelf?.upperView.roundViewCorners(corners: [.topLeft, .topRight], radius: 5)
            weakSelf?.bottomView.roundViewCorners(corners: [.bottomLeft, .bottomRight], radius: 5)
        }
    }
    
}
