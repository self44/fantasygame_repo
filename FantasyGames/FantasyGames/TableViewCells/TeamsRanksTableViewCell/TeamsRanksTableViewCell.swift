//
//  TeamsRanksTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 21/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class TeamsRanksTableViewCell: UITableViewCell {

    @IBOutlet weak var lblWinningZone: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    
    @IBOutlet weak var lblTeamName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(details: JoinedLeaguesTeamsDetails, isMatchClosed: Bool, totoalWinners: Int, matchStatus: String) {
        lblTeamName.text = details.userName
        lblWinningZone.isHidden = true
        if isMatchClosed {
            lblPoints.text = details.userPoints + " pts"
            lblRank.text = "#\(details.userRank)"
            if Int(details.userRank) ?? 0 <= totoalWinners {
                lblWinningZone.isHidden = false
                lblWinningZone.text = "IN WINNING ZONE"
            }
            
            if matchStatus == MatchStatus.Completed.rawValue {
                if (details.creditAmount.count > 0) && (details.creditAmount != "0") {
                    lblWinningZone.text = "Won ₹\(details.creditAmount)"
                }
                else{
                    lblWinningZone.isHidden = true
                }
            }
            else if (matchStatus == MatchStatus.Abandoned.rawValue) || (matchStatus == MatchStatus.MatchClosed.rawValue)  {
                lblWinningZone.isHidden = true
            }

        }
        else{
            lblPoints.text = "Wait"
            lblRank.text = ""
        }
        
        var placeholder = "MalePlayerPhotoPlaceholder"
        if details.gender == "f" {
            placeholder = "FemalePlayerPhotoPlaceholder"
        }
        
        if details.userImgUrl.count > 0 {
            if let url = URL(string: details.userImgUrl){
                imgView.setImage(with: url, placeholder: UIImage(named: placeholder), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.imgView.image = image
                    }
                    else{
                        self?.imgView.image = UIImage(named: placeholder)
                    }
                })
            }
        }
        else{
            imgView.image = UIImage(named: placeholder)
        }
    }
    
}
