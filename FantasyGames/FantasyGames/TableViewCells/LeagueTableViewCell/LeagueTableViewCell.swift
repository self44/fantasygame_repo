//
//  LeagueTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class LeagueTableViewCell: UITableViewCell {
    @IBOutlet weak var lblLeagueName: UILabel!
    
    @IBOutlet weak var multiTeamTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var progressViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var winnerButton: UIButton!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblTotalWinners: UILabel!
    @IBOutlet weak var lblJoiningAmount: UILabel!
    @IBOutlet weak var lblJoinedTeamCount: UILabel!
    @IBOutlet weak var lblRemaingTeamCount: UILabel!

    @IBOutlet weak var lblConfirmLabel: UILabel!
    @IBOutlet weak var lblMultiTeamLabel: UILabel!
    @IBOutlet weak var lblBonusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configData(details: LeagueDetails) {
        


        lblConfirmLabel.layer.borderColor = UIColor.appGrayColor().cgColor
        lblMultiTeamLabel.layer.borderColor = UIColor.appGrayColor().cgColor
        lblBonusLabel.layer.borderColor = UIColor.appGrayColor().cgColor
        lblBonusLabel.layer.borderWidth = 0.8
        lblMultiTeamLabel.layer.borderWidth = 0.8
        lblConfirmLabel.layer.borderWidth = 0.8
        lblMultiTeamLabel.layer.cornerRadius = 3
        lblBonusLabel.layer.cornerRadius = 3
        lblConfirmLabel.layer.cornerRadius = 3
        innerView.cellShadow();
        lblConfirmLabel.isHidden = true
        lblMultiTeamLabel.isHidden = true
        lblBonusLabel.isHidden = true
        
        if details.isConfirmedLeague {
            lblConfirmLabel.isHidden = false
        }
        
        if details.isMultiEntryLeague {
            lblMultiTeamLabel.isHidden = false
            lblMultiTeamLabel.text = "M\(UserDetails.sharedInstance.maxTeamLimitForCricket)"
        }

        if details.isBonusApplicable{
            lblBonusLabel.isHidden = false
            lblBonusLabel.text = "B\(details.bonusPercentage)%"
        }
        
        lblLeagueName.text = details.leagueName
        if (details.totalWinners == "0") || (details.totalWinners == "") {
            lblTotalWinners.text = "No Winners"
        }
        else{
            lblTotalWinners.text = details.totalWinners + " Winners"
        }
        if (details.joiningAmount == "0") || (details.joiningAmount == ""){
            lblJoiningAmount.text = "FREE"
        }
        else{
            lblJoiningAmount.text = "₹" + details.joiningAmount
        }
        
        if details.isBonusApplicable && details.isMultiEntryLeague && details.isConfirmedLeague {
            multiTeamTrailingConstraint.constant = 60
            confirmTrailingConstraint.constant = 100
        }
        else if details.isMultiEntryLeague && details.isConfirmedLeague {
            multiTeamTrailingConstraint.constant = 10
            confirmTrailingConstraint.constant = 50
        }
        else if details.isBonusApplicable && details.isMultiEntryLeague {
            multiTeamTrailingConstraint.constant = 60
        }
        else if details.isBonusApplicable && details.isConfirmedLeague {
            confirmTrailingConstraint.constant = 60
        }
        else if details.isMultiEntryLeague {
            multiTeamTrailingConstraint.constant = 10
        }
        else if details.isConfirmedLeague {
            confirmTrailingConstraint.constant = 10
        }
        
        let maxPlayers = (Int(details.maxPlayers) ?? 0)
        let totalJoined = (Int(details.totalJoined) ?? 0)
        let reamingPlayer = maxPlayers - totalJoined
        
        let screenWidth = Int(UIScreen.main.bounds.width - 38) * totalJoined/maxPlayers
        progressViewWidthConstraint.constant = CGFloat(screenWidth)
        layoutIfNeeded()
        progressView.removeProgressGradient()
        
        progressView.progressGradient()

        lblJoinedTeamCount.text = "\(reamingPlayer) spots left"
        lblRemaingTeamCount.text = details.maxPlayers + " spots"

        weak var weakSelf = self

        DispatchQueue.main.async {
            weakSelf?.bottomView.roundViewCorners(corners:[.bottomLeft, .bottomRight], radius: 5)
        }
    }
    
}
