//
//  Playing22PointsTableViewCell.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class Playing22PointsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblPlayerName: UILabel!
    
    @IBOutlet weak var plyaerRoleImgView: UIImageView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblPlayerPoints: UILabel!
    @IBOutlet weak var playerImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(details: PlayerDetails) {
        
        lblPlayerName.text = details.playerName   
        lblTeamName.text = details.teamName
        lblPlayerPoints.text = details.playerScore
        
        if details.playerImage.count > 0 {
            if let url = URL(string: details.playerImage){
                playerImgView.setImage(with: url, placeholder: UIImage(named: "MalePlayerPhotoPlaceholder"), progress: { received, total in
                    // Report progress
                }, completion: { [weak self] image in
                    if (image != nil){
                        self?.playerImgView.image = image
                    }
                    else{
                        self?.playerImgView.image = UIImage(named: "MalePlayerPhotoPlaceholder")
                    }
                })
            }
        }
        else{
            playerImgView.image = UIImage(named: "MalePlayerPhotoPlaceholder")
        }

    }
}
