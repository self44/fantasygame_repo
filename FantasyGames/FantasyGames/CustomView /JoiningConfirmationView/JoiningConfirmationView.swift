//
//  JoiningConfirmationView.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 04/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

protocol JoiningConfirmationViewDelegate {
    func redirectbackNavigation()
}

class JoiningConfirmationView: UIView {

    @IBOutlet weak var lblLeagueEntryFee: UILabel!
    @IBOutlet weak var joinLeagueButton: UIButton!
    @IBOutlet weak var lblSelectedTeamCount: UILabel!
    @IBOutlet weak var lblUsableCashBonusAmount: UILabel!
    @IBOutlet weak var lblToPayAmount: UILabel!
    @IBOutlet weak var lblTotalJoiningAmount: UILabel!

    var leagueDetails: LeagueDetails?
    var matchDetails: MatchDetails?
    var delegate: JoiningConfirmationViewDelegate?
    
    lazy var selectedTeamsArray = Array<TeamDetails>()
    lazy var matchType = MatchType.Cricket.rawValue
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("JoiningConfirmationView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            joinLeagueButton.gradient()
            addSubview(view)
        }
    }
    
    func updateData() {

        guard let leagueInfo = leagueDetails else {
            return
        }
        
        lblLeagueEntryFee.text = "₹" + leagueInfo.joiningAmount
        let leagueJoiningAmount = Float(leagueInfo.joiningAmount) ?? 0
        lblSelectedTeamCount.text = "x\(selectedTeamsArray.count)"
        lblTotalJoiningAmount.text = String(leagueJoiningAmount * Float(selectedTeamsArray.count))
        
        if leagueInfo.isBonusApplicable {
            let bonusPercentage = Float(leagueInfo.bonusPercentage) ?? 0
            var bonusAmount = leagueJoiningAmount * bonusPercentage/100
            if bonusAmount >= leagueJoiningAmount {
                bonusAmount = leagueJoiningAmount
            }
            let avilableBonusAmount = Float(UserDetails.sharedInstance.bonusCash) ?? 0
            if bonusAmount >= avilableBonusAmount {
                bonusAmount = avilableBonusAmount
            }
            
            lblUsableCashBonusAmount.text = "-₹\(bonusAmount)"
            lblToPayAmount.text = "₹ \(leagueJoiningAmount - bonusAmount)"
        }
        else{
            lblUsableCashBonusAmount.text = "₹0"
            lblToPayAmount.text = "₹" + leagueInfo.joiningAmount
        }
    }
    
    @IBAction func joinButtonTapped(_ sender: Any) {
   
        if selectedTeamsArray.count == 0{
            AppHelpers.showCustomErrorAlertView(message: "Please select a team")
            return
        }
        
        guard let leagueInfo = leagueDetails else {
            return
        }
        
        var teamNumber = ""
        
        for details in selectedTeamsArray {
            if teamNumber.count == 0 {
                teamNumber = details.teamNumber
            }
            else{
                teamNumber = teamNumber + "," + details.teamNumber
            }
        }
        
        callJoinLeagueAPI(leagueDetails: leagueInfo, teamNumber: teamNumber)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        removeFromSuperview()
    }
    
    func callJoinLeagueAPI(leagueDetails: LeagueDetails, teamNumber: String) {
        
        guard let matchDetails = matchDetails else {
            return;
        }
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }

        AppHelpers.sharedInstance.displayLoader()

        var urlString = ""
        if matchType == MatchType.Cricket.rawValue {
            urlString = kCricketJoinLeague
        }
                
        let params = ["matchId": matchDetails.matchID, "leagueId": leagueDetails.leagueId, "teamNumber": teamNumber]
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
                    
            AppHelpers.sharedInstance.removeLoader()
            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? kErrorMsg

                if statusCode == "200" {
                    AppHelpers.showCustomAlertView(message: message)
                    weakSelf?.delegate?.redirectbackNavigation()
                    weakSelf?.removeFromSuperview()
                }
                else if statusCode == "405"{
                    weakSelf?.removeFromSuperview()
                    let matchCloseView = MatchClosedView(frame: APPDELEGATE.window!.frame)
                    APPDELEGATE.window?.addSubview(matchCloseView)
                }
                else {
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
            }
        }
    }
        
}
