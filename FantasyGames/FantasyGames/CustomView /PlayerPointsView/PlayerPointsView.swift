//
//  PlayerPointsView.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 09/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class PlayerPointsView: UIView {
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblPlayerPoints: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblCaptainTag: UILabel!
    
    lazy var matchClosed = false
    var playerDetails: PlayerDetails?

    lazy var matchType = MatchType.Cricket.rawValue
    

    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("PlayerPointsView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            addSubview(view)
        }
    }
    
    func showPlayerInformation(details: PlayerDetails, gameType: Int, isMatchClosed: Bool, team1Name: String) -> Float {
        
        var totalPoints: Float = 0.0
        matchClosed = isMatchClosed

        var placeholder = "MalePlayerPhotoPlaceholder"
        if details.gender == "f" {
            placeholder = "FemalePlayerPhotoPlaceholder"
        }

        if let url = NSURL(string: details.playerImage){
            imgView.setImage(with: url as URL, placeholder: UIImage(named: placeholder), progress: { received, total in
                // Report progress
            }, completion: { [weak self] image in
                if (image != nil){
                    self?.imgView.image = image
                }
                else{
                    self?.imgView.image = UIImage(named: placeholder)
                }
            })
        }
        else{
            imgView.image = UIImage(named: placeholder)
        }
        

//        if firstTeamName == details.teamShortName{
//            lblName.backgroundColor = kTeam2Color
//        }
//        else if firstTeamName == details.teamKey{
//            lblName.backgroundColor = kTeam2Color
//        }
//        else{
//            lblName.backgroundColor = kTeam1Color
//        }
        
        playerDetails = details
        lblPlayerName.text = details.playerShortName
        lblCaptainTag.isHidden = true
        
        
        if matchType == MatchType.Cricket.rawValue {

            if isMatchClosed{
                if details.isCaption{
                    totalPoints = totalPoints + (Float(details.captainScore) ?? 0)
                    lblPlayerPoints.text = details.captainScore + " Pts"
                }
                else if details.isViceCaption{
                    totalPoints = totalPoints + (Float(details.viceCaptainScore) ?? 0)
                    lblPlayerPoints.text = details.viceCaptainScore + " Pts"
                }
                else{
                    totalPoints = totalPoints + (Float(details.playerScore) ?? 0)
                    lblPlayerPoints.text = details.playerScore + " Pts"
                }
            }
            else{
                lblPlayerPoints.text = details.playerCredits + " CR"
                totalPoints = totalPoints + (Float(details.playerCredits) ?? 0)
            }
        }
        
        if details.isCaption{
            lblCaptainTag.text = "C"
            lblCaptainTag.isHidden = false
        }
        else if details.isViceCaption{
            lblCaptainTag.text = "VC"
            lblCaptainTag.isHidden = false
        }

        
        return totalPoints
    }
}
