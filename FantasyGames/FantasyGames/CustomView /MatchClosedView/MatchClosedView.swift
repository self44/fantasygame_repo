//
//  MatchClosedView.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 05/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class MatchClosedView: UIView {

    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("MatchClosedView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }
    
    
    @IBAction func okButtonTapped(_ sender: Any) {
        removeFromSuperview()
        if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navVC.popToRootViewController(animated: true)
        }
    }
}
