//
//  AppLoaderView.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 05/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//


import UIKit

class AppLoaderView: UIView {
    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("AppLoaderView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            
            view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            addSubview(view)
        }
    }

    
}
