//
//  CustomAlertView.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class CustomAlertView: UIView {

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    var timer: Timer?

    
    // MARK: init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if subviews.count == 0 {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        if let view = Bundle.main.loadNibNamed("CustomAlertView", owner: self, options: nil)?.first as? UIView {
            view.frame = bounds
            view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
            view.backgroundColor = .clear
            addSubview(view)
            
            viewShadow()
        }
    }
    
    func viewShadow()  {
        
        messageView.layer.cornerRadius = 7.0
        messageView.layer.shadowColor = UIColor.lightGray.cgColor
        messageView.layer.shadowOffset = CGSize(width: 0, height: 1)
        messageView.layer.shadowOpacity = 0.5
        messageView.layer.shadowRadius = 2.5
        
        messageView.layer.masksToBounds = false
    }
    
    @objc func removeViewFromSuperView(){
        timer?.invalidate()
        timer = nil
        weak var weakSelf = self

        frame.origin.y = 0
        UIView.animate(withDuration: 0.5, animations: {
            weakSelf?.frame.origin.y = -100
        }, completion: {res in
        })
    }
    
    func messageDisplay(message:String, color: UIColor?){
        lblTitle.text = message
        messageView.backgroundColor = color
    }
    
    
    func showAnimation() {
        
        var topMargin:CGFloat = 20.0
        if AppHelpers.isApplicationRunningOnIphoneX(){
            topMargin = 40.0;
        }
        frame.origin.y = -80
        weak var weakSelf = self

        UIView.animate(withDuration: 0.5, animations: {
            weakSelf?.frame.origin.y = topMargin
        }, completion: {res in
        })

        if #available(iOS 10.0, *) {
            weak var weakSelf = self
            
            timer = Timer.scheduledTimer(withTimeInterval: 2.5, repeats: false){_ in
                weakSelf?.removeViewFromSuperView()
            }
        }
        else {
            timer = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(removeViewFromSuperView), userInfo: nil, repeats: false)
        }

    }

    @IBAction func removeButtonTapped(_ sender: Any) {
        removeViewFromSuperView()
    }
}
