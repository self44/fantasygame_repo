
//
//  TransactionsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 01/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class TransactionsViewController: UIViewController {
    lazy var pageLimit = 30
    lazy var pageNumber = 0
    var isNeedToShowLoadMore = false

    @IBOutlet weak var containerView: UIView!
    var transactionDetailsArray = Array<[String: Any]>()

    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.topViewCorners()
        view.headerGradient()

        tblView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        tblView.register(UINib(nibName: "TransactionsTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionsTableViewCell")
        tblView.tableFooterView = UIView()
        callGetUserTransactions(isShowLoader: true);
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func callGetUserTransactions(isShowLoader: Bool) {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        if isShowLoader{
            AppHelpers.sharedInstance.displayLoader()
        }
        

        weak var weakSelf = self
        let params = ["pageStart": String(pageNumber), "limit": String(pageLimit)]

        WebServiceHandler.performPOSTRequest(urlString: kUserTransaction, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                if status == "200"{
                    if let dataArray = result!["alltxns"].array {

                        if weakSelf?.pageNumber == 0{
                            weakSelf?.pageNumber += 1
                            weakSelf?.transactionDetailsArray.removeAll();
                        }
                        else{
                            weakSelf?.pageNumber += 1
                        }
                        
                        let detailsTempArray = UserTransactionDetails.getUserTransactionArray(dataArray: dataArray)
                        if detailsTempArray.count >= weakSelf!.pageLimit{
                            weakSelf?.isNeedToShowLoadMore = true;
                        }

                        
                        let tempArray = detailsTempArray.enumerated().map { (index,element) in
                            element.createdDate
                        }

                        let tempSetArray = Array(Set(tempArray))
                        let setArray = AppHelpers.sortDateArray(dataArray: tempSetArray)
                        
                        let lastObj = weakSelf?.transactionDetailsArray.last
                        
                        for dateStr in setArray{
                            let transactionArray = detailsTempArray.filter({ (details) -> Bool in
                                details.createdDate == dateStr
                            })
                            if lastObj != nil{
                                let dateTitle = lastObj!["date"] as! String
                                
                                if dateTitle == dateStr{
                                    let detailsArray = lastObj!["details_list"] as! Array<UserTransactionDetails>

                                    let newTransactionArray = detailsArray + transactionArray
                                    weakSelf?.transactionDetailsArray.removeLast()
                                    let tempDetails = ["date": dateStr, "details_list": newTransactionArray] as [String : Any]
                                    weakSelf?.transactionDetailsArray.append(tempDetails)
                                }
                                else{
                                    let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                                    weakSelf?.transactionDetailsArray.append(tempDetails)

                                }
                            }
                            else{
                                let tempDetails = ["date": dateStr, "details_list": transactionArray] as [String : Any]
                                weakSelf?.transactionDetailsArray.append(tempDetails)
                            }
                        }
                        
                        if weakSelf!.transactionDetailsArray.count == 0{
//                            weakSelf!.lblNoRecordFound.isHidden = false
//                            weakSelf!.placeholderimgView.isHidden = false
                            weakSelf!.tblView.isHidden = true
                        }
                        else{
                            weakSelf!.tblView.isHidden = false
//                            weakSelf!.lblNoRecordFound.isHidden = true
//                            weakSelf!.placeholderimgView.isHidden = true
                        }
                        
                        DispatchQueue.main.async {
                            weakSelf!.tblView.reloadData()
                        }
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }

    }
}

extension TransactionsViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return transactionDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let transAction = transactionDetailsArray[section]
        let detailsArray = transAction["details_list"] as! Array<UserTransactionDetails>
        
        if isNeedToShowLoadMore && (section == transactionDetailsArray.count - 1) {
            return detailsArray.count + 1;
        }

        return detailsArray.count;
    }


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 5, width: tableView.bounds.size.width, height: 24))
        titleLabel.textColor = UIColor.appBlackColor()
        titleLabel.font = UIFont(name: "Arial", size: 15)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        let transAction = transactionDetailsArray[section]
        titleLabel.text = transAction["date"] as? String
        titleLabel.textAlignment = .center
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transAction = transactionDetailsArray[indexPath.section]
        let detailsArray = transAction["details_list"] as! Array<UserTransactionDetails>

        if  (indexPath.section == transactionDetailsArray.count - 1) && detailsArray.count == indexPath.row{

            var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell

            if cell == nil {
                cell = UITableViewCell(style: .value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            }
            cell?.selectionStyle = .none

            if isNeedToShowLoadMore{
                isNeedToShowLoadMore = false
                callGetUserTransactions(isShowLoader: false)
            }

            return cell!
        }
        else {
            var cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsTableViewCell") as? TransactionsTableViewCell
            
            if cell == nil {
                cell = TransactionsTableViewCell(style: .value1, reuseIdentifier: "TransactionsTableViewCell")
            }
            cell?.selectionStyle = .none
            let details = detailsArray[indexPath.row]
            cell!.configData(details: details)
            
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
//        let transAction = transactionDetailsArray[indexPath.section]
//        let detailsArray = transAction["details_list"] as! Array<TransactionsDetails>
//        let details = detailsArray[indexPath.row]
//
//        let transactionPopup = TransactionDetailsPopup(frame: (APPDELEGATE.window?.frame)!)
//        transactionPopup.transactionDetails = details
//        APPDELEGATE.window?.addSubview(transactionPopup)
//        transactionPopup.updateTransactionDetails()
//        transactionPopup.showAnimation();
    }
}
