//
//  TabBarViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 04/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class TabBarViewController: UIViewController {

    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var upcomingContainerView: UIView!
    @IBOutlet weak var myContestView: UIView!
    @IBOutlet weak var moreContainerView: UIView!
    @IBOutlet weak var profileContainerView: UIView!
   
    
    @IBOutlet weak var lblUpcomingMatches: UILabel!
    @IBOutlet weak var lblMyContest: UILabel!
    @IBOutlet weak var lblMyAccount: UILabel!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    
    @IBOutlet weak var imgViewUpcomingMatches: UIImageView!
    @IBOutlet weak var imgViewMyContest: UIImageView!
    @IBOutlet weak var imgViewMyAccount: UIImageView!
    @IBOutlet weak var imgViewMore: UIImageView!

    
    var selectedTab = SelectedTab.UpcomingMatches.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDetails.sharedInstance.startTimer()
        view.headerGradient()
        tabBarView.bottomTabShadow()
        containerView.topViewCorners()
        matchesButtonTapped(button: nil)
    }
    
    //MARK:- -IBAction Methods
    @IBAction func matchesButtonTapped(button: UIButton?)  {
        
        upcomingContainerView.isHidden = false
        myContestView.isHidden = true
        moreContainerView.isHidden = true
        profileContainerView.isHidden = true
        selectedTab = SelectedTab.UpcomingMatches.rawValue
        lblUpcomingMatches.textColor.selectedTabColor()
        lblMyContest.textColor.unselectedTabColor()
        lblMyAccount.textColor.unselectedTabColor()
        lblMore.textColor.unselectedTabColor()

        imgViewUpcomingMatches.image = UIImage.init(named: "MatchesSelectedIcon")
        imgViewMyContest.image = UIImage.init(named: "MyContestUnselected")
        imgViewMyAccount.image = UIImage.init(named: "MyAccountUnSelectedIcon")
        imgViewMore.image = UIImage.init(named: "MoreUnselectedIcon")
        lblTitle.text = "Matches"
        
        NotificationCenter.default.post(name: .upcompingTabTappedNotification, object: nil)
    }
    
    @IBAction func walletButtonTapped(_ sender: Any) {
        let walletCashVC = storyboard?.instantiateViewController(withIdentifier: "AddCashViewController") as! AddCashViewController
        navigationController?.pushViewController(walletCashVC, animated: true)
    }
    
    @IBAction func myContestButtonTapped(button: UIButton)  {
        upcomingContainerView.isHidden = true
        myContestView.isHidden = false
        moreContainerView.isHidden = true
        profileContainerView.isHidden = true
        selectedTab = SelectedTab.myContest.rawValue

        lblMyContest.textColor.selectedTabColor()
        lblUpcomingMatches.textColor.unselectedTabColor()
        lblMyAccount.textColor.unselectedTabColor()
        lblMore.textColor.unselectedTabColor()
        lblTitle.text = "My Contest"

        imgViewUpcomingMatches.image = UIImage.init(named: "MatchesUnSelectedIcon")
        imgViewMyContest.image = UIImage.init(named: "MyContestSelected")
        imgViewMyAccount.image = UIImage.init(named: "MyAccountUnSelectedIcon")
        imgViewMore.image = UIImage.init(named: "MoreUnselectedIcon")
        
        NotificationCenter.default.post(name: .myContestTabTappedNotification, object: nil)

    }
        
    @IBAction func myAccountButtonTapped(button: UIButton)  {
        
        upcomingContainerView.isHidden = true
        myContestView.isHidden = true
        moreContainerView.isHidden = true
        profileContainerView.isHidden = false

        selectedTab = SelectedTab.myAccount.rawValue

        lblMyAccount.textColor.selectedTabColor()
        lblUpcomingMatches.textColor.unselectedTabColor()
        lblMyContest.textColor.unselectedTabColor()
        lblMore.textColor.unselectedTabColor()
        lblTitle.text = "My Account"

        imgViewUpcomingMatches.image = UIImage.init(named: "MatchesUnSelectedIcon")
        imgViewMyContest.image = UIImage.init(named: "MyContestUnselected")
        imgViewMyAccount.image = UIImage.init(named: "MyAccountSelectedIcon")
        imgViewMore.image = UIImage.init(named: "MoreUnselectedIcon")
        
        NotificationCenter.default.post(name: .myAccountTabTappedNotification, object: nil)
    }
    
    
    @IBAction func moreButtonTapped(button: UIButton)  {
        
        upcomingContainerView.isHidden = true
        myContestView.isHidden = true
        moreContainerView.isHidden = false
        profileContainerView.isHidden = true
        selectedTab = SelectedTab.more.rawValue
        
        lblMore.textColor.selectedTabColor()
        lblUpcomingMatches.textColor.unselectedTabColor()
        lblMyContest.textColor.unselectedTabColor()
        lblMyAccount.textColor.unselectedTabColor()
        lblTitle.text = "More"

        imgViewUpcomingMatches.image = UIImage.init(named: "MatchesUnSelectedIcon")
        imgViewMyContest.image = UIImage.init(named: "MyContestUnselected")
        imgViewMyAccount.image = UIImage.init(named: "MyAccountUnSelectedIcon")
        imgViewMore.image = UIImage.init(named: "MoreSelectedIcon")
        
        NotificationCenter.default.post(name: .moreTabTappedNotification, object: nil)
    }
}
