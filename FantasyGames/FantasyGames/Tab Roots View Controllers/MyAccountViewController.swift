//
//  MyAccountViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import CropViewController

class MyAccountViewController: UIViewController, WithdrawWinningsViewControllerDelegates, UIImagePickerControllerDelegate,UINavigationControllerDelegate, CropViewControllerDelegate {
    
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var withdrawButton: UIButton!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var lblUnUsedAmount: UILabel!
    @IBOutlet weak var lblWiningAmount: UILabel!
    @IBOutlet weak var lblBonusAmount: UILabel!
    @IBOutlet weak var lblLeaguesWon: UILabel!
    @IBOutlet weak var lblLeaguesJoined: UILabel!
    @IBOutlet weak var lblMatchJoined: UILabel!
    @IBOutlet weak var lblSeriesJoined: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    var selectedImage: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.topViewCorners()
        walletView.cellShadow()
        withdrawButton.gradient();
        historyView.cellShadow()
        logoutView.cellShadow()
        NotificationCenter.default.addObserver(self, selector: #selector(myAccountTabTappedNotification(notification:)), name: .myAccountTabTappedNotification, object: nil)
        withdrawButton.setTitle("Withdraw", for: .normal)
        withdrawButton.isSelected = false
        withdrawButton.isHidden = true
        showUserDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UIScreen.main.bounds.height > 844 {
            scrollContentViewHeightConstraint.constant = UIScreen.main.bounds.height - 152
            view.layoutIfNeeded()
        }
        
        lblName.text = UserDetails.sharedInstance.firstName
        if UserDetails.sharedInstance.lastName.count > 0 {
            lblName.text = "\(UserDetails.sharedInstance.firstName) \(UserDetails.sharedInstance.lastName)"
        }
    }
     
    //MARK:- Action Methods
    @objc func myAccountTabTappedNotification(notification: Notification) {
        callGetProfileAPI(isShowLoader: true)
    }
    
    func showUserDetails() {
        
        lblPhoneNumber.text = "+91\(UserDetails.sharedInstance.phoneNumber)"
        lblName.text = UserDetails.sharedInstance.firstName + " " + UserDetails.sharedInstance.lastName
        lblTeamName.text = UserDetails.sharedInstance.userName
        lblUnUsedAmount.text = "₹\(UserDetails.sharedInstance.unusedAmount)"
        lblWiningAmount.text = "₹\(UserDetails.sharedInstance.totalWinningCash)"
        lblBonusAmount.text = "₹\(UserDetails.sharedInstance.bonusCash)"
        lblLeaguesWon.text = UserDetails.sharedInstance.totalContestWon
        lblLeaguesJoined.text = UserDetails.sharedInstance.totalContestJoined
        lblMatchJoined.text = UserDetails.sharedInstance.totalMatchJoined
        lblSeriesJoined.text = UserDetails.sharedInstance.totalSeriedJoined
        
       if UserDetails.sharedInstance.imageURL.count > 0 {
           if let url = URL(string: UserDetails.sharedInstance.imageURL){
               profileImgView.setImage(with: url, placeholder: UIImage(named: "ProfilePlaceholder"), progress: { received, total in
                   // Report progress
               }, completion: { [weak self] image in
                   if (image != nil){
                       self?.profileImgView.image = image
                   }
                   else{
                       self?.profileImgView.image = UIImage(named: "ProfilePlaceholder")
                   }
               })
           }
       }
       else{
           profileImgView.image = UIImage(named: "ProfilePlaceholder")
       }
        
    }

    @IBAction func withdrawButtonTapped(_ sender: Any) {
        if (UserDetails.sharedInstance.emailVerified == "1") && (UserDetails.sharedInstance.phoneVerified == "1") && (UserDetails.sharedInstance.bankVerified == "1") && (UserDetails.sharedInstance.panVerified == "1"){
            if withdrawButton.isSelected {
                cancelWithdrawRequestAPI()
            }
            else{
                let withdrawVC = storyboard?.instantiateViewController(withIdentifier: "WithdrawWinningsViewController") as? WithdrawWinningsViewController
                withdrawVC?.delegate = self
                navigationController?.pushViewController(withdrawVC!, animated: true)
            }
        }
        else{
            if withdrawButton.isSelected {
                cancelWithdrawRequestAPI()
            }
            else{
                let kycVerificationVC = storyboard?.instantiateViewController(withIdentifier: "KYCVerificationsViewController") as? KYCVerificationsViewController
                navigationController?.pushViewController(kycVerificationVC!, animated: true)
            }
        }
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        let editAccountVC = storyboard?.instantiateViewController(withIdentifier: "EditAccountViewController")
        navigationController?.pushViewController(editAccountVC!, animated: true)
    }
    
    @IBAction func updateProfilePictureButtonTapped(_ sender: Any) {
        
        weak var weakSelf = self
        let actionSheet: UIAlertController = UIAlertController(title: "Please choose an option", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheet.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            weakSelf?.cameraButtonTapped()
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            weakSelf?.galleryButtonTapped()
        }
        actionSheet.addAction(deleteActionButton)
        
        if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
            navigationVC.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: kAlert, message: kLogoutMsg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: kLogout, style: UIAlertAction.Style.default, handler: { action -> Void in
            AppHelpers.resetDefaults()
            AppHelpers.makePhoneAsRootViewController()
        }))
        alert.addAction(UIAlertAction(title: kCancel, style: UIAlertAction.Style.default, handler: nil))
        navigationController?.present(alert, animated: true, completion: nil)
    }
    
    func updateWithdrawRequestStatus() {
        withdrawButton.setTitle("Cancel Request", for: .normal)
        withdrawButton.isSelected = true
    }

    
    //MARK:- API Related Methods
    
    func callGetProfileAPI(isShowLoader: Bool) {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        if isShowLoader{
            AppHelpers.sharedInstance.displayLoader()
        }
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kUserProfile, andParameters: ["checkRequest": "1"], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                if status == "200"{
                    if result!["response"].dictionary != nil {
                        let isWithdrawRequestPending = result!["isWithdrawRequestPending"].boolValue
                        weakSelf?.withdrawButton.isHidden = false

                        if isWithdrawRequestPending {
                            weakSelf?.withdrawButton.setTitle("Cancel Request", for: .normal)
                            weakSelf?.withdrawButton.isSelected = true
                        }
                        else{
                            weakSelf?.withdrawButton.setTitle("Withdraw", for: .normal)
                            weakSelf?.withdrawButton.isSelected = false
                        }
                        
                        UserDetails.sharedInstance.parseUserDetails(response: result!["response"])
                        AppHelpers.saveUserDetails()
                        weakSelf?.showUserDetails()
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
    
    func cancelWithdrawRequestAPI() {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        AppHelpers.sharedInstance.displayLoader()

        weak var weakSelf = self
        
        WebServiceHandler.performGETRequest(withURL: kCancelWithdrawRequestURL) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                if status == "200"{
                    weakSelf?.withdrawButton.setTitle("Withdraw", for: .normal)
                    weakSelf?.withdrawButton.isSelected = false
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
        
        
//        WebServiceHandler.performPOSTRequest(urlString: kCancelWithdrawRequestURL, andParameters: nil, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
//            AppHelpers.sharedInstance.removeLoader()
//            
//            if result != nil{
//                let status = result!["code"].stringValue
//                let message = result!["message"].string
//                if status == "200"{
//                    weakSelf?.withdrawButton.setTitle("Withdraw", for: .normal)
//                    weakSelf?.withdrawButton.isSelected = false
//                }
//                else{
//                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
//                }
//            }
//            else{
//                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
//            }
//        }
    }
    
    func callUpdateProfilePicture() {
        if !AppHelpers.isInterNetConnectionAvailable() {
            return
        }
        
        guard let imageData = selectedImage!.jpegData(compressionQuality: 0.5) else { return }
        AppHelpers.sharedInstance.displayLoader()

        WebServiceHandler.performMultipartRequest(urlString: kUploadProfilePicURL, fileName: "image", params: [:], imageData: imageData, accessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
              if result != nil{
                  let status = result!["code"].stringValue
                  let message = result!["message"].string
                  if status == "200"{
                    AppHelpers.showCustomAlertView(message: message ?? "Profile picture has been uploaded successfully")
                  }
                  else{
                      AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                  }
              }
              else{
                  AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
              }
        }
    }
    
    
    //MARK:- Image Picker Related Methods
    
     func galleryButtonTapped() {
        
         let imagePicker = UIImagePickerController()
         imagePicker.delegate = self
         imagePicker.sourceType = .photoLibrary
         if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
             navigationVC.present(imagePicker, animated: true, completion: nil)
         }
     }
    
     func cameraButtonTapped() {
        
         let imagePicker = UIImagePickerController()
         imagePicker.delegate = self
         imagePicker.sourceType = .camera
         if let navigationVC = APPDELEGATE.window?.rootViewController as? UINavigationController{
             navigationVC.present(imagePicker, animated: true, completion: nil)
         }
     }
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         
         picker.dismiss(animated: true, completion: nil)
         selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
         presentCropViewController()
     }

     func presentCropViewController() {
        
         let cropViewController = CropViewController(croppingStyle: .circular, image: selectedImage!)
         cropViewController.delegate = self
         if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
             navVC.present(cropViewController, animated: true, completion: nil)
         }
     }
    
     func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
         selectedImage = image
         profileImgView.image = selectedImage
         if let navVC = APPDELEGATE.window?.rootViewController as? UINavigationController {
             navVC.dismiss(animated: true, completion: nil)
         }
        callUpdateProfilePicture()
    }
}
