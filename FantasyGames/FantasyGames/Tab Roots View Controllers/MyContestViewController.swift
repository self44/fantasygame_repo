//
//  MyContestViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class MyContestViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var cricketLiveMatchsArray = Array<MatchDetails>()
    lazy var cricketUpcomingMatchsArray = Array<MatchDetails>()
    lazy var cricketCompletedMatchsArray = Array<MatchDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "JoinedContestCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JoinedContestCollectionViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(myContestTabTappedNotification(notification:)), name: .myContestTabTappedNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(myContestDataRefreshNotification(notification:)), name: .myContestDataRefreshNotification, object: nil)

    }
    
    @objc func myContestTabTappedNotification(notification: Notification) {
        
        if (cricketLiveMatchsArray.count == 0) && (cricketUpcomingMatchsArray.count == 0) && (cricketCompletedMatchsArray.count == 0) {
            callGetMatchList(isShowLoader: true)
        }
        else{
            callGetMatchList(isShowLoader: false)
        }
    }
    
    @objc func myContestDataRefreshNotification(notification: Notification) {            
        callGetMatchList(isShowLoader: true)
    }

    
    //MARK:- API Related Methods
    func callGetMatchList(isShowLoader: Bool) {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        if isShowLoader{
            AppHelpers.sharedInstance.displayLoader()
        }
        
        let matchURL = kCricketJoinedAllMatched
        
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: matchURL) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    if let upcomingMatches = result!["upcomming"].array{
                        weakSelf?.cricketUpcomingMatchsArray = MatchDetails.getMatchDetails(dataArray: upcomingMatches)
                    }
                    
                    if let completedMatches = result!["completed"].array{
                        weakSelf?.cricketCompletedMatchsArray = MatchDetails.getMatchDetails(dataArray: completedMatches)
                    }

                    if let liveMatches = result!["live"].array{
                        weakSelf?.cricketLiveMatchsArray = MatchDetails.getMatchDetails(dataArray: liveMatches)
                    }
                    
                    weakSelf?.collectionView.reloadData()
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
}


extension MyContestViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JoinedContestCollectionViewCell", for: indexPath) as! JoinedContestCollectionViewCell
                    
        cell.configData(liveArray: cricketLiveMatchsArray, upcomingArray: cricketUpcomingMatchsArray, completedArray: cricketCompletedMatchsArray)
        
        return cell
        
    }
}
