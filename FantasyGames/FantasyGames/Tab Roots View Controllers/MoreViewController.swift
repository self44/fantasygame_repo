//
//  MoreViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    lazy var dataArray = Array<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataArray = ["KYC Verifications", "Transactions", "Promotions", "Refer & Earn", "Point System", "How to Play" ,"Privacy Policies", "Terms & Conditions","Withdrawal Policies" ,"About Us", "FAQ", "Contact Us", "Logout"]
        tblView.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreTableViewCell")
        
//        NotificationCenter.default.addObserver(self, selector: #selector(moreTabTappedNotification(notification:)), name: .moreTabTappedNotification, object: nil)
    }
    
//    @objc func moreTabTappedNotification(notification: Notification) {
//        
//    }
    
    //MARK: TableView Delegates & Data Sources
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as? MoreTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "MoreTableViewCell") as? MoreTableViewCell
        }
        
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        cell?.lblTitle.text = dataArray[indexPath.row]
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titleStr = dataArray[indexPath.row]
        if titleStr == "KYC Verifications" {
            let kycVerificationVC = storyboard?.instantiateViewController(withIdentifier: "KYCVerificationsViewController") as! KYCVerificationsViewController
            navigationController?.pushViewController(kycVerificationVC, animated: true);
        }
        else if titleStr == "Transactions" {
            let transactionsVC = storyboard?.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
            navigationController?.pushViewController(transactionsVC, animated: true);
        }
        else if titleStr == "Promotions" {
            let promoVC = storyboard?.instantiateViewController(withIdentifier: "PromotionsViewController") as! PromotionsViewController
            navigationController?.pushViewController(promoVC, animated: true);
        }
        else if titleStr == "Refer & Earn" {
            let referAndEarnVC = storyboard?.instantiateViewController(withIdentifier: "ReferAndEarnViewController") as! ReferAndEarnViewController
            navigationController?.pushViewController(referAndEarnVC, animated: true);
        }
        else if titleStr == "How to Play" {
            let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.webViewType = WebViewType.HowToPlay.rawValue
            navigationController?.pushViewController(webView, animated: true)
        }
        else if titleStr == "Point System" {
            let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.webViewType = WebViewType.PointSysytem.rawValue
            navigationController?.pushViewController(webView, animated: true)
        }
        else if titleStr == "Privacy Policies" {
            let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.webViewType = WebViewType.PrivacyPolices.rawValue
            navigationController?.pushViewController(webView, animated: true)
        }
        else if titleStr == "Terms & Conditions" {
            let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.webViewType = WebViewType.TermsAndConditions.rawValue
            navigationController?.pushViewController(webView, animated: true)
        }
        else if titleStr == "Withdrawal Policies" {
            let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.webViewType = WebViewType.WithdrawalPolicies.rawValue
            navigationController?.pushViewController(webView, animated: true)
        }
        else if titleStr == "About Us" {
            let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.webViewType = WebViewType.AboutUs.rawValue
            navigationController?.pushViewController(webView, animated: true)
        }
        else if titleStr == "FAQ" {
            let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webView.webViewType = WebViewType.FAQ.rawValue
            navigationController?.pushViewController(webView, animated: true)
        }
        else if titleStr == "Contact Us" {

        }
        else if titleStr == "Logout" {
            let alert = UIAlertController(title: kAlert, message: kLogoutMsg, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: kLogout, style: UIAlertAction.Style.default, handler: { action -> Void in
                AppHelpers.resetDefaults()
                AppHelpers.makePhoneAsRootViewController()
            }))
            alert.addAction(UIAlertAction(title: kCancel, style: UIAlertAction.Style.default, handler: nil))
            navigationController?.present(alert, animated: true, completion: nil)
        }
        
    }


}
