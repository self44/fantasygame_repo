//
//  UpcomingMatchesViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class UpcomingMatchesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var upcomingMatchesArray = Array<MatchDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "isUserLogin")
        UserDefaults.standard.synchronize()
        
        collectionView.register(UINib(nibName: "UpcomingMatchesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UpcomingMatchesCollectionViewCell")
        view.appbackgroudColor()
        
        NotificationCenter.default.addObserver(self, selector: #selector(upcompingTabTappedNotification(notification:)), name: .upcompingTabTappedNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(upcomingDataRefreshNotification(notification:)), name: .upcomingDataRefreshNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeClosedMatch(notification:)), name: .removeClosedMatch, object: nil)
        
    }
    
    @objc func upcompingTabTappedNotification(notification: Notification) {
        updateUpcomingMatchList()
    }
    
    @objc func removeClosedMatch(notification: Notification) {
        if let matchID = notification.object as? String {
            
            var isMatchRemoved = false
            
            upcomingMatchesArray.removeAll { (matchDetails) -> Bool in
                isMatchRemoved = true
                return matchDetails.matchID == matchID
            }
            
            if isMatchRemoved {
                collectionView.reloadData()
//                callGetMatchList(isShowLoader: false)
            }
            
//            for (index, details) in upcomingMatchesArray.enumerated() {
//                if details.matchID == matchID{
//                    upcomingMatchesArray.removeAll { (matchDetails) -> Bool in
//                        matchDetails.matchID = matchID
//                    }
//                    upcomingMatchesArray.remove(at: index)
//                    collectionView.reloadData()
//                    callGetMatchList(isShowLoader: false)
//                    break
//                }
//            }
        }
    }
    
    @objc func upcomingDataRefreshNotification(notification: Notification) {
        callGetMatchList(isShowLoader: true)
    }
    
    
    func updateUpcomingMatchList() {
                    
        let savedResponse = CoreDataFileHandler.getDetailsFromCoreData(urlString: kMatchURL)
        if (savedResponse != nil) {
            let servertime = savedResponse!["serverTimestamp"].stringValue
            if servertime.count > 0 {
                UserDetails.sharedInstance.serverTimeStemp = servertime
            }
            upcomingMatchesArray = MatchDetails.getAllUpcomingMatchList(JSONDict: savedResponse!)
        }
        
        if upcomingMatchesArray.count == 0 {
            callGetMatchList(isShowLoader: true)
        }
        else{
            callGetMatchList(isShowLoader: false)
            collectionView.reloadData()
        }
    }

    
    //MARK:- API Related Methods
    func callGetMatchList(isShowLoader: Bool) {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        if isShowLoader{
            AppHelpers.sharedInstance.displayLoader()
        }
        
        let matchURL = kMatchURL
        
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: matchURL) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    weakSelf?.upcomingMatchesArray = MatchDetails.getAllUpcomingMatchList(JSONDict: result!)
                    weakSelf?.collectionView.reloadData()
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
}

extension UpcomingMatchesViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpcomingMatchesCollectionViewCell", for: indexPath) as! UpcomingMatchesCollectionViewCell
        cell.configData(upcomingMatchArray: upcomingMatchesArray)
        return cell
    }
    
}
