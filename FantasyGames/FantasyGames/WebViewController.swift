//
//  WebViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 01/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    lazy var webViewType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        webview.backgroundColor = .clear
        webview.isOpaque = false

        if webViewType == WebViewType.PointSysytem.rawValue {
            backButton.setTitle("Point System", for: .normal)
            getHtmlContent(type: "1")
        }
        else if webViewType == WebViewType.HowToPlay.rawValue {
            backButton.setTitle("How to Play", for: .normal)
        }
        else if webViewType == WebViewType.PrivacyPolices.rawValue {
            backButton.setTitle("Privacy Policies", for: .normal);
            getHtmlContent(type: "2")
        }
        else if webViewType == WebViewType.TermsAndConditions.rawValue {
            backButton.setTitle("Terms & Conditions", for: .normal);
            getHtmlContent(type: "3")
        }
        else if webViewType == WebViewType.WithdrawalPolicies.rawValue {
            backButton.setTitle("Withdrawal Policies", for: .normal);
            getHtmlContent(type: "4")
        }
        else if webViewType == WebViewType.AboutUs.rawValue {
            backButton.setTitle("About Us", for: .normal);
            getHtmlContent(type: "5")
        }
        else if webViewType == WebViewType.FAQ.rawValue {
            backButton.setTitle("FAQ", for: .normal);
            getHtmlContent(type: "6")
        }
    }
    
    func getHtmlContent(type: String) {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        AppHelpers.sharedInstance.displayLoader()

        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kGetHtmlPagesURL, andParameters: ["type": type], andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                if status == "200"{
                    if let webcontent = result!["webcontent"].dictionary {
                        let htmlString = webcontent["htmlString"]?.string ?? ""
                        weakSelf?.webview.loadHTMLString(htmlString, baseURL: nil)
                    }
                    AppHelpers.sharedInstance.removeLoader()
                }
                else{
                    AppHelpers.sharedInstance.removeLoader()
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.sharedInstance.removeLoader()
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppHelpers.sharedInstance.removeLoader()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppHelpers.sharedInstance.removeLoader()
    }

}
