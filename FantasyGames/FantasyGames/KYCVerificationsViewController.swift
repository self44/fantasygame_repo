
//
//  KYCVerificationsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 01/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class KYCVerificationsViewController: UIViewController {

    @IBOutlet weak var panButton: UIButton!
    @IBOutlet weak var bankButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        containerView.topViewCorners()
        tabView.customTabBottomShadow()
        showEmailDetails()
        callGetKycDetails()
        collectionView.register(UINib(nibName: "VerifyEmailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VerifyEmailCollectionViewCell")
        collectionView.register(UINib(nibName: "VerifyPanDetailsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VerifyPanDetailsCollectionViewCell")
        collectionView.register(UINib(nibName: "VerifyBankCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VerifyBankCollectionViewCell")
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func emailButtonTapped(_ sender: Any) {
        showEmailDetails()
        
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
   
    @IBAction func panButtonTapped(_ sender: Any) {
        showPanDetails()
        
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func bankButtonTapped(_ sender: Any) {
        showBankDetails()
        
        let indexPath = IndexPath(item: 2, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func showEmailDetails()  {
        emailButton.tabGradient()
        panButton.removeTabGradient()
        bankButton.removeTabGradient()

        emailButton.setTitleColor(UIColor.white, for: .normal)
        panButton.setTitleColor(UIColor.selectedTabColor(), for: .normal)
        bankButton.setTitleColor(UIColor.selectedTabColor(), for: .normal)
    }
    
    func showPanDetails()  {
        
        emailButton.removeTabGradient()
        panButton.tabGradient()
        bankButton.removeTabGradient()
        
        emailButton.setTitleColor(UIColor.selectedTabColor(), for: .normal)
        panButton.setTitleColor(UIColor.white, for: .normal)
        bankButton.setTitleColor(UIColor.selectedTabColor(), for: .normal)
    }
    
    func showBankDetails()  {

        emailButton.removeTabGradient()
        panButton.removeTabGradient()
        bankButton.tabGradient()
        
        emailButton.setTitleColor(UIColor.selectedTabColor(), for: .normal)
        panButton.setTitleColor(UIColor.selectedTabColor(), for: .normal)
        bankButton.setTitleColor(UIColor.white, for: .normal)
    }
    //MARK:- API Related method
    func callGetKycDetails() {
        if !AppHelpers.isInterNetConnectionAvailable() {
            return
        }
        
        AppHelpers.sharedInstance.displayLoader()
        weak var weakSelf = self
        
        WebServiceHandler.performGETRequest(withURL: kGetKYCdetailsURL ) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            if result != nil{
                let status = result!["code"].stringValue
                if status == "200"{
                    if let kycDetails = result!["kycDetails"].dictionary{

                    }
                    
                    if let userDetails = result!["userDetails"].dictionary{

                    }
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
}

extension KYCVerificationsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = CGSize(width: UIScreen.main.bounds.width, height: collectionView.bounds.height)
        return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VerifyEmailCollectionViewCell", for: indexPath) as! VerifyEmailCollectionViewCell

            return cell
        }
        else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VerifyPanDetailsCollectionViewCell", for: indexPath) as! VerifyPanDetailsCollectionViewCell

            cell.configData()
            return cell
        }
        else if indexPath.row == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VerifyBankCollectionViewCell", for: indexPath) as! VerifyBankCollectionViewCell
            cell.configData()

            return cell
        }

        return UICollectionViewCell()

    }
    
}
