//
//  LeagueWinnersListViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 06/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class LeagueWinnersListViewController: UIViewController {

    @IBOutlet weak var collectionContainerView: UIView!
    @IBOutlet weak var lblWinningAmount: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    var leagueDetails: LeagueDetails?

    lazy var winerRanksArray = Array<WinnerRanksDetails>()
    lazy var matchType = MatchType.Cricket.rawValue
        
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        if leagueDetails != nil {
            lblWinningAmount.text = "₹" + leagueDetails!.pricePool
        }
        else{
            lblWinningAmount.text = "₹0"
        }
        containerView.topViewCorners()
        callGetMatchWinners()
        collectionView.register(UINib(nibName: "LeagueWinnersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeagueWinnersCollectionViewCell")
        
    }
        
    override func viewWillAppear(_ animated: Bool) {
    
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- API Related Methods
    func callGetMatchWinners()  {
        guard let details = leagueDetails else { return}

        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }
        AppHelpers.sharedInstance.displayLoader()

        var urlString = ""
        if matchType == MatchType.Cricket.rawValue {
            urlString = kLeagueWiningsURL
        }
        
        weak var weakSelf = self
        let params = ["templateId": details.templateId] as [String : Any]
        WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()

            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? kErrorMsg

                if statusCode == "200" {
                    if let winnersArray = result!["winners"].array  {
                        if winnersArray.count > 0 {
                            weakSelf?.winerRanksArray = WinnerRanksDetails.getAllWinningRankDetails(dataArray: winnersArray)
                        }
                        else{
                            let winnerDetails = WinnerRanksDetails()
                            winnerDetails.rankPrice = details.winPerUser
                            winnerDetails.rank = "#1"
                            weakSelf?.winerRanksArray = [winnerDetails]
                        }
                        weakSelf?.collectionView.reloadData()
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
}



extension LeagueWinnersListViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return winerRanksArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let reminder = winerRanksArray.count % 2
        
        if (reminder == 1) && (indexPath.row == winerRanksArray.count - 1) {
            return CGSize(width: collectionView.frame.width, height: 60)
        }
        return CGSize(width: collectionView.frame.width/2 - 0.5, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueWinnersCollectionViewCell", for: indexPath) as? LeagueWinnersCollectionViewCell
        let details = winerRanksArray[indexPath.item]
        
        cell?.lblAmount.text = "₹\(details.rankPrice)"
        cell?.lblRankUpTo.text = details.rank
        return cell!;
    }
    
}

