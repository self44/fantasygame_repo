//
//  UpdateUserNameViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class UpdateUserNameViewController: UIViewController {
    
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var referalCodeView: UIView!
    @IBOutlet weak var txtFieldReferral: UITextField!
    @IBOutlet weak var txtFieldUserName: UITextField!
    @IBOutlet weak var letsPlayButton: UIButton!
    
    lazy var userName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtFieldUserName.text = userName
        
        usernameView.layer.cornerRadius = 5
        usernameView.layer.borderColor = UIColor.appbackgroudColor().cgColor
        usernameView.layer.borderWidth = 1.0
        
        referalCodeView.layer.cornerRadius = 5
        referalCodeView.layer.borderColor = UIColor.appbackgroudColor().cgColor
        referalCodeView.layer.borderWidth = 1.0
    }
    
    override func viewDidLayoutSubviews() {
        letsPlayButton.removeTabGradient()
        letsPlayButton.gradient()
    }
    
    //MARK:- API Related Methods
    func callUpdateUserNameAPI() {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        AppHelpers.sharedInstance.displayLoader()
        weak var weakSelf = self
        let params = ["username": txtFieldUserName.text!, "referralCode": txtFieldReferral.text ?? ""] as [String : Any]
        
        WebServiceHandler.performPOSTRequest(urlString: kEditURL, andParameters: params as Parameters, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    if result!["user"].dictionary != nil{
                        weakSelf?.parseUserDetails(details: result!["user"])
                    }
                    AppHelpers.makeTabBarAsRootViewController()
                }
                else{
                    AppHelpers.showCustomAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
    
    func parseUserDetails(details: JSON) {
        
        UserDetails.sharedInstance.userName = details["username"].string ?? ""
        UserDetails.sharedInstance.lastName = details["lastName"].string ?? ""
        UserDetails.sharedInstance.firstName = details["firstName"].string ?? ""

        UserDetails.sharedInstance.emailAdress = details["email"].string ?? ""
        UserDetails.sharedInstance.phoneNumber = details["phone"].string ?? ""
        UserDetails.sharedInstance.imageURL = details["image"].string ?? ""
        UserDetails.sharedInstance.gender = details["gender"].string ?? ""
        UserDetails.sharedInstance.dateOfBirth = details["dob"].string ?? ""
        
        AppHelpers.saveUserDetails()
    }
    
    @IBAction func confirmButtonTapped(_ sender: Any) {
        if txtFieldUserName.isEmpty() {
            AppHelpers.showCustomAlertView(message: UpdateUserName)
            return;
        }
        callUpdateUserNameAPI()
    }

}
