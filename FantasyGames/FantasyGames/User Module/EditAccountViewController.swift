//
//  EditAccountViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 24/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class EditAccountViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var updateProfile: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var txtFieldDob: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldTeamName: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!

    let datePicker = UIDatePicker()
    lazy var selectedDob = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        bottomView.bottomTabShadow()
        containerView.topViewCorners()
        updateProfile.gradient();
        txtFieldDob.inputView = datePicker
        
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.datePickerMode = .date
        datePicker.maximumDate = date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        navigationController?.navigationBar.isHidden = true
        updateUserDetails()
    }

    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        selectedDob = dateFormatter.string(from: sender.date)
        txtFieldDob.text = AppHelpers.getFormatedDateWithoutTime(dateString: selectedDob)
    }
    
    func updateUserDetails()  {
        
        txtFieldFirstName.text = UserDetails.sharedInstance.firstName
        txtFieldLastName.text = UserDetails.sharedInstance.lastName
        selectedDob = UserDetails.sharedInstance.dateOfBirth
        txtFieldDob.text = AppHelpers.getFormatedDateWithoutTime(dateString: selectedDob)
        txtFieldEmail.text = UserDetails.sharedInstance.emailAdress
        txtFieldPhoneNumber.text = UserDetails.sharedInstance.phoneNumber
        txtFieldTeamName.text = UserDetails.sharedInstance.userName
        if UserDetails.sharedInstance.phoneNumber.count == 0{
            txtFieldPhoneNumber.isUserInteractionEnabled = true
        }
        else{
            txtFieldPhoneNumber.isUserInteractionEnabled = false
        }
        
        if UserDetails.sharedInstance.firstName.count == 0{
            txtFieldFirstName.isUserInteractionEnabled = true
        }
        else{
            txtFieldFirstName.isUserInteractionEnabled = false
        }
        
        if UserDetails.sharedInstance.lastName.count == 0{
            txtFieldLastName.isUserInteractionEnabled = true
        }
        else{
            txtFieldLastName.isUserInteractionEnabled = false
        }

        if UserDetails.sharedInstance.emailAdress.count > 0{
            if UserDetails.sharedInstance.isEmailVerified{
                txtFieldEmail.isUserInteractionEnabled = false
            }
        }
        
        txtFieldTeamName.isUserInteractionEnabled = false
        txtFieldPhoneNumber.isUserInteractionEnabled = false

        if UserDetails.sharedInstance.gender == "M"{
            maleButton.isSelected = true
            femaleButton.isSelected = false
            otherButton.isSelected = false
        }
        else if UserDetails.sharedInstance.gender == "F"{
            maleButton.isSelected = false
            femaleButton.isSelected = true
            otherButton.isSelected = false
        }
        else if UserDetails.sharedInstance.gender == "O"{
            maleButton.isSelected = false
            femaleButton.isSelected = false
            otherButton.isSelected = true
        }
        
//        if (UserDetails.sharedInstance.panVerified == "1") || (UserDetails.sharedInstance.panVerified == "2"){
//            txtFieldFirstName.isUserInteractionEnabled = false
//            txtFieldDob.isUserInteractionEnabled = false
//            txtFieldLastName.isUserInteractionEnabled = false
//
//        }
        
    }
    
    @IBAction func maleButtonTapped(_ sender: Any) {
        maleButton.isSelected = true
        femaleButton.isSelected = false
        otherButton.isSelected = false
    }
    
    @IBAction func femaleButtonTapped(_ sender: Any) {
        maleButton.isSelected = false
        femaleButton.isSelected = true
        otherButton.isSelected = false
    }
    
    @IBAction func otherButtonTapped(_ sender: Any) {
        maleButton.isSelected = false
        femaleButton.isSelected = false
        otherButton.isSelected = true
    }

    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if txtFieldFirstName.text?.count == 0 {
            AppHelpers.showCustomErrorAlertView(message: EnterFirstName)
            return;
        }
        else if txtFieldDob.text?.count == 0 {
            AppHelpers.showCustomErrorAlertView(message: SelectDateOfBirth)
            return;
        }
        else if txtFieldEmail.text?.count == 0 {
            AppHelpers.showCustomErrorAlertView(message: EnterEmail)
            return;
        }
        else if AppHelpers.isNotValidEmail(email: txtFieldEmail.text!){
            AppHelpers.showCustomErrorAlertView(message: EnterValidEmail)
            return
        }
        
        callUpdateUserProfileAPI()
    }
    
    
    func callUpdateUserProfileAPI()  {
        
        if !AppHelpers.isInterNetConnectionAvailable(){
            return;
        }
        
        AppHelpers.sharedInstance.displayLoader()
        
        var gender = "M"
        
        if maleButton.isSelected{
            gender = "M"
        }
        else if femaleButton.isSelected{
            gender = "F"
        }
        else{
            gender = "O"
        }
        
        let params = ["email": txtFieldEmail.text!,
                      "firstName": txtFieldFirstName.text!,
                      "lastNname": txtFieldLastName.text!,
                      "dob": selectedDob,
                      "gender": gender] as [String : Any]
        
        weak var weakSelf = self
        WebServiceHandler.performPOSTRequest(urlString: kUpdateProfileURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let statusCode = result!["code"].stringValue
                let message = result!["message"].string ?? ""
                
                if statusCode == "200"{
                    
                    UserDetails.sharedInstance.dateOfBirth = weakSelf?.selectedDob ?? ""
                    UserDetails.sharedInstance.firstName = weakSelf?.txtFieldFirstName.text ?? ""
                    UserDetails.sharedInstance.lastName = weakSelf?.txtFieldLastName.text ?? ""
                    UserDetails.sharedInstance.emailAdress = weakSelf?.txtFieldEmail.text ?? ""
                    UserDetails.sharedInstance.gender = gender

                    
                    AppHelpers.saveUserDetails()
                    
                    let alert = UIAlertController(title: kAlert, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action -> Void in
                        weakSelf!.navigationController?.popViewController(animated: true)
                    }))
                    weakSelf?.present(alert, animated: true, completion: nil)
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
