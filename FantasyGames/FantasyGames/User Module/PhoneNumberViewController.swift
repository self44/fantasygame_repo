//
//  PhoneNumberViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class PhoneNumberViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var txtFieldContainerView: UIView!

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.headerGradient()
//        nextButton.tabGradient()
        txtFieldContainerView.layer.cornerRadius = 5
        txtFieldContainerView.layer.borderColor = UIColor.appbackgroudColor().cgColor
        txtFieldContainerView.layer.borderWidth = 1.0
        nextButton.backgroundColor = UIColor.appThemeColorColor()
        nextButton.showShadowOnCirleButton()
    }
    
    //MARK:- -IBAction Methods
    @IBAction func nextButtonTapped(_ sender: Any) {

        if txtFieldPhoneNumber.isEmpty() {
            AppHelpers.showCustomErrorAlertView(message: EnterPhoneNumber)
            return
        }
        else if txtFieldPhoneNumber.textCount() < 10{
            AppHelpers.showCustomErrorAlertView(message: MinPhoneNumber)
            return;
        }
        updatePhoneOnServier()
    }
    
    //MARK:- API Related Methods
    func updatePhoneOnServier() {
        if UserDetails.sharedInstance.accessToken.count == 0 {
            AppHelpers.getGeustToken()
            return
        }
        txtFieldPhoneNumber.resignFirstResponder()
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        AppHelpers.sharedInstance.displayLoader()
        let params = ["phone":txtFieldPhoneNumber.text ?? "", "verify":false, "otp":"", "deviceToken": APPDELEGATE.deviceToken] as [String : Any]
        
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kLoginURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()

            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    let otp = result!["otp"].stringValue
                    AppHelpers.showCustomAlertView(message: otp)

                    UserDetails.sharedInstance.phoneNumber = weakSelf?.txtFieldPhoneNumber.text ?? ""
                    let verifyOtpVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
                    weakSelf?.navigationController?.pushViewController(verifyOtpVC, animated: true)
                }
                else{
                    AppHelpers.showCustomAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if string == "" {
            return true;
        }
        if string == " " {
            return false
        }
        
        let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
        if (textField.text?.count)! < 10 {
            return isNumber
        }
        
        return false
    }
    
}
