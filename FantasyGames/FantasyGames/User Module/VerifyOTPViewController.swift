//
//  VerifyOTPViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 02/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VerifyOTPViewController: UIViewController, UITextFieldDelegate, CustomTextFieldDelegates {
    
    @IBOutlet weak var lblExpireOtp: UILabel!
    
    @IBOutlet weak var lblMobileNumberMessage: UILabel!
    @IBOutlet weak var lblResndMesssage: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet var textFieldsArray: [CustomTextField]!
    @IBOutlet weak var nextButton: UIButton!

    lazy var otpStr = ""
    lazy var remaningSecs = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let txtField = textFieldsArray[0]
        txtField.becomeFirstResponder()
        resendButton.isHidden = true;
        lblResndMesssage.isHidden = true;
        lblMobileNumberMessage.text = SentVerificationMessage + UserDetails.sharedInstance.phoneNumber
        
        nextButton.backgroundColor = UIColor.appThemeColorColor()
        nextButton.showShadowOnCirleButton()

        weak var weakSelf = self
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){_ in
            weakSelf?.showResendButton()
        }
    }
    
    @objc func showResendButton() {
        remaningSecs = remaningSecs - 1
        if remaningSecs <= 0 {
            resendButton.isHidden = false;
            lblResndMesssage.isHidden = false;
            lblExpireOtp.isHidden = true
        }
        else{
            resendButton.isHidden = true;
            lblResndMesssage.isHidden = true;
            lblExpireOtp.isHidden = false
        }
        
        lblExpireOtp.text = "The OTP will be expire in next \(remaningSecs) seconds"
    }
    
    // MARK:- IBAction Methods
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        otpStr = ""
        for txtField in textFieldsArray {
            if txtField.text != ""{
                otpStr = otpStr + txtField.text!
            }
            else{
//                AppHelpers.showCustomAlertView(message: EnterOTP)
//                return;
            }
        }
        
        let params = ["phone":UserDetails.sharedInstance.phoneNumber, "verify":true, "otp":otpStr] as [String : Any]
        callVerifyOtp(params: params)
    }
    
    @IBAction func changePhoneNumberButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOtpButtonTapped(_ sender: Any) {
        callResendOTPAPI()
    }
    
    //MARK:- API Related Methods
    func callResendOTPAPI() {

        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        AppHelpers.sharedInstance.displayLoader()
        let params = ["phone": UserDetails.sharedInstance.phoneNumber, "verify":false, "otp":""] as [String : Any]
        
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kLoginURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()

            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    weakSelf?.lblExpireOtp.text = "The OTP will be expire in next 60 seconds"
                    weakSelf?.remaningSecs = 60
                }
                else{
                    AppHelpers.showCustomAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
    //MARK:- API Related Methods
    func callVerifyOtp(params: [String : Any]) {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        AppHelpers.sharedInstance.displayLoader()        
        weak var weakSelf = self
        
        WebServiceHandler.performPOSTRequest(urlString: kLoginURL, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    var usernameEdited = "0"
                    var userName = ""
                    
                    UserDetails.sharedInstance.accessToken = result!["accessTokoen"].string ?? ""
                    if let userDetails = result!["user"].dictionary{
                        usernameEdited = userDetails["usernameEdited"]?.stringValue ?? "0"
                        userName = userDetails["username"]?.string ?? ""
                        if usernameEdited != "0"{
                            weakSelf?.parseUserDetails(details: result!["user"])
                        }
                    }
                    if usernameEdited == "0"{
                        let userNameVC = weakSelf?.storyboard?.instantiateViewController(withIdentifier: "UpdateUserNameViewController") as! UpdateUserNameViewController
                        userNameVC.userName = userName; weakSelf?.navigationController?.pushViewController(userNameVC, animated: true)
                    }
                    else{
                        AppHelpers.makeTabBarAsRootViewController()
                    }
                }
                else{
                    AppHelpers.showCustomAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: kErrorMsg)
            }
        }
    }
    
    func parseUserDetails(details: JSON) {
        
        UserDetails.sharedInstance.userName = details["username"].string ?? ""
        UserDetails.sharedInstance.firstName = details["firstName"].string ?? ""
        UserDetails.sharedInstance.lastName = details["lastName"].string ?? ""

        UserDetails.sharedInstance.emailAdress = details["email"].string ?? ""
        UserDetails.sharedInstance.phoneNumber = details["phone"].string ?? ""
        UserDetails.sharedInstance.imageURL = details["image"].string ?? ""
        UserDetails.sharedInstance.gender = details["gender"].string ?? ""
        UserDetails.sharedInstance.dateOfBirth = details["dob"].string ?? ""
        
        AppHelpers.saveUserDetails()

    }
    
    
    // MARK:- Text Filed Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if string.count != 0 {
            if string == " " {
                return false
            }

            let lenght = range.location
            if (string.count == 1) && (lenght == 0) {
                fillTheOtp(textField: textField, string: string)
            }
            else{
                if lenght < textFieldsArray.count{
                   textFieldsArray[lenght].text = string
                }
            }
        }
        if string == UIPasteboard.general.string{
            print("Print")
        }

        return true
    }

    func textField(_ textField: UITextField, didDeleteBackwardAnd wasEmpty: Bool) {
        if wasEmpty {

            let index = textField.tag - 11
            if index >= 0{
                textFieldsArray[index].becomeFirstResponder()
            }
        }
    }

    func autofillTheOtp(clipboardString: String)  {
        if clipboardString.count > 6 {
            return
        }
        
        for index in 0..<clipboardString.count {
            textFieldsArray[index].text = clipboardString[index]
        }
    }
    
    func fillTheOtp(textField: UITextField, string: String)  {
        weak var weakSelf = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
            
            if textField == weakSelf!.textFieldsArray[0]{
                if string == ""{
                    
                }else{
                    if (weakSelf!.textFieldsArray[1].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[1].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[0].text = string
                        weakSelf!.textFieldsArray[0].resignFirstResponder()
                    }
                }
            }else if textField == weakSelf!.textFieldsArray[1]{
                if string == ""{
                    weakSelf!.textFieldsArray[0].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[2].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[2].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[1].text = string
                        weakSelf!.textFieldsArray[1].resignFirstResponder()
                    }
                }
            }else if textField == weakSelf!.textFieldsArray[2]{
                if string == ""{
                    weakSelf!.textFieldsArray[1].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[3].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[3].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[2].text = string
                        weakSelf!.textFieldsArray[2].resignFirstResponder()
                    }
                }
                
            }else if textField == weakSelf!.textFieldsArray[3]{
                if string == ""{
                    weakSelf!.textFieldsArray[2].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[4].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[4].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[3].text = string
                        weakSelf!.textFieldsArray[3].resignFirstResponder()
                    }
                }
                
            }else if textField == weakSelf!.textFieldsArray[4]{
                if string == ""{
                    weakSelf!.textFieldsArray[3].becomeFirstResponder()
                    
                }else{
                    if (weakSelf!.textFieldsArray[5].text?.isEmpty)!{
                        textField.text = string
                        weakSelf!.textFieldsArray[5].becomeFirstResponder()
                    }
                    else{
                        weakSelf!.textFieldsArray[4].text = string
                        weakSelf!.textFieldsArray[4].resignFirstResponder()
                    }
                }
                
            }else if textField == weakSelf!.textFieldsArray[5]{
                if string == ""{
                    weakSelf!.textFieldsArray[4].becomeFirstResponder()
                }else{
                    textField.text = string
                    textField.resignFirstResponder()
                }
            }
        }
    }
    

}


extension String {
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
}

