//
//  TeamDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 13/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class TeamDetails: NSObject {

    lazy var teamId = ""
//    lazy var userTeamId = ""
//    lazy var matchId = ""
    lazy var teamNumber = ""
    lazy var fantasyType = ""
    lazy var captainName = ""
    lazy var viceCaptainName = ""
    lazy var gender = ""
    lazy var captainImageName = ""
    lazy var viceCaptainImageName = ""
    lazy var team1Name = ""
    lazy var team2Name = ""

    lazy var totalWicketKeeprCount = "0"
    lazy var totalBatsmanCount = "0"
    lazy var totalAllRounderCount = "0"
    lazy var totalBowlerCount = "0"
    lazy var team1PlayerCount = "0"
    lazy var team2PlayerCount = "0"

    
    
    lazy var playersArray = Array<PlayerDetails>()

    class func parseTeamDetails(details: JSON) -> TeamDetails {
        
        let teamDetails = TeamDetails()

        teamDetails.teamId = details["_id"].string ?? ""
//        teamDetails.userTeamId = details["userTeamId"].stringValue
//        teamDetails.matchId = details["matchId"].stringValue
        teamDetails.teamNumber = details["teamNumber"].stringValue
        teamDetails.fantasyType = details["fantasyType"].stringValue
        
        if details["players"].dictionary != nil {
            
            if let playerInfo = TeamDetails.getPlayerDetails(key: "p1", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }

                teamDetails.playersArray.append(playerInfo)
            }

            if let playerInfo = TeamDetails.getPlayerDetails(key: "p2", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                
                teamDetails.playersArray.append(playerInfo)
            }

            if let playerInfo = TeamDetails.getPlayerDetails(key: "p3", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }

                teamDetails.playersArray.append(playerInfo)
            }
            
            if let playerInfo = TeamDetails.getPlayerDetails(key: "p4", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }

                teamDetails.playersArray.append(playerInfo)
            }
            
            if let playerInfo = TeamDetails.getPlayerDetails(key: "p5", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                teamDetails.playersArray.append(playerInfo)
            }
            
            if let playerInfo = TeamDetails.getPlayerDetails(key: "p6", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }

                teamDetails.playersArray.append(playerInfo)
            }

            if let playerInfo = TeamDetails.getPlayerDetails(key: "p7", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }

                teamDetails.playersArray.append(playerInfo)
            }

            if let playerInfo = TeamDetails.getPlayerDetails(key: "p8", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }

                teamDetails.playersArray.append(playerInfo)
            }
            
            if let playerInfo = TeamDetails.getPlayerDetails(key: "p9", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }

                teamDetails.playersArray.append(playerInfo)
            }
            
            if let playerInfo = TeamDetails.getPlayerDetails(key: "p10", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                   
                teamDetails.playersArray.append(playerInfo)
            }
            
            if let playerInfo = TeamDetails.getPlayerDetails(key: "p11", playersDetails: details["players"])  {
                if playerInfo.isCaption {
                    teamDetails.captainName = playerInfo.playerShortName
                    teamDetails.captainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                else if playerInfo.isViceCaption {
                    teamDetails.viceCaptainName = playerInfo.playerShortName
                    teamDetails.viceCaptainImageName = playerInfo.playerImage
                    teamDetails.gender = playerInfo.gender
                }
                   
                teamDetails.playersArray.append(playerInfo)
            }
        }
        
        return teamDetails
    }
    
    class func getPlayerDetails(key: String, playersDetails: JSON) -> PlayerDetails? {
        
        if let player1Details = playersDetails[key].dictionary {
            if player1Details["player"]?.dictionary != nil{
                
                let playerInfo = PlayerDetails.parsePlayerDetails(details: player1Details["player"]!)
                let playerRole = player1Details["playerRole"]?.stringValue
                if playerRole == "1" {
                    playerInfo.isCaption = true
                }
                else if playerRole == "2" {
                    playerInfo.isViceCaption = true
                }
                return playerInfo
            }
        }
        
        return nil
    }
    
    class func getAllUserTeamsForCricket(dataArray: Array<JSON>, matchDetails: MatchDetails) -> Array<TeamDetails> {
        var teamsArray = Array<TeamDetails>()
        
        for details in dataArray {
            let teamDetails = TeamDetails.parseTeamDetails(details: details)
            
            let wicketKeepersArray = teamDetails.playersArray.filter { (playerDetails) -> Bool in
                return playerDetails.playerRole == PlayerRole.WicketKeeper.rawValue
            }
            
            let batsmanArray = teamDetails.playersArray.filter { (playerDetails) -> Bool in
                return playerDetails.playerRole == PlayerRole.Batsman.rawValue
            }
            
            let allRounderArray = teamDetails.playersArray.filter { (playerDetails) -> Bool in
                return playerDetails.playerRole == PlayerRole.AllRounder.rawValue
            }
            
            let bowlersArray = teamDetails.playersArray.filter { (playerDetails) -> Bool in
                return playerDetails.playerRole == PlayerRole.Bowler.rawValue
            }
            
            let firstTeamCount = teamDetails.playersArray.filter { (playerDetails) -> Bool in
                return playerDetails.teamID == matchDetails.team1ID
            }
            
            let secondTeamCount = teamDetails.playersArray.filter { (playerDetails) -> Bool in
                return playerDetails.teamID == matchDetails.team2ID
            }
            
            teamDetails.team1Name = matchDetails.team1ShortName
            teamDetails.team2Name = matchDetails.team2ShortName
           
            teamDetails.team1PlayerCount = "\(firstTeamCount.count)"
            teamDetails.team2PlayerCount = "\(secondTeamCount.count)"

            teamDetails.totalWicketKeeprCount = "\(wicketKeepersArray.count)"
            teamDetails.totalBatsmanCount = "\(batsmanArray.count)"
            teamDetails.totalAllRounderCount = "\(allRounderArray.count)"
            teamDetails.totalBowlerCount = "\(bowlersArray.count)"
            teamsArray.append(teamDetails)
        }
                
        return teamsArray
    }
    
}
