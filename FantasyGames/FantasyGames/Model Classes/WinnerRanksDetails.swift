//
//  WinnerRanksDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 21/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
class WinnerRanksDetails: NSObject {

    lazy var rankPrice = ""
    lazy var rank = ""
    
    class func getAllWinningRankDetails(dataArray: Array<JSON> ) -> Array<WinnerRanksDetails> {
        var winnersArray = Array<WinnerRanksDetails>()
        for details in dataArray {
            let winnerDetails = WinnerRanksDetails()
            winnerDetails.rankPrice = details["winAmount"].stringValue
            let toRank = details["toRank"].stringValue
            let fromRank = details["fromRank"].stringValue
            if toRank == fromRank {
                winnerDetails.rank = "#\(fromRank)"
            }
            else{
                winnerDetails.rank = "#\(fromRank) - #\(toRank)"
            }
            winnersArray.append(winnerDetails)
        }
        return winnersArray
    }
    
}
