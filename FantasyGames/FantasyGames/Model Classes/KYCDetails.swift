//
//  KYCDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 06/02/20.
//  Copyright © 2020 App Creatives Technologies. All rights reserved.
//

import UIKit

class KYCDetails: NSObject {

    lazy var panName = ""
    lazy var panNumber = ""
    lazy var panDOB = ""
    lazy var panImage = ""

    lazy var accountNumber = ""
    lazy var ifscCode = ""
    lazy var bankName = ""
    lazy var branchName = ""
    lazy var bankImage = ""
    
    lazy var panVerified = "0"
    lazy var bankVerified = "0"
    lazy var emailVerified = "0"

}
