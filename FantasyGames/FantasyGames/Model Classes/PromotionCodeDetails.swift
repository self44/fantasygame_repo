//
//  PromotionCodeDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 08/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class PromotionCodeDetails: NSObject {

    lazy var promoCode = ""
    lazy var message = ""
    lazy var shortMessage = ""
    lazy var expiryDate = ""

    
    class func getPromoCodeArray(dataArray: [JSON]) -> Array<PromotionCodeDetails> {
        var promoCodesArray = Array<PromotionCodeDetails>()
        
        for details in dataArray {
            if let codeDetails = details["promocode"].dictionary {
                let promoCodeDetails = PromotionCodeDetails()
                promoCodeDetails.promoCode = codeDetails["promocode"]?.string ?? ""
                promoCodeDetails.message = codeDetails["description"]?.string ?? ""
                promoCodeDetails.shortMessage = codeDetails["shortDescription"]?.string ?? ""
                if let expiryDate = codeDetails["codeDetails"]?.string {
                    promoCodeDetails.expiryDate = AppHelpers.getFormatedDateWithoutTime(dateString: expiryDate)
                }
                promoCodesArray.append(promoCodeDetails)
            }
        }
        return promoCodesArray
    }
}
