//
//  PlayerDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 13/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class PlayerDetails: NSObject {
    lazy var playerName = ""
    lazy var playerShortName = ""

    lazy var playerRole = ""
    lazy var playerImage = ""
    lazy var playerKey = ""
    lazy var teamName = ""
    lazy var teamID = ""
    lazy var matchID = ""
    lazy var gender = ""

    lazy var playerCredits = "0"
    lazy var playerPoints = "0"
    lazy var playerScore = "0"

    lazy var isSelected = false
    
    lazy var isCaption = false
    lazy var isViceCaption = false
    lazy var isPlaying = false

    
    lazy var captainScore = "0"
    lazy var viceCaptainScore = "0"

    
    
    lazy var pointsBattingSixes = "0"
    lazy var pointsBowlingWicket5 = "0"
    lazy var pointsBowlingWickets = "0"
    lazy var pointsBattingRuns = "0"
    lazy var pointsStarting = "0"
    lazy var pointsBatting100 = "0"
    lazy var pointsBatting50 = "0"
    lazy var pointsBowlingWicket4 = "0"
    lazy var pointsBowlingMaidens = "0"
    lazy var pointsFieldingRunout = "0"
    lazy var pointsFieldingStumped = "0"
    lazy var pointsFieldingCatch = "0"
    lazy var pointsBattingDuck = "0"
    lazy var pointsBattingFours = "0"
    lazy var pointsBowlingEconomy = "0"
    lazy var pointsBattingRate = "0"
    lazy var pointsBoundaryBonus = "0"
    lazy var battingFours = "0"
    lazy var battingRuns = "0"
    lazy var battingRate = "0"
    lazy var bowlingWickets = "0"
    lazy var bowlingEconomy = "0"
    lazy var battingDismissed = "0"

    lazy var fieldingCatch = "0"
    lazy var fieldingRunout = "0"
    lazy var fieldingStumped = "0"
        
    lazy var battingSixes = "0"
    lazy var totalBoundary = "0"
    lazy var bowlingMaidens = "0"
    
    class func getPlayerDetailsArray(responseArray: Array<JSON>) -> Array<PlayerDetails> {
        
        var playerDetailsArray = Array<PlayerDetails>()
        
        for details in responseArray {
            let playerDetails = PlayerDetails.parsePlayerDetails(details: details)
            playerDetailsArray.append(playerDetails)
        }
        
        return playerDetailsArray
    }
    
    
    class func parsePlayerDetails(details: JSON) -> PlayerDetails {
        let playerDetails = PlayerDetails()
        
        playerDetails.playerName = details["fullname"].string ?? ""
        playerDetails.playerShortName = playerDetails.playerName.getShortName()
        if let position = details["position"].dictionary {
            playerDetails.playerRole = position["name"]?.string ?? ""
        }
        else if let position = details["position"].string{
            playerDetails.playerRole = position
        }
        playerDetails.playerImage = details["imagePath"].string ?? ""
        playerDetails.matchID = details["matchID"].stringValue
        playerDetails.playerKey = details["id"].stringValue
        playerDetails.teamID = details["teamId"].stringValue
        playerDetails.teamName = details["team"].string ?? ""
        playerDetails.gender = details["gender"].string ?? ""
        playerDetails.playerCredits = String(details["playerCredits"].floatValue)
        playerDetails.playerPoints = details["playerPoints"].stringValue
        playerDetails.playerScore = details["playerScore"].stringValue

        playerDetails.captainScore = details["captainScore"].stringValue
        playerDetails.viceCaptainScore = details["viceCaptainScore"].stringValue
        
        if playerDetails.captainScore == "" {
            playerDetails.captainScore = "0"
        }
        
        if playerDetails.viceCaptainScore == "" {
            playerDetails.viceCaptainScore = "0"
        }
        
        if playerDetails.playerScore == "" {
            playerDetails.playerScore = "0"
        }
        
        if playerDetails.playerPoints == "" {
            playerDetails.playerPoints = "0"
        }        
        
        if playerDetails.playerPoints == ""{
            playerDetails.playerPoints = "0"
        }
        let isPlaying = details["isPlaying"].stringValue

        if isPlaying == "1" {
            playerDetails.isPlaying = true
        }
        
        playerDetails.teamName = playerDetails.teamName.getShortName()
        return playerDetails
    }
    
    
    // After match close to get the players points details
    class func getPlayerPointsDetailsArray(responseArray: Array<JSON>, matchDetails: MatchDetails) -> Array<PlayerDetails> {
        
        var playerDetailsArray = Array<PlayerDetails>()
        
        for details in responseArray {
            let playerDetails = PlayerDetails.parsePlayerPointsDetails(details: details, matchDetails: matchDetails)
            playerDetailsArray.append(playerDetails)
        }
        
        return playerDetailsArray
    }

    class func parsePlayerPointsDetails(details: JSON, matchDetails: MatchDetails) -> PlayerDetails {
        let playerDetails = PlayerDetails()

        playerDetails.playerName = details["fullname"].string ?? ""
        playerDetails.playerShortName = playerDetails.playerName.getShortName()
        if let position = details["position"].dictionary {
            playerDetails.playerRole = position["name"]?.string ?? ""
        }
        else if let position = details["position"].string{
            playerDetails.playerRole = position
        }
        playerDetails.playerImage = details["imagePath"].string ?? ""
        playerDetails.playerKey = details["playerId"].stringValue
        
        var teamID = ""

        if let lineups = details["lineup"].dictionary {
            teamID = lineups["teamId"]?.stringValue ?? ""
        }

        if matchDetails.team1ID == teamID {
            playerDetails.teamName = matchDetails.team1ShortName
        }
        else if matchDetails.team2ID == teamID {
            playerDetails.teamName = matchDetails.team2ShortName
        }
        
        playerDetails.teamID = teamID

        playerDetails.pointsBattingSixes = details["pointsBattingSixes"].stringValue
        playerDetails.pointsBowlingWicket5 = details["pointsBowlingWicket5"].stringValue
        playerDetails.pointsBowlingWickets = details["pointsBowlingWickets"].stringValue
        playerDetails.pointsBattingRuns = details["pointsBattingRuns"].stringValue
        playerDetails.pointsStarting = details["pointsStarting"].stringValue
        playerDetails.pointsBatting100 = details["pointsBatting100"].stringValue
        playerDetails.pointsBatting50 = details["pointsBatting50"].stringValue
        playerDetails.pointsBowlingWicket4 = details["pointsBowlingWicket4"].stringValue
        playerDetails.pointsBowlingMaidens = details["pointsBowlingMaidens"].stringValue
        playerDetails.pointsFieldingRunout = details["pointsFieldingRunout"].stringValue
        playerDetails.pointsFieldingStumped = details["pointsFieldingStumped"].stringValue
        playerDetails.pointsFieldingCatch = details["pointsFieldingCatch"].stringValue
        
        playerDetails.battingFours = details["battingFours"].stringValue
        playerDetails.battingRuns = details["battingRuns"].stringValue
        playerDetails.battingSixes = details["battingSixes"].stringValue
        playerDetails.battingSixes = details["battingSixes"].stringValue
        playerDetails.battingRate = details["battingRate"].stringValue
        playerDetails.bowlingWickets = details["bowlingWickets"].stringValue
        playerDetails.bowlingMaidens = details["bowlingMaidens"].stringValue
        playerDetails.bowlingEconomy = details["bowlingEconomy"].stringValue
        playerDetails.battingDismissed = details["battingDismissed"].stringValue
        playerDetails.fieldingCatch = details["fieldingCatch"].stringValue
        playerDetails.fieldingRunout = details["fieldingRunout"].stringValue
        playerDetails.fieldingStumped = details["fieldingStumped"].stringValue

        playerDetails.pointsBattingDuck = details["pointsBattingDuck"].stringValue
        playerDetails.pointsBattingFours = details["pointsBattingFours"].stringValue
        playerDetails.pointsBowlingEconomy = details["pointsBowlingEconomy"].stringValue
        playerDetails.pointsBattingRate = details["pointsBattingRate"].stringValue
        playerDetails.pointsBoundaryBonus = details["pointsBoundaryBonus"].stringValue
        
        playerDetails.captainScore = details["captainScore"].stringValue
        playerDetails.viceCaptainScore = details["viceCaptainScore"].stringValue

        playerDetails.playerScore = details["playerScore"].stringValue

        return playerDetails
   }
}


