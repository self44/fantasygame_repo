//
//  LeagueDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class LeagueDetails: NSObject {

    lazy var leagueId = ""
    lazy var catagoryId = ""
    lazy var isPrivate = ""
    lazy var fantasyType = ""
    lazy var templateId = ""
    lazy var leagueType = ""
    lazy var leagueName = ""
    lazy var leagueMessage = ""
    lazy var isMega = ""
    lazy var winPerUser = ""
    lazy var totalWinners = ""
    lazy var isMultipleWinners = ""
    lazy var matchId = ""
    lazy var maxPlayers = ""
    lazy var joiningAmount = "0"
    lazy var pricePool = "0"
    lazy var leagueCode = ""
    lazy var teamEntryType = ""
    lazy var bonusPercentage = "0"
    lazy var totalJoined = "0"
    
    lazy var teamNumber = "0"
    lazy var rank = "0"

    lazy var isConfirmedLeague = false
    lazy var isMultiEntryLeague = false
    lazy var isBonusApplicable = false

    
    class func getAllLeagues(dataArray: Array<JSON>) -> Array<LeagueDetails> {
        var leaguesArray = Array<LeagueDetails>()
        for details in dataArray {
            let leagueDetails = LeagueDetails.parseLeagueDetails(details: details)
            leaguesArray.append(leagueDetails)
        }
        return leaguesArray
    }

    class func parseLeagueDetails(details: JSON) -> LeagueDetails {
        
        let leagueDetails = LeagueDetails()
        
        leagueDetails.leagueId = details["leagueId"].stringValue
        leagueDetails.catagoryId = details["templateId"].stringValue
        leagueDetails.isPrivate = details["isPrivate"].stringValue
        leagueDetails.fantasyType = details["fantasyType"].stringValue
        leagueDetails.templateId = details["templateId"].stringValue
        leagueDetails.leagueType = details["leagueType"].stringValue
        leagueDetails.leagueMessage = details["leagueMessage"].string ?? ""
        leagueDetails.isMega = details["leagueMessage"].stringValue
        leagueDetails.winPerUser = details["winPerUser"].stringValue
        leagueDetails.totalWinners = details["totalWinners"].stringValue
        leagueDetails.isMultipleWinners = details["isMultipleWinners"].stringValue
        leagueDetails.matchId = details["matchId"].stringValue
        leagueDetails.maxPlayers = details["maxPlayers"].stringValue
        leagueDetails.joiningAmount = details["joiningAmount"].stringValue
        leagueDetails.pricePool = details["pricePool"].stringValue
        leagueDetails.leagueCode = details["leagueCode"].string ?? ""
        leagueDetails.leagueName = details["leagueName"].string ?? ""
        leagueDetails.teamEntryType = details["teamEntryType"].stringValue
        if leagueDetails.teamEntryType == "1" {
            leagueDetails.isMultiEntryLeague = true
        }
        
        let isBonusApplicable = details["isBonusApplicable"].stringValue

        if isBonusApplicable == "1" {
            leagueDetails.isBonusApplicable = true
        }

        let isConfirmedLeague = details["isConfirmedLeague"].stringValue

        if isConfirmedLeague == "1" {
            leagueDetails.isConfirmedLeague = true
        }

        leagueDetails.totalJoined = details["totalJoined"].stringValue
        leagueDetails.bonusPercentage = details["bonusPercent"].stringValue
        
        
        leagueDetails.teamNumber = details["teamNumber"].stringValue
        leagueDetails.rank = details["rank"].stringValue
        
        return leagueDetails
    }
  
}
