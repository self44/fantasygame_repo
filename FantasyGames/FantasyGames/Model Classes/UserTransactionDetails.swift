//
//  UserTransactionDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 17/12/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserTransactionDetails: NSObject {
    
    lazy var transactionID = ""
    lazy var message = ""
    lazy var amount = ""
    lazy var leagueName = ""
    lazy var matchName = ""
    lazy var type = ""
    lazy var createdDate = ""

    class func getUserTransactionArray(dataArray: Array<JSON>) -> Array<UserTransactionDetails> {
        var transactionsArray = Array<UserTransactionDetails>()
        
        for details in dataArray {
            transactionsArray.append(UserTransactionDetails.parseUserTransactionDetails(details: details))
        }
        
        return transactionsArray
    }
    
    class func parseUserTransactionDetails(details: JSON) -> UserTransactionDetails {

        let transactionDetails = UserTransactionDetails()
        
        transactionDetails.message = details["message"].string ?? ""
        transactionDetails.amount = details["amount"].stringValue
        transactionDetails.leagueName = details["leagueName"].string ?? ""
        transactionDetails.matchName = details["matchName"].string ?? ""
        transactionDetails.type = details["type"].stringValue
        transactionDetails.transactionID = details["_id"].string ?? ""
        let createdDate = details["createdAt"].string ?? ""
        transactionDetails.createdDate = AppHelpers.getTransactionFormattedDate(dateString: createdDate)
        return transactionDetails
    }
}

