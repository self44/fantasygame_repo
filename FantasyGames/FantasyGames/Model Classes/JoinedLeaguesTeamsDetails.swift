//
//  JoinedLeaguesTeamsDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 21/10/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class JoinedLeaguesTeamsDetails: NSObject {

    lazy var userImgUrl = ""
    lazy var userName = ""
    lazy var userID = ""
    lazy var teamID = ""
    lazy var teamNumber = ""
    lazy var userRank = ""
    lazy var userPoints = ""
    lazy var gender = ""
    lazy var creditAmount = ""

    
    class func getAllJoinedLeagueTeamsArray(dataArray: Array<JSON>) -> Array<JoinedLeaguesTeamsDetails> {
        var teamsArray = Array<JoinedLeaguesTeamsDetails>()
        
        for details in dataArray {
            let teamsDetails = JoinedLeaguesTeamsDetails.parseJoinedLeagueTeamsDetails(details: details)
            teamsArray.append(teamsDetails)
        }
        
        return teamsArray
    }
    
    class func parseJoinedLeagueTeamsDetails(details: JSON) -> JoinedLeaguesTeamsDetails {
        
        let teamsDetails = JoinedLeaguesTeamsDetails()
        teamsDetails.userRank = details["rank"].stringValue 
        teamsDetails.creditAmount = details["creditsWon"].stringValue

        if let userDetails = details["user"].dictionary  {
            teamsDetails.userName = userDetails["username"]?.string ?? ""
            teamsDetails.userImgUrl = userDetails["image"]?.string ?? ""
            teamsDetails.userID = userDetails["_id"]?.string ?? ""
        }
        
        if let teamDetails = details["team"].dictionary  {
            teamsDetails.userPoints = teamDetails["points"]?.stringValue ?? "0"
            teamsDetails.teamID = teamDetails["_id"]?.string ?? ""
            let teamNumber = teamDetails["teamNumber"]?.stringValue ?? "0"
            teamsDetails.userName = "\(teamsDetails.userName) (T\(teamNumber))"
        }

        return teamsDetails
    }

}
