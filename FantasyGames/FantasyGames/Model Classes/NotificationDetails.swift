//
//  NotificationDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/01/20.
//  Copyright © 2020 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationDetails: NSObject {

    lazy var notificationID = ""
    lazy var message = ""
    lazy var date = ""
    lazy var time = ""
    lazy var image = ""
    lazy var redirectionType = ""
    
    class func getAllNotifcationDetailsArray(dataArray: Array<JSON>) -> [NotificationDetails] {
        var notificationDetailsArray = Array<NotificationDetails>()
        for details in dataArray {
            let notificationDetails = NotificationDetails.parseNotifcationDetails(details: details)
            notificationDetailsArray.append(notificationDetails)
        }
        
        return notificationDetailsArray
    }
    
    class func parseNotifcationDetails(details: JSON) -> NotificationDetails {
        let notificationDetails = NotificationDetails()
        return notificationDetails
    }
}
