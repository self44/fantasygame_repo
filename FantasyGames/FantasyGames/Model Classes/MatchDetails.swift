//
//  MatchDetails.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 07/09/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class MatchDetails: NSObject {
    
    lazy var team1Name = ""
    lazy var team2Name = ""
    
    lazy var team1ShortName = ""
    lazy var team2ShortName = ""

    lazy var team1Image = ""
    lazy var team2Image = ""

    lazy var team1ID = ""
    lazy var team2ID = ""
    lazy var showLineupsOut = "" // 1 Means Linesup out
    
    lazy var seriesName = ""
    lazy var matchID = ""
    lazy var isClosed = false
    lazy var startDateUnix = "0"
    lazy var matchTime = ""

    lazy var matchCompleteMessage = ""

    lazy var closingTs = ""
    var matchStatus = ""    
    
    class func getAllUpcomingMatchList(JSONDict: JSON) -> Array<MatchDetails> {
        var matchDetailsArray = Array<MatchDetails>()
        
        if let response = JSONDict["response"].dictionary {
            if let matchesArray = response["matches"]?.array {
                for details in matchesArray {
                    let matchDetails = MatchDetails.parseMatchDetails(details: details)
                    matchDetailsArray.append(matchDetails)
                }
            }
        }
        
        return matchDetailsArray
    }
    
    
    class func getMatchDetails(dataArray: Array<JSON>) -> Array<MatchDetails> {
        var matchDetailsArray = Array<MatchDetails>()

        for details in dataArray {
            let matchDetails = MatchDetails.parseMatchDetails(details: details)
            matchDetailsArray.append(matchDetails)
        }
        
        return matchDetailsArray
    }
    
    class func parseMatchDetails(details: JSON) -> MatchDetails {
        
        let matchDetails = MatchDetails()
        
        if let visitorTeam = details["visitorteam"].dictionary {
            matchDetails.team2Name = visitorTeam["name"]?.string ?? ""
            matchDetails.team2ShortName = visitorTeam["code"]?.string ?? ""
            matchDetails.team2ID = visitorTeam["id"]?.stringValue ?? ""
            matchDetails.team2Image = visitorTeam["imagePath"]?.string ?? ""
        }
        
        if let localTeam = details["localteam"].dictionary {
            matchDetails.team1Name = localTeam["name"]?.string ?? ""
            matchDetails.team1ShortName = localTeam["code"]?.string ?? ""
            matchDetails.team1ID = localTeam["id"]?.stringValue ?? ""
            matchDetails.team1Image = localTeam["imagePath"]?.string ?? ""
        }
        
        if let season = details["season"].dictionary {
            matchDetails.seriesName = season["name"]?.string ?? ""
        }
        
        matchDetails.matchStatus = details["active"].stringValue
        if (matchDetails.matchStatus == MatchStatus.Inactive.rawValue) || (matchDetails.matchStatus == MatchStatus.VisiblelOnly.rawValue) || (matchDetails.matchStatus == MatchStatus.Active.rawValue) {
            matchDetails.isClosed = false
        }
        else{
            matchDetails.isClosed = true
        }
        
        matchDetails.closingTs = details["closingTs"].stringValue
        matchDetails.startDateUnix = details["startDateUnix"].stringValue

        let closingTime = Double(matchDetails.closingTs) ?? 0
        let startDateUnix = Double(matchDetails.startDateUnix) ?? 0
        matchDetails.startDateUnix = String(startDateUnix - closingTime);

        matchDetails.showLineupsOut = details["showLineupsOut"].stringValue
        matchDetails.matchID = details["matchId"].stringValue
       matchDetails.matchCompleteMessage = details["matchCompleteMessage"].string ?? ""

       
        matchDetails.matchTime = AppHelpers.getMatchTime(timeResult: matchDetails.startDateUnix)
        
        return matchDetails
        
    }
}
