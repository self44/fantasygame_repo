//
//  NotificationsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/01/20.
//  Copyright © 2020 App Creatives Technologies. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    var notificationDetailsArray = Array<[String: Any]>()
    var isNeedToShowLoadMore = false

    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
    }
    
    //MARK:- API Related Methods
    func callGetNotificationsList(isShowLoader: Bool) {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        if isShowLoader{
            AppHelpers.sharedInstance.displayLoader()
        }
                
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: kGetNotificationURL) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    if let dataArray = result![""].array{
//                        weakSelf?.notificationDetailsArray = NotificationDetails.getAllNotifcationDetailsArray(dataArray: dataArray)
                    }
//                    weakSelf?.tbl.reloadData()
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return notificationDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 38))
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 5, width: tableView.bounds.size.width, height: 24))
        titleLabel.textColor = UIColor.appBlackColor()
        titleLabel.font = UIFont(name: "Arial", size: 15)
        headerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255, alpha: 1)
        titleLabel.clipsToBounds = true;
        headerView.addSubview(titleLabel)
        headerView.clipsToBounds = true;
        let transAction = notificationDetailsArray[section]
        titleLabel.text = transAction["date"] as? String
        titleLabel.textAlignment = .center
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let transAction = notificationDetailsArray[section]
        let detailsArray = transAction["details_list"] as! Array<NotificationDetails>
        
        if isNeedToShowLoadMore && (section == notificationDetailsArray.count - 1) {
            return detailsArray.count + 1;
        }

        return detailsArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transAction = notificationDetailsArray[indexPath.section]
        let detailsArray = transAction["details_list"] as! Array<NotificationDetails>

        if (indexPath.section == notificationDetailsArray.count - 1) && detailsArray.count == indexPath.row{
            var cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: .value1, reuseIdentifier: "LoadMoreTableViewCell") as? LoadMoreTableViewCell
            }
            
            cell?.selectionStyle = .none
            if isNeedToShowLoadMore{
                isNeedToShowLoadMore = false
//                callNotificationAPI(isNeedToShowLoader: false)
            }
            
            return cell!

        }else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell") as? NotificationsTableViewCell
            if cell == nil {
                cell = NotificationsTableViewCell(style: .value1, reuseIdentifier: "NotificationsTableViewCell")
            }
            
            cell?.selectionStyle = .none
            
            let notificationDetails = detailsArray[indexPath.row]
            
//            cell?.lblTitle.text = notificationDetails.notificationTitle
//            cell?.lblNotificationMessage.text = notificationDetails.notificationMessage.htmlToString
//            cell?.lblDate.text = AppHelper.getTicketFormattedTime(dateString: notificationDetails.notificationDateAndTime)
            
//            if notificationDetails.notificationImage.count > 0 {
//                cell?.imgViewHeightConstraint.constant = 140.0
//
//                if let url = NSURL(string: UserDetails.sharedInstance.promotionImageUrl + notificationDetails.notificationImage){
//                    cell?.imgView.setImage(with: url as URL, placeholder: UIImage(named: "ImagePlaceholder"), progress: { received, total in
//                        // Report progress
//                    }, completion: { [weak self] image in
//                        if (image != nil){
//                            cell?.imgView.image = image
//                        }
//                        else{
//                            cell?.imgView.image = UIImage(named: "ImagePlaceholder")
//                        }
//                    })
//                }
//                else{
//                    cell?.imgView.image = UIImage(named: "ImagePlaceholder")
//                }
//            }
//            else{
//                cell?.imgViewHeightConstraint.constant = 0.0
//            }
            
            cell?.layoutIfNeeded()
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
