//
//  Playing22ViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 10/11/19.
//  Copyright © 2019 App Creatives Technologies. All rights reserved.
//

import UIKit

class Playing22ViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    lazy var totalPlayerArray = Array<PlayerDetails>()
    var selectedMatchDetails: MatchDetails?
    lazy var matchType = MatchType.Cricket.rawValue
    let refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.headerGradient()
        containerView.topViewCorners()
        refreshControl.addTarget(self, action: #selector(refreshPlayerPoints), for: UIControl.Event.valueChanged)
        tblView.addSubview(refreshControl)
        tblView.alwaysBounceVertical = true;

        tblView.register(UINib(nibName: "Playing22PointsTableViewCell", bundle: nil), forCellReuseIdentifier: "Playing22PointsTableViewCell")
        callGetPlaying22Points()
    }
    
    @objc func refreshPlayerPoints() {
        refreshControl.endRefreshing()
        callGetPlaying22Points()
    }
   
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API Related Methods
    func callGetPlaying22Points() {
        if !AppHelpers.isInterNetConnectionAvailable(){
            return
        }
        
        guard let details = selectedMatchDetails else {
            return;
        }

        var matchURL = ""
        
        if matchType == MatchType.Cricket.rawValue {
            matchURL = kGetCricketPlaying22URL + details.matchID
        }
        AppHelpers.sharedInstance.displayLoader()
        weak var weakSelf = self
        WebServiceHandler.performGETRequest(withURL: matchURL) { (result, error) in
            AppHelpers.sharedInstance.removeLoader()
            
            if result != nil{
                let status = result!["code"].stringValue
                let message = result!["message"].string
                
                if status == "200"{
                    if let matchDetails = result!["match"].dictionary {
                        if let playersArray = matchDetails["lineup"]?.array {
                            weakSelf?.totalPlayerArray = PlayerDetails.getPlayerPointsDetailsArray(responseArray: playersArray, matchDetails: details)
                            weakSelf?.tblView.reloadData()
                        }
                    }
                }
                else{
                    AppHelpers.showCustomErrorAlertView(message: message ?? kErrorMsg)
                }
            }
            else{
                AppHelpers.showCustomErrorAlertView(message: error?.localizedDescription ?? kErrorMsg)
            }
        }
    }
    
}



extension Playing22ViewController: UITableViewDelegate, UITableViewDataSource{
    
   //MARK:- Table View Data Source and Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalPlayerArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "Playing22PointsTableViewCell") as? Playing22PointsTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "Playing22PointsTableViewCell") as? Playing22PointsTableViewCell
        }
        
        cell?.selectionStyle = .none
        let details = totalPlayerArray[indexPath.row]
        cell!.configData(details: details)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let playerPointDetailsVC = storyboard?.instantiateViewController(withIdentifier: "PlayerPointDetailsViewController") as! PlayerPointDetailsViewController
        playerPointDetailsVC.totalPlayerArray = totalPlayerArray
        playerPointDetailsVC.selectedIndex = indexPath.row
        navigationController?.pushViewController(playerPointDetailsVC, animated: true)
    }
    
}
