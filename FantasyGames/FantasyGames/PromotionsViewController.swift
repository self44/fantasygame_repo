//
//  PromotionsViewController.swift
//  FantasyGames
//
//  Created by Vikash Rajput on 15/03/20.
//  Copyright © 2020 App Creatives Technologies. All rights reserved.
//

import UIKit

class PromotionsViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
